<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahsulotlar';
?>
<div class="products-index">
    <div class="row">
        <div class="col-md-6">
            <h3 style="margin: 0;"><?= Html::encode($this->title) ?></h3>
        </div>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sub_category_id',
            'title_uz',
            'title_ru',
            'price',
            'count',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',    
            ],
        ],
    ]); ?>


</div>
