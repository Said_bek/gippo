<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OrderCode */

$this->title = $model->order_code;
\yii\web\YiiAsset::register($this);

?>
<div class="order-code-view">

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            Buyurtma kodi: <?= Html::encode($this->title) ?>
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tr>
                <th style="width: 10px">#</th>
                <th>Mahsulot nomi</th>
                <th>Buyurtma ummumiy summasi</th>
                <th>Buyurtma soni</th>
                <!-- <th>Qo`shimcha ma`lumotlar</th> -->
            </tr>
            <?php 
                $i = 1;
                foreach ($model->orders as $key => $value) { 
            ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo (($value->products) ? $value->products->title_uz : ''); ?></td>
                    <td><?php echo $value["sell_price"]; ?></td>
                    <td><?php echo $value["count"]; ?></td>
                    
                </tr>
            <?php
                $i++; 
                } 
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <?php if ($model->type == 1 ) { ?>
                    <img style="width:500px;" src="<?php echo $model->file_id ?>" alt="">
                <?php  } else if ($model->type == 2) { ?>
                    <a href="<?php echo $model->file_id; ?>" style="margin-top: 20px;" class="btn btn-success">To`lovni ko`rish</a>
                <?php } else if ($model->type == 3) { ?>
                    <p>Naqd</p>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    </div>
<!-- /.box -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <!-- <th style="width: 10px">#</th> -->
                    <th>Buyurtma viloyati</th>
                    <th>Buyurtma haqida mijoz fikri</th>
                </tr>
                <tr>

                    <td><?php echo $model->region->title_uz; ?></td>
                    <?php if (isset($model->camment)): ?>
                        <td><?php echo base64_decode($model->camment->text_camment); ?></td>
                    <?php endif ?>
                    
                </tr>
            </table>            
        </div>
        <div class="col-md-12">
            <iframe src="https://maps.google.com/maps?q=<?php echo $model->lat; ?>, <?php echo $model->lang; ?>&z=15&output=embed" width="100%" height="270" frameborder="0" style="border:0"></iframe>

        </div>
    </div>
</div>

