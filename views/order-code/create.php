<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderCode */

$this->title = 'Create Order Code';
$this->params['breadcrumbs'][] = ['label' => 'Order Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-code-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
