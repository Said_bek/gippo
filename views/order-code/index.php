<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buyurtmalar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-code-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_code',
            [ 
                'attribute' =>'full_name',
                'label'  => ' Buyurtma vaqti',
                'value' => function ($data) {
                    return $date = date("Y-m-d H:i",strtotime($data->created_date));
                }
            ],
            [ 
                'attribute' =>'full_name',
                'label'  => 'Ismi',
                'value' => function ($data) {
                    return base64_decode($data->client->full_name);
                }
            ],
            [ 
                'attribute' =>'full_name',
                'label'  => 'Telefon raqami',
                'value' => function ($data) {
                    return base64_decode($data->client->phone_number);
                }
            ],
            [
                'format' => 'html',
                'label' => 'Holat',
                'value' => function ($data) {
                    if ($data->billing_status == 1) {
                        return '<span class=\'badge\' style=\'background:#17a2b8;\'>Buyurtma rasmiylashtiridi</span>';
                    } else if ($data->billing_status == 2) {
                        return '<span class=\'badge badge->secondary\'>Kutilmoqda</span>';
                    } else if ($data->billing_status == 3) {
                        return '<span class=\'badge badge->secondary\' >Kutilmoqda</span>';
                    } else if ($data->billing_status == 4) {
                        return '<span class=\'badge\' style=\'background:#28a745;\'>Tolov bo`lgan</span>';
                    } else if ($data->billing_status > 5) {
                        return '<span class=\'badge\' style=\'background:#28a745;\'>Qabul qilingan</span>';
                    } else  {
                        return '<span class=\'badge badge->secondary\'>Kutilmoqda</span>';
                    }
                },
            ],
            [
                'format' => 'html',
                'label' => 'Qo`shimcha',
               'contentOptions' => ['style' => 'text-align:right;'], // For TD
                'value' => function ($data) {
                    if ($data->billing_status == 1) {
                        return '<a href="/order-code/delete?id='.$data->id.'" class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    } else if ($data->billing_status == 2) {
                        return '<a href="/order-code/delete?id='.$data->id.'" class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    } else if ($data->billing_status == 3) {
                        return '<a href="/order-code/delete?id='.$data->id.'" class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    } else if ($data->billing_status == 4) {
                        if ($data->type == 1 or $data->type == 2 or $data->type == 3) {
                            return '
                            <a href="/order-code/confirm?id='.$data->id.'" class="btn btn-success btn-xs">
                                Qabul qilish
                            </a>
                            <a href="/order-code/delete?id='.$data->id.'" class="btn btn-danger btn-xs">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>';
                        } else {
                            return '<a href="/order-code/confirm?id='.$data->id.'" class="btn btn-success btn-xs">
                                Qabul Qilish
                            </a>';

                        }
                    } else if ($data->billing_status == 5) {
                        return '<a href="/order-code/check?id='.$data->id.'" class="btn btn-success btn-xs">
                                Tasdiqlash
                            </a>';
                    } else if ($data->billing_status == 6) {
                        return '<a href="/order-code/way?id='.$data->id.'" class="btn btn-success btn-xs">
                            Yo`lga chiqarish
                        </a>';
                    } else if ($data->billing_status == 7) {
                        return '<a href="/order-code/done?id='.$data->id.'" class="btn btn-success btn-xs">
                            Yetkazib berildi
                        </a>';
                    } else if ($data->billing_status == 8) {
                        return '<span class=\'badge\' style=\'background:#28a745;\'>Buyurtma yakunlangan</span>';
                    } else {
                        return '<a href="/order-code/delete?id='.$data->id.'" class="btn btn-danger btn-xs"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    }
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',   
            ],
        ],
    ]); ?>


</div>

