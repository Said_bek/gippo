<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cashbacks */

$this->title = 'Cashback kiritish';
$this->params['breadcrumbs'][] = ['label' => 'Cashbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbacks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
