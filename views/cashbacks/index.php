<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cashback';
?>
<style>

    .input {
    position: relative !important;
    appearance: none;
    margin: 8px;
    box-sizing: content-box;
    overflow: hidden;
    }
    .input:before {
    content: '';
    display: block;
    box-sizing: content-box;
    width: 16px;
    height: 16px;
    border: 2px solid #ccc;
    transition: 0.2s border-color ease;
    }
    .input:checked:before {
    border-color: #12CBC4;
    transition: 0.5s border-color ease;
    }
    .input:disabled:before {
    border-color: #ccc;
    background-color: #ccc;
    }
    .input{
    outline: none!important;
    }
    .input:after {
    content: '';
    display: block;
    position: absolute;
    box-sizing: content-box;
    top: 50%;
    left: 50%;
    transform-origin: 50% 50%;
    background-color: #12CBC4;
    width: 16px;
    height: 16px;
    border-radius: 100vh;
    transform: translate(-50%,-50%) scale(0);
    }
    .input:before {
    border-radius: 16px/4;
    }
    .input:after {
    width: 9.6px;
    height: 16px;
    border-radius: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    background-color: transparent;
    box-shadow: 4px 4px 0px 0px #12CBC4;
    }
    .input:checked:after {
    animation: toggleOnCheckbox 0.2s ease forwards;
    }
    .input.filled:before {
    border-radius: 16px/4;
    transition: 0.2s border-color ease, 0.2s background-color ease;
    }
    .input.filled:checked:not(:disabled):before {
    background-color: #12CBC4;
    }
    .input.filled:not(:disabled):after {
    box-shadow: 4px 4px 0px 0px white;
    }
    @keyframes toggleOnCheckbox {
    0% {
    opacity: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-85%) scale(0.9) rotate(45deg);
    }
    100% {
    transform: translate(-50%,-85%) scale(0.8) rotate(45deg);
    }
    }
    @keyframes toggleOnRadio {
    0% {
    opacity: 0;
    transform: translate(-50%,-50%) scale(0);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-50%) scale(0.9);
    }
    100% {
    transform: translate(-50%,-50%) scale(0.8);
    }

    }
    .yii-debug-toolbar {
        display: none!important;
    }

</style>
<div class="cashbacks-index">
    <div class="row">
        <div class="col-md-6">
            <h3 style="margin: 0;"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-6">
            <p>
                <?= Html::a('Cashback kiritish', ['create'], ['class' => 'pull-right btn btn-success']) ?>
            </p>
            <div class="pull-right" style="margin-right: 10px ; display: flex; align-items: center;">
                <label style="margin: 0; padding-right: 5px;" for="cashback">
                    Cashback
                </label>
                <?php if ($cashback_status == 1): ?>
                    <input class="input off_checkbox" checked type="checkbox" id="cashback">
                <?php else: ?>
                    <input class="input on_cashback" type="checkbox" id="cashback">
                <?php endif ?>
            </div>
        </div>
    </div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cashback',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
