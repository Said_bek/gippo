<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSale */

$this->title = 'Chegirma berish';
$this->params['breadcrumbs'][] = ['label' => 'Product Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-sale-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products
    ]) ?>

</div>
