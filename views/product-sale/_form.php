<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSale */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-sale-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?=$form->field($model, 'product_id')->label('Mahsulot nomi')->widget(Select2::classname(),
                [
                    'data' => ArrayHelper::map($products,'id','title_uz'),
                    'pluginOptions' => [
                        'allowClear' => true,
                        // 'minimumInputLength' => 2,
                    ],
                ]);
            ?>            
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-6">
            <label for="">Mahsulot narxi (so'm)</label>
            <input type="text" disabled readonly class="form-control" id='sum'>
        </div>
        <div class="col-md-6">
            <label for="">Mahsulot narxi ($)</label>
            <input type="text" disabled readonly class="form-control" id='dollar'>
        </div>
        <div class="col-md-6">
            <br>
            <?= $form->field($model, 'sale_price')->textInput() ?>
        </div>
        <div class="col-md-6">
            <br>
            <?= $form->field($model, 'sale_dollar')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <br>
            <?php 
                echo DateTimePicker::widget([
                    'name' => 'Section[product_sale][enter_date]',
                    'id' => 'w1',
                    'options' => ['placeholder' => 'Boshlanish vaqti', 'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd hh:ii',
                        'autoclose'=>true,
                    ]
                ]);
            ?>
        </div>
        <div class="col-md-6">
            <br>
            <?php 
                echo DateTimePicker::widget([
                    'name' => 'Section[product_sale][end_date]',
                    'id' => 'w2',
                    'options' => ['placeholder' => 'Tugash vaqti', 'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd hh:ii',
                        'autoclose'=>true,
                    ]
                ]);
            ?>
        </div>
    </div>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
    $js = <<<JS

        $(document).on("change","#productsale-product_id",function(){
            let input = $(this).val()
            
            $.ajax({
                    url: '/product-sale/find',
                    dataType: 'json',
                    type: 'get',
                    data: {input: input},
                    success: function(response) {
                        if (response.status == 'success') {
                            $("#sum").val(response.sum)
                            $("#dollar").val(response.dollar)
                        }
                    }
                })
            
        })

    JS;
    $this->registerJs($js);
?>

