<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->title_uz;
\yii\web\YiiAsset::register($this);
// echo "<pre>";
// // foreach ($arr_multi_id as $key => $value) {
// //     echo "detail_code: ".$value[0]."<br>";
// //     // foreach ($value["detail_id"] as $key2 => $value2) {
// //     //     echo "detail_id: ".$key2."<br>";
// //     //     // print_r($value2);
// //     //     $i = 0;
// //     //     foreach ($value2 as $key3 => $value3) {
// //     //         foreach ($value3 as $key4 => $value4) {
// //     //             echo "Multi_detail_id: ".$value4."<br>";
// //     //         }
// //     //     }
// //     // }
// // }
// print_r($brand_details);
// die();
?>
<style>

    .input {
    position: relative !important;
    appearance: none;
    margin: 8px;
    box-sizing: content-box;
    overflow: hidden;
    }
    .input:before {
    content: '';
    display: block;
    box-sizing: content-box;
    width: 16px;
    height: 16px;
    border: 2px solid #ccc;
    transition: 0.2s border-color ease;
    }
    .input:checked:before {
    border-color: #12CBC4;
    transition: 0.5s border-color ease;
    }
    .input:disabled:before {
    border-color: #ccc;
    background-color: #ccc;
    }
    .input{
    outline: none!important;
    }
    .input:after {
    content: '';
    display: block;
    position: absolute;
    box-sizing: content-box;
    top: 50%;
    left: 50%;
    transform-origin: 50% 50%;
    background-color: #12CBC4;
    width: 16px;
    height: 16px;
    border-radius: 100vh;
    transform: translate(-50%,-50%) scale(0);
    }
    .input:before {
    border-radius: 16px/4;
    }
    .input:after {
    width: 9.6px;
    height: 16px;
    border-radius: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    background-color: transparent;
    box-shadow: 4px 4px 0px 0px #12CBC4;
    }
    .input:checked:after {
    animation: toggleOnCheckbox 0.2s ease forwards;
    }
    .input.filled:before {
    border-radius: 16px/4;
    transition: 0.2s border-color ease, 0.2s background-color ease;
    }
    .input.filled:checked:not(:disabled):before {
    background-color: #12CBC4;
    }
    .input.filled:not(:disabled):after {
    box-shadow: 4px 4px 0px 0px white;
    }
    @keyframes toggleOnCheckbox {
    0% {
    opacity: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-85%) scale(0.9) rotate(45deg);
    }
    100% {
    transform: translate(-50%,-85%) scale(0.8) rotate(45deg);
    }
    }
    @keyframes toggleOnRadio {
    0% {
    opacity: 0;
    transform: translate(-50%,-50%) scale(0);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-50%) scale(0.9);
    }
    100% {
    transform: translate(-50%,-50%) scale(0.8);
    }

    }
    .yii-debug-toolbar {
        display: none!important;
    }

</style>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('O`zgartirish', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('O`chirish', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'sub_category_id',

                'label' => 'Kategoriya Nomi',

                'value' => function ($data) {

                    return $data->subCategory->title_uz;
                }


            ],
            'title_uz',
            'title_ru',
            'price',
            [
                'attribute' => 'img',

                'format' => 'html',

                'label' => 'Rasm',

                'value' => function ($data) {

                    return Html::img("/uploads/".$data['img'], // folder need

                        ['width' => '80px']);
                },  

            ],
            'description_uz:ntext',
            'description_ru:ntext',
        ],
    ]) ?>

        <div class="row">
            <div class="col-md-12">
                <?php if (isset($product_img) and !empty($product_img)) { ?>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Qo'shimcha Rasmlar</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Rasm</th>
                                <th>Qo`shimcha</th>
                            </tr>
                            <?php
                            $i = 1;
                            foreach ($product_img as $key3 => $value3) { ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <img style="width:80px;" src="/uploads/<?php echo $value3["img"];?>">
                                    </td>
                                    <td>
                                        <button id="delete_id" value="<?php echo $value3["id"] ?>" class="btn btn-success delete_img">
                                            <span class="fa fa-trash"></span>
                                        </button>
                                    </td>
                                </tr>
                            <?php 
                            $i++;
                            }

                            ?>
                            <tr>
                            </tr>

                            </table>
                        </div>
                    <!-- /.box-body -->
                    </div>
                <?php } ?>
                    <a href="add-img?id=<?php echo $model["id"] ?>" class="pull-right btn btn-success">Rasm Kiritsh</a>
            </div>
        </div>
     
   
  
</div>
<?php

$js = <<<JS

    $(document).on("click", '.save_multi_details', function() {
        var data_id = $(this).attr("data-id");
        var class1 = ".div_form_" + data_id;
        var form = $(class1);
        $.ajax({
            url: 'save-multi-details',
            data: form.serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    location.reload()   
                } else if (response.status == "fail") {
                    alert(response.errors);
                } else if (response.status == "fail 1") {
                    alert(response.errors);
                }
            }
        })
    });
    $(document).on("click", '.delete_details', function() {
        if(confirm('Ushbu malumotni o`chirishni tasdiqlaysizmi ?') == true){
            var data_id = $(this).attr("data-id");
        }
        $.ajax({
            url: 'delete-code',
            data: {
                id:data_id
            },
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                if (response.status == "success") {
                    location.reload()
                }
            }
        })
    });

JS;
$this->registerJs($js);
?>