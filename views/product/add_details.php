<?php
// echo "<pre>";
// print_r($checked_details);
// die();

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\SubCategory;
use kartik\widgets\SwitchInput;


/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
// echo "<pre>";
// print_r($dropdown);
// die();

?>
<style>

    .input {
    position: relative !important;
    appearance: none;
    margin: 8px;
    box-sizing: content-box;
    overflow: hidden;
    }
    .input:before {
    content: '';
    display: block;
    box-sizing: content-box;
    width: 16px;
    height: 16px;
    border: 2px solid #ccc;
    transition: 0.2s border-color ease;
    }
    .input:checked:before {
    border-color: #12CBC4;
    transition: 0.5s border-color ease;
    }
    .input:disabled:before {
    border-color: #ccc;
    background-color: #ccc;
    }
    .input{
    outline: none!important;
    }
    .input:after {
    content: '';
    display: block;
    position: absolute;
    box-sizing: content-box;
    top: 50%;
    left: 50%;
    transform-origin: 50% 50%;
    background-color: #12CBC4;
    width: 16px;
    height: 16px;
    border-radius: 100vh;
    transform: translate(-50%,-50%) scale(0);
    }
    .input:before {
    border-radius: 16px/4;
    }
    .input:after {
    width: 9.6px;
    height: 16px;
    border-radius: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    background-color: transparent;
    box-shadow: 4px 4px 0px 0px #12CBC4;
    }
    .input:checked:after {
    animation: toggleOnCheckbox 0.2s ease forwards;
    }
    .input.filled:before {
    border-radius: 16px/4;
    transition: 0.2s border-color ease, 0.2s background-color ease;
    }
    .input.filled:checked:not(:disabled):before {
    background-color: #12CBC4;
    }
    .input.filled:not(:disabled):after {
    box-shadow: 4px 4px 0px 0px white;
    }
    @keyframes toggleOnCheckbox {
    0% {
    opacity: 0;
    transform: translate(-50%,-85%) scale(0) rotate(45deg);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-85%) scale(0.9) rotate(45deg);
    }
    100% {
    transform: translate(-50%,-85%) scale(0.8) rotate(45deg);
    }
    }
    @keyframes toggleOnRadio {
    0% {
    opacity: 0;
    transform: translate(-50%,-50%) scale(0);
    }
    70% {
    opacity: 1;
    transform: translate(-50%,-50%) scale(0.9);
    }
    100% {
    transform: translate(-50%,-50%) scale(0.8);
    }

    }

</style>
<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php foreach ($details as $key => $value) { ?>
            <div class="col-md-4">
                <div class="checkBox" > 
                     <div class="box box-success box-solid" >
                        <div class="box-header with-border">
                          <h3 class="box-title">
                            <?php echo $value->title_uz ?>
                          </h3>
                        </div>
                        <?php foreach ($value->multiDetails as $key2 => $value2) { ?>
                            <!-- /.box-tools -->
                            <!-- /.box-header -->
                            <div class="box-body" style="display:flex; align-items: flex-end;">
                                    <?php if ($value->type == 2) { ?>
                                        <input class="hidden"  value="<?php echo $value->id; ?>" name="its_brand">
                                    <?php } ?>
                                        <input class="input" type="checkbox" id="check_<?php echo $value2->id ?>" value="<?php echo $id; ?>" name="<?php echo $value->id ?>[<?php echo $value2->id; ?>]">

                                    <label style="margin: 3px 0 0 5px;" for="check_<?php echo $value2->id ?>">
                                        <?php echo $value2->title_uz ?>
                                    </label>
                            </div>
                            <!-- /.box-body -->
                         <?php } ?>
                      </div>
                      <!-- /.box -->
                    <br>
                </div>
            </div>
        <?php } ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
