<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SubCategory;
/* @var $this yii\web\View */
/* @var $model app\models\Products */

$checked_details = [];
$this->title = 'Mahsulot Qo`shish';
?>
<div class="products-create">

    <h1><?= Html::encode($this->title) ?></h1>
   
    <?=
	 $this->render('_form', [
        'model' => $model,
        'details' => $details,
        'checked_details' => $checked_details,
        'dropdown' => $dropdown
    ]); ?>

</div>
