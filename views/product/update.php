<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Products */


$this->title = 'Mahsulot o`zgartirish: ';
$this->params['breadcrumbs'][] = ['label' => 'Mahsulotlar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'details' => $details,
        'checked_details' => $checked_details,
        'dropdown' => $model->dropdown
    ]) ?>

</div>
