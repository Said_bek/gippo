<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\switchinput\SwitchInput;
 
// Usage with ActiveForm and model and initial value set to true

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahsulotlar';
?>
<div class="products-index">
    <div class="row">
        <div class="col-md-6">
            <h3 style="margin: 0;"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-6">
            <?= Html::a('Mahsulotlar kiritish', ['create'], ['class' => 'pull-right btn btn-success']) ?>
        </div>
    </div>

    <p>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'img',

                'format' => 'html',

                'label' => 'Rasm',

                'value' => function ($data) {

                    return Html::img("/web/uploads/".$data['img'], // folder need

                        ['width' => '80px']);
                },

            ],

            [ 
                'attribute' =>'title_uz',
                'label'  => 'Mahsulot Nomi'
            ],
            [
                'attribute' => 'sub_category_id',

                'label' => 'Kategoriya Nomi',

                'value' => function ($data) {
                    return $data->subCategory->title_uz;
                }

            ],
            [ 
                'attribute' =>'price',
                'label'  => 'Narxi'
            ],
            
            // 'img',
            //'description_uz:ntext',
            //'description_ru:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
