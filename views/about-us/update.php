<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = 'Ma`lumotlarni o`zgaritrish: ' . $model->title_uz;
?>
<div class="about-us-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
