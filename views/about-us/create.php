<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AboutUs */

$this->title = 'Malumot kiritsh';
?>
<div class="about-us-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
