<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\SubCategory;
/* @var $this yii\web\View */
/* @var $model app\models\Category */

?>
<div class="category-view row">
    <div class="col-md-6">
        <h3 style="margin: 0;">
            <?php 
                echo $category->title_uz . " Sub Kategoriyasi";
            ?>
        </h3>    
        
    </div>
    <div class="col-md-6">
            <a class="btn btn-primary pull-right" href="add-sub-category-step?id=<?php echo $category->id; ?>">
                Sub Kategoriya qo'shish
            </a>
            <div class="modal fade" id="modal-default">
                <div class="modal-dialog" style="width: 75%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <label>Tovar nomi</label>
                            <input type="text" name="title_uz">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Yopish</button>
                            <button type="button" class="btn btn-success">Saqlash</button>
                        </div>
                    </div>
                <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>  
    </div>
    
    <table class='table table-bordered table-striped mt-20'>
        <thead>
        <tr>
            <th>#</th>
            <th>Sub Kategoriy nomi</th>
            <th>Sub Kategoriy nomi ru</th>
            <th>Rasm</th>
            <th>Qo'shimcha</th>
            <th>Rasm Qo`shish</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (isset($model) and !empty($model)){
            $i = 1;
            foreach ($model as $key => $value) {
                echo "<tr>";
                echo "<td>".$i."</td>";
                echo "<td>".$value->title_uz."</td>";
                echo "<td>".$value->title_ru."</td>"; 
                if (isset($value->img) and !empty($value->img)) {

                ?>
                    <td>
                        <img style="width:80px;" src='<?php echo "/uploads/".$value->img ?>'>
                    </td>
                <?php
                } else {
                    echo "<td> Rasm hali yuklangman </td>";                 
                }
                $id = 0;
                if(isset($value->id) && !empty($value->id)){
                    $id = $value->id;
                }
                echo "<td>
                        <a href='#' class='ml-3 delete_sub_category' title='Delete' data-delete='$id'><span class='fa fa-trash'></span></a>
                            <a href='#' class='update'  data-target='#modal-default' data-toggle='modal' title='Update' data-update='$id' aria-label='Update'><i class='fa fa-pencil' style='color: #3e8ef7'></i></a>
                            <a href='sub-view?id=".$id."'class='ml-3'><span class='fa fa-eye'></span></a>
                        </td>";
                
                    echo "<td>
                        <a class='btn btn-success' href='change-photo-sub?id=".$value->id."'>Rasm yuklash</a>
                    </td>";
                       
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo  "<td class='text-center' colspan='6'>Ma'lumot yo'q</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>

</div>
