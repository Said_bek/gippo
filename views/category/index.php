<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategoriyalar';
?>
<div class="category-index">
        <div class="row">
            <div class="col-md-6 p-0">
                <h2 style="margin:0;" ><?= Html::encode($this->title) ?></h2>
            </div>
            <div class="col-md-6">
                <p>
                    <?= Html::a('Kategoriya kiritish', ['create'], ['class' => 'pull-right btn btn-success']) ?>
                </p>
            </div>
        </div>
        <div class="row" style="margin-top: 12px;">
            <div class="col-md-12"> 
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [ 
                            'attribute' =>'title_uz',
                            'label'  => 'Kategoriya nomi'
                        ],
                        [ 
                            'attribute' =>'title_ru',
                            'label'  => 'Kategoriya Nomi ru'
                        ],
                        [
                            'format' => 'html',
                            'label' => 'Holat',
                            'value' => function ($data) {
                                if ($data->status == 1) {
                                    return '<span class=\'badge\' style=\'background:#17a2b8;\'>Active</span>';
                                } else if ($data->status == 2) {
                                    return '<span class=\'badge badge-secondary\'>Arxiv</span>';
                                }
                            },
                        ],


                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
</div>
