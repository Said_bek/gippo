<div class="holder breadcrumbs-wrap mt-0">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="index.html">
                <?php 
                    echo Yii::t("app","home");
                ?>
            </a></li>
            <li><span>
                <?php 
                    echo Yii::t("app","my_cabinet");
                ?>  
            </span></li>
        </ul>
    </div>
</div>
<div class="holder">
    <div class="container">
        <div class="row">
            <div class="col-md-4 aside aside--left">
                <div class="list-group">
                    <a href="account-details" class="list-group-item">
                        <?php 
                            echo Yii::t("app","my_informations");
                        ?>
                    </a>
                    <a href="account-address" class="list-group-item">
                        <?php 
                            echo Yii::t("app","my_address");
                        ?>
                    </a>
                    <a href="account-wishlist" class="list-group-item">
                        <?php 
                            echo Yii::t("app","favorite_products");
                        ?>
                    </a>
                    <a href="account-history" class="list-group-item">
                        <?php 
                            echo Yii::t("app","order_history");
                        ?>
                    </a>    
                    <a href="account-referal" class="list-group-item active">
                        <?php 
                            echo Yii::t("app","referal");
                        ?>
                    </a>
                </div>
            </div>
            <div class="col-md-14 aside ">
                <h3 style="margin:0;">
                    <label>Web Site</label>
                    <div class="form-group d-flex" style="justify-content: space-between;">
                        <input maxlength="3000" style="width: 80%;" readonly class="form-control form-control--sm" type="text" value="<?php  echo $link ?>" id="referal">
                        <button style="width: 18%;" title="" class="btn btn-primary ml-auto" onclick="myFunction()"><?php echo Yii::t("app","copy") ?></button>
                    </div>
                    <label>Telegram bot</label>
                    <div class="form-group d-flex" style="justify-content: space-between;">
                        <input maxlength="3000" style="width: 80%;" readonly class="form-control form-control--sm" type="text" value="<?php  echo $link_tg ?>" id="referal_tg">
                        <button style="width: 18%;" title="" class="btn btn-primary ml-auto" onclick="myFunctionTwo()"><?php echo Yii::t("app","copy") ?></button>
                    </div>
                </h3>
                <br>
            </div>
        </div>
    </div>
</div>