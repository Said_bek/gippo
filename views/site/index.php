<?php

use app\models\Products;
use app\models\ProductDetailsCode;
use yii\helpers\Url;
// echo "<pre>";
// print_r($sale_products);
// die();

?>

<div class="page-content">
    <!-- Main Slider -->
    <div class="holder fullwidth full-nopad mt-0">
        <div class="container">
            <div class="bnslider-wrapper">
                <div class="bnslider bnslider--lg keep-scale" id="bnslider-001" data-slick='{"arrows": true, "dots": true}' data-autoplay="false" data-speed="12000" data-start-width="1917" data-start-height="764" data-start-mwidth="1550" data-start-mheight="1000">
                    <?php foreach ($carusel as $key => $value) { ?>
                        <div class="bnslider-slide">
                            <div class="bnslider-image-mobile lazyload" style="width:100%;" data-bgset="/uploads/<?php echo $value->img ?>"></div>
                            <div class="bnslider-image lazyload" style="width:100%;" data-bgset="/uploads/<?php echo $value->img ?>"></div>
                            <div class="bnslider-text-wrap bnslider-overlay ">
                                <div class="bnslider-text-content txt-middle txt-right txt-middle-m txt-center-m">
                                    <div class="bnslider-text-content-flex ">
                                        <div class="bnslider-vert w-s-60 w-ms-100" style="padding: 0px">
                                            <div class="bnslider-text order-1 mt-sm bnslider-text--md text-center data-ini" data-animation="fadeInUp" data-animation-delay="800" data-fontcolor="#282828" data-fontweight="700" data-fontline="1.5">
                                                <?php 
                                                    if (Yii::$app->language == 'uz'){
                                                        echo $value->title_uz;
                                                    } else {
                                                        echo $value->title_ru;
                                                    }
                                                ?> 
                                             </div>
                                            <div class="bnslider-text order-2 mt-sm bnslider-text--xs text-center data-ini" data-animation="fadeInUp" data-animation-delay="1000" data-fontcolor="#7c7c7c" data-fontweight="400" data-fontline="1.5"><?php echo $value->description_uz ?></div>
                                            <div class="btn-wrap text-center  order-4 mt-md" data-animation="fadeIn" data-animation-delay="2000" style="opacity: 1;">
                                                <a href="https://bit.ly/3eJX5XE" target="_blank" class="btn">
                                                    Shop now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                
                </div>
                <div class="bnslider-arrows container-fluid">
                    <div></div>
                </div>
                <div class="bnslider-dots container-fluid"></div>
            </div>
        </div>
    </div>
    <div class="holder holder-mt-xsmall">
        <div class="container-fluid">
            <div class="row vert-margin-small">
            <?php foreach ($category as $key => $value): ?>
                <div class="col-sm">
                    <a href="category.html" class="collection-grid-3-item image-hover-scale">
                        <div class="collection-grid-3-item-img image-container" style="padding-bottom: 68.22%">
                            <img src="data:/uploads/site/<?php echo $value["imgs"] ?>" data-src="/uploads/site/<?php echo $value["imgs"] ?>" class="lazyload fade-up" alt="Banner">
                            <div class="foxic-loader"></div>
                        </div>
                        <div class="collection-grid-3-caption-bg">
                            <h3 class="collection-grid-3-title">
                                <?php if (Yii::$app->language == "uz"): ?>
                                    <?php echo $value["title_uz"] ?>
                                <?php else: ?>
                                    <?php echo $value["title_ru"] ?>
                                <?php endif ?>
                            </h3>
                            <!-- <h4 class="collection-grid-3-subtitle">The&nbsp;Best&nbsp;Look&nbsp</h4> -->
                        </div>
                    </a>
                </div>    
            <?php endforeach ?>
        </div>
        </div>
    </div>
    <div class="holder holder-mt-medium">
        <div class="container">
            <div class="title-tabs" style="width:100%;text-align: center;">
                <h2>
                    <?php echo Yii::t('app', 'sponsors'); ?>
                </h2>
            </div>
            <ul class="brand-grid flex-wrap justify-content- js-color-hover-brand-grid">
                <?php foreach ($sponsors as $key => $value) { ?>
                    <li>
                        <a href="#" target="_self" class="d-block image-container" title="Brand">
                            <img class="lazyload fade-up" src="" data-src="/uploads/<?php echo $value["img"] ?>" alt="Brand">
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <div class="text-center mt-3 d-md-none">
                <a href="#" class="btn btn--grey brands-show-more js-brands-show-more"><span>Show More</span><span>Show Less</span></a>
            </div>
        </div>
    </div>
    <div class="holder holder-mt-medium section-name-products-grid" id="productsGrid01">
        <div class="container">
            <div class="title-wrap text-center">
                <h2 class="h1-style">
                    <?php echo Yii::t('app', 'new_products'); ?>
                </h2>
                <div class="title-wrap title-tabs-wrap text-center js-title-tabs">
                    <div class="title-tabs">
                        <?php foreach ($category as $key => $value): ?>
                            <h2 class="h3-style">
                                <a data-id="<?php echo "best_saler_".$value["id"] ?>" class="change_tab " href="ajax/ajax-product-tab-01.json" data-total="8" data-loaded="4" data-grid-tab-title>
                                    <span class="change_tab title-tabs-text theme-font">
                                        <?php if (Yii::$app->language == "uz"): ?>
                                            <?php echo $value["title_uz"] ?>
                                        <?php else: ?>
                                            <?php echo $value["title_ru"] ?>
                                        <?php endif ?>
                                    </span>
                                </a>
                            </h2>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="prd-grid-wrap">
                <div class="prd-grid data-to-show-4 data-to-show-md-3 data-to-show-sm-2 data-to-show-xs-2" data-grid-tab-content></div>
                <div class="loader-horizontal-sm js-loader-horizontal-sm d-none" data-loader-horizontal style="opacity: 0;"><span></span></div>
                <!--<div class="circle-loader-wrap d-none">-->
                    <!--<div class="circle-loader">-->
                        <!--<a href="" data-load="4" class="js-circle-loader">-->
                            <!--<svg id="svg_d" version="1.1" xmlns="http://www.w3.org/2000/svg">-->
                                <!--<circle cx="50%" cy="50%" r="63" fill="transparent"></circle>-->
                                <!--<circle class="js-circle-bar" cx="50%" cy="50%" r="63" fill="transparent"></circle>-->
                            <!--</svg>-->
                            <!--<svg id="svg_m" version="1.1" xmlns="http://www.w3.org/2000/svg">-->
                                <!--<circle cx="50%" cy="50%" r="50" fill="transparent"></circle>-->
                                <!--<circle class="js-circle-bar" cx="50%" cy="50%" r="50" fill="transparent"></circle>-->
                            <!--</svg>-->
                            <!--<div class="circle-loader-text">Load More</div>-->
                            <!--<div class="circle-loader-text-alt"><span class="js-circle-loader-start"></span>&nbsp;out of&nbsp;<span class="js-circle-loader-end"></span></div>-->
                        <!--</a>-->
                    <!--</div>-->
                <!--</div>-->
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="prd-grid-wrap position-relative">
                <?php $i = 1; ?>
                <?php foreach ($best_saler as $key_product => $value_product): ?>
                    <div class="<?php if ($i != 1): ?><?php echo "d-none best_saler_".$key_product." "?><?php else: echo "best_saler_".$key_product." " ?><?php endif ?> best_saler prd-grid data-to-show-4 data-to-show-lg-4 data-to-show-md-3 data-to-show-sm-2 data-to-show-xs-2 js-category-grid best_saler_products" data-grid-tab-content>
                        <?php foreach ($value_product as $key => $value) { ?>
                            <?php $product = Products::getByDetailId($color_id,$value->id); ?>
                            <?php if (Yii::$app->language == "uz"): ?>
                                <?php $title = $value["title_uz"] ?>
                                <?php $description = $value["description_uz"] ?>
                            <?php else: ?>
                                <?php $title = $value["title_ru"] ?>
                                <?php $description = $value["description_ru"] ?>
                            <?php endif ?>
                            <?php $sum_text = ''; ?>
                            <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                <?php $sum_text = Yii::t("app","usd"); ?>
                            <?php endif ?>
                            <?php if (!isset($_SESSION['currency']) || $_SESSION['currency'] == 'uzs'): ?>
                                <?php $sum_text = Yii::t("app","sum"); ?>
                            <?php endif ?>
                            <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                                <div class="prd-inside">
                                    <div class="prd-img-area">
                                        <a href="<?php echo Url::toRoute('site/products?id='.$value->id) ?>" class="prd-img image-hover-scale image-container">
                                            <img src="<?php echo $value->img ?>" data-src="<?php echo $value->img ?>" alt="Midi Dress with Belt" class="js-prd-img lazyload fade-up">
                                            <div class="foxic-loader"></div>
                                            <div class="prd-big-squared-labels">
                                                
                                                
                                                
                                            </div>
                                        </a>
                                        <div class="prd-circle-labels">
                                            <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" id="add_wishlist" data-id="<?php echo $value["id"] ?>" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                            <?php $sale = $value->sale; ?>
                                            <?php if (isset($sale) and !empty($sale)): ?>
                                                <?php $save_money = $value["price"] - $sale->sale_price ?>
                                            <?php endif ?>
                                            <a 
                                                href="#" 
                                                data-img="<?php echo $value["img"] ?>" 
                                                data-product_name="<?php echo $title ?>" 
                                                data-desc='<?php echo $description ?>'
                                                data-price="<?php echo number_format($value["price"],0,'',' ') ?>" 
                                                data-sale_price="<?php if (isset($sale) and !empty($sale)): ?><?php echo  number_format($sale->sale_price,0,'',' ') ?><?php endif ?>"
                                                data-pracent="<?php if (isset($sale) and !empty($sale)): ?><?php echo  number_format($sale->sale_pracent,0,'',' ') ?><?php endif ?>"
                                                data-category="<?php echo $value->category->title_uz ?>"
                                                data-brand=" <?php echo $value["made"] ?>"
                                                data-sku="<?php echo $value["sku"] ?>"
                                                data-sum-text="<?php echo $sum_text ?>"
                                                data-save_money="<?php echo $sum_text ?>"

                                                data-id="<?php echo $value["id"] ?>" 
                                                id="open_quick_view_form" 
                                                data-fancybox=""  
                                                data-src="#contactModal" 
                                                class="circle-label-qview  prd-hide-mobile" 
                                            >
                                                    <i class="icon-eye"></i>
                                                    <span>QUICK VIEW</span>
                                            </a>
                                            <?php if (count($product) > 1): ?>
                                                <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                    <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                    <ul>
                                                        <?php foreach ($product as $key2 => $value2): ?>
                                                            <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                            <li data-image="<?php if (isset($product_img) && !empty($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                                <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                    <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                                </a>
                                                            </li>
                                                        <?php endforeach ?>
                                                        
                                                    </ul>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                        
                                        <ul class="list-options color-swatch">
                                            <?php $i = 1; ?>
                                            <?php foreach ($value->imgs as $key2 => $value2): ?>
                                                <?php if ($i == 1): ?>
                                                    <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                <?php else: ?>
                                                    
                                                    <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                                <?php endif ?>
                                                <?php $i++; ?>
                                            <?php endforeach ?>
                                            
                                            
                                            
                                            
                                        </ul>
                                        
                                    </div>
                                    <div class="prd-info">
                                        <div class="prd-info-wrap">
                                            <div class="prd-info-top">
                                                <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                            </div>
                                            <div class="prd-rating justify-content-center"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                            <div class="prd-tag"><a href="#">
                                                <?php echo $value->subCategory->title_uz ?>
                                            </a></div>
                                            <h2 class="prd-title"><a href="product.html">
                                                <?php
                                                    $name = '';
                                                 if (Yii::$app->language == "uz"): ?>
                                                    <?php echo $name = $value["title_uz"] ?>
                                                <?php else: ?>
                                                    <?php echo $name = $value["title_ru"] ?>
                                                <?php endif ?>

                                            </a></h2>
                                            <div class="prd-description">
                                                <?php if (Yii::$app->language == "uz"): ?>
                                                    <?php echo $value["description_uz"] ?>
                                                <?php else: ?>
                                                    <?php echo $value["description_ru"] ?>
                                                <?php endif ?>
                                            </div>
                                            <div class="prd-action">
                                                <form action="#">
                                                    <button class="btn js-prd-addtocart four" 
                                                    data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}' 
                                                    product-name='<?php echo $name ?>'
                                                    product-price='<?php echo $value['price']; ?>'
                                                    product-img='<?php echo $value['img']; ?>'>
                                                        <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="prd-hovers">
                                            <div class="prd-circle-labels">
                                                <div>
                                                    <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" id="add_wishlist" data-id="<?php echo $value->id ?>" title="Add To Wishlist"><i class="icon-heart-stroke"></i>
                                                    </a>
                                                    <a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                                </div>
                                                <div class="prd-hide-mobile"
                                                ><a href="#" data-id="<?php echo $value->id ?>" id="open_quick_view_form" data-fancybox=""  data-src="#contactModal" class="circle-label-qview  prd-hide-mobile"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                            </div>
                                            <div class="prd-price">
                                                
                                                <div class="price-new">
                                                    <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                                        <?php echo price($value['dollar']); ?>
                                                    <?php else: ?>
                                                        <?php echo price($value['price']); ?>
                                                    <?php endif ?>
                                                    
                                                    
                                                </div>
                                            </div>
                                            <div class="prd-action">
                                                <div class="prd-action-left">
                                                    <form action="#">
                                                        <button class="btn js-prd-addtocart one"
                                                        product-id="<?php echo $value->id ?>" 
                                                        data-product='{"name": "<?php echo $name ?>", "path":"<?php echo $value['img'] ?>", "url":"product.html", "aspect_ratio":0.778}' 
                                                        product-name='<?php echo $name ?>'
                                                        product-price='<?php echo $value['price']; ?>'
                                                        product-img='<?php echo $value['img']; ?>'>
                                                            <?php echo Yii::t('app', 'add_to_cart'); ?>                 
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php $i++; ?>
                <?php endforeach ?>
            </div>
        </div>
    </div>
    
    <div id="contactModal" style="display: none; background: none!important;"
         class="modal-info-content modal-info-content-sm">
        <div class="modal--quickview modal-quickview--classic fancybox-content" id="modalQuickView">
            <div class="modal-content">
                <div class="prd-block prd-block--prv-bottom" id="prdGalleryModal">
                    <div class="row no-gutters" style="padding-bottom:55px;">
                        <div class="col-lg-9 quickview-gallery">
                            <div class="prd-block_main-image mt-0">
                                <div class="prd-block_main-image-holder">
                                    <div id="big_carusel" class="product-main-carousel js-product-main-carousel-qw js-product-main-zoom-container"
                                         data-zoom-position="inner">
                                        
                                    </div>
                                </div>
                                <div class="prd-block_viewed-wrap">
                                    <div class="prd-block_viewed">
                                        <i class="icon-watch"></i>
                                        <span><span class="quickview-hidden">This product was</span>Viewed 25 times within 24 hours</span>
                                    </div>
                                    <div class="prd-block_viewed prd-block_viewed--real-time">
                                        <div class="prd-block_visitiors">Real time <span
                                                class="prd-block_visitiors-count js-visitors-now" data-vmax="100"
                                                data-vmin="10">21</span> visitor right now!
                                        </div>
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="product-previews-wrapper">
                            </div>
                        </div>
                        <div class="col-lg-9 quickview-info">
                            <div class="prd-block_info prd-block_info--style2">
                                <div class="prd-block_title-wrap">
                                    <h1 class="prd-block_title" id="pr_name">
                                        
                                    </h1>
                                </div>
                                <div class="prd-block_price" id="price_block" style="display: block!important;">
                                    
                                </div>
                                <div class="prd-block_description prd-block_info_item">
                                    <p id="product_desc">
                                        test
                                    </p>
                                    <div class="mt-1"></div>
                                    <div class="row vert-margin-less">
                                        <div class="col-sm">
                                            <ul class="list-marker">
                                                <li><?php echo Yii::t('app', 'sale'); ?>: <?php echo Yii::t('app', 'has'); ?></li>
                                                <li><?php echo Yii::t('app', 'collection'); ?>:
                                                    <span id="category"></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm">
                                            <ul class="list-marker">
                                                <li >
                                                    Sku:<span id="sk"></span>
                                                </li>
                                                <li id="made"><?php echo Yii::t("app","brend") ?>  </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="prd-block_options">
                                    <div class="prd-color swatches">
                                        <div class="option-label">Color:</div>
                                        <select class="select_colors_js form-control hidden single-option-selector-modalQuickView" id="SingleOptionSelector-2" data-index="option1">
 
                                        </select>
                                        <ul id="img_ul_li_js" class="images-list js-size-list" data-select-id="SingleOptionSelector-2">
                                            
                                        </ul>
                                    </div>
                                    <div class="prd-size swatches">
                                        <div class="option-label">Size:</div>
                                        <select class="form-control hidden single-option-selector-modalQuickView"
                                                id="SingleOptionSelector-3" data-index="option2">
                                            <option value="Small" selected="selected">Small</option>
                                            <option value="Medium">Medium</option>
                                            <option value="Large">Large</option>
                                        </select>
                                        <ul class="size-list js-size-list" data-select-id="SingleOptionSelector-3">
                                            <li class="active"><a href="#" data-value="Small"><span
                                                    class="value">Small</span></a></li>
                                            <li><a href="#" data-value="Medium"><span class="value">Medium</span></a></li>
                                            <li><a href="#" data-value="Large"><span class="value">Large</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="prd-block_actions prd-block_actions--wishlist">
                                    <div class="prd-block_qty">
                                        <div class="qty qty-changer">
                                            <button class="decrease js-qty-button"></button>
                                            <input type="number" class="qty-input" name="quantity" value="1" data-min="1"
                                                   data-max="1000">
                                            <button class="increase js-qty-button"></button>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <button class="btn btn--add-to-cart js-prd-addtocart"
                                                data-product='{"name":"Leather Pegged Pants ", "url": "product.html", "path": "images/skins/fashion/products/product-01-1.jpg", "aspect_ratio ": "0.78"}'>
                                            Add to cart
                                        </button>
                                    </div>
                                    <div class="btn-wishlist-wrap">
                                        <a href="#" class="btn-add-to-wishlist ml-auto btn-add-to-wishlist--add js-add-wishlist"
                                           title="Add To Wishlist"><i class="icon-heart-stroke"></i></a>
                                        <a href="#"
                                           class="btn-add-to-wishlist ml-auto btn-add-to-wishlist--off js-remove-wishlist"
                                           title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                    </div>
                                </div>
                                <div class="prd-block_shopping-info-wrap-compact">
                                    <div class="prd-block_shopping-info-compact"><i
                                            class="icon-delivery-truck"></i><span>Fast<br>Shipping</span></div>
                                    <div class="prd-block_shopping-info-compact"><i class="icon-return"></i><span>Easy<br>Return</span>
                                    </div>
                                    <div class="prd-block_shopping-info-compact"><i class="icon-call-center"></i><span>24/7<br>Support</span>
                                    </div>
                                </div>
                                <div class="prd-block_info_item mt-3 row row--sm-pad vert-margin-middle">
                                    <div class="col"><a href="product.html" class="btn btn--grey w-100">View Full Info</a></div>
                                    <div class="col"><a href="product.html" class="btn btn--grey w-100" data-fancybox-close>Close</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <style>
        .modal--quickview .prd-block--prv-bottom .product-previews-carousel {
            padding: 0;
        }

        body:not(.equal-height) .modal--quickview .product-previews-carousel a > span {
            display: block;
            position: relative;
            border: 0;
            height: 0;
            overflow: hidden;
            padding-bottom: 128%;
        }

        body:not(.equal-height) .modal--quickview .product-previews-carousel a > span > span {
            position: absolute;
            display: flex;
            justify-content: center;
            align-items: center;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        body:not(.equal-height) .modal--quickview .product-previews-carousel a > span img {
            position: absolute;
            width: 100%;
            height: 100%;
            object-fit: contain;
        }

        .modal--quickview.modal-quickview--classic .product-previews-carousel {
            height: 0;
            position: relative;
            transition: 0s;
        }

        @media (min-width: 480px) {
            .modal--quickview.modal-quickview--classic .product-previews-carousel {
                padding-bottom: calc(38%  10px);
            }
        }

        @media (max-width: 479px) {
            .modal--quickview.modal-quickview--classic .product-previews-carousel {
                padding-bottom: calc(38%  10px);
            }
        }

        @media (min-width: 992px) {
            .modal--quickview .prd-block--no-previews .prd-block_info-bottom, .modal--quickview .prd-block--no-previews .prd-block_info-top {
                width: calc(100% - 50% - 20px);
            }

            .modal--quickview.modal-quickview--classic .prd-block_info-bottom {
                width: calc(100% - 50% - 20px);
            }

            .modal--quickview.modal-quickview--classic .quickview-info {
                min-height: 65vh;
            }
        }
    </style>
</div>
        
    </div>
    <div id="shopify-section-1586608150816" class="shopify-section index-section index-section--flush">
        <div class="container">
            <div class="holder fullwidth fullboxed mt-0 full-nopad">
                <div class="container">
                    <div class="bnslider-wrapper">
                        <div class="bnslider keep-scale" data-start-width='1920'  data-start-height='785' data-start-mwidth='414'  data-start-mheight='736' id="bnslider-1586608150816" data-autoplay="true" data-speed="5000" >
                            <?php foreach ($trend as $key => $value) { ?>
                                <a href="#" target="_self" class="bnslider-slide ">
                                    <div class="bnslider-image-mobile lazyload fade-up-fast" data-bgset="<?php echo $value->img ?>" data-sizes="auto"></div>
                                    <div class="bnslider-image lazyload fade-up-fast" data-bgset="<?php echo $value->img ?>" data-sizes="auto"></div>
                                    <div class="bnslider-text-wrap bnslider-overlay container">
                                        <div class="bnslider-text-content txt-middle txt-left txt-middle-m txt-center-m">
                                            <div class="bnslider-text-content-flex  container ">
                                                <div class="bnslider-vert w-s-60 w-ms-80 p-0">
                                                    <div class="bnslider-text order-2 mt-sm bnslider-text--xl text-center" data-animation="fadeInUp" data-animation-delay="800" data-fontcolor="#363b4b" data-bgcolor="" data-fontweight="600" data-fontline="1.00" data-otherstyle="">
                                                       <h1>
                                                            <?php 
                                                                if (Yii::$app->language == "uz") {
                                                                    echo $value->title_uz;
                                                                } else {
                                                                    echo $value->title_ru;
                                                                }
                                                            ?>
                                                        </h1>
                                                    </div>
                                                    <div class="bnslider-text order-3 mt-sm bnslider-text--sm text-center" data-animation="zoomIn" data-animation-delay="1000" data-fontcolor="#ffffff" data-bgcolor="" data-fontweight="500" data-fontline="1.5" data-otherstyle="">
                                                        <?php 
                                                            if (Yii::$app->language == "uz") {
                                                                echo $value->description_uz;
                                                            } else {
                                                                echo $value->description_ru;
                                                            }
                                                        ?>
                                                    </div>
                                                    <div class="bnslider-text order-1 mt-0 bnslider-text--md text-center" data-animation="fadeInDown" data-animation-delay="1600" data-fontcolor="#5378f4" data-bgcolor="" data-fontweight="600" data-fontline="1.00" data-otherstyle="">
                                                        <?php if (isset($_SESSION['currency']) && $_SESSION['currency'] == 'usd'): ?>
                                                            <?php echo price($value['dollar']); ?>
                                                        <?php else: ?>
                                                            <?php echo price($value['price']); ?>
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="btn-wrap text-center  order-4 mt-md"
                                                         data-animation="fadeInUp"
                                                         data-animation-delay="2000">
                                                        <div class="btn btn--lg">Shop now</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="bnslider-loader"></div>
                        <div class="bnslider-arrows d-sm-none container"><div></div></div>
                        <div class="bnslider-dots d-none d-sm-block container"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="holder">
        <div class="container">
            <div class="title-wrap text-center"><h2 class="h1-style">
                <?php if (Yii::$app->language == 'uz'): ?>
                    Chegirmadagi Mahsulotlar
                <?php else: ?>
                    Товары со скидкой
                <?php endif ?>
            </h2>
                <div class="h-sub maxW-825">
                    <?php if (Yii::$app->language == 'uz'): ?>
                        Shoshiling, mahsulotlar cheklangan
                    <?php else: ?>
                        Торопитесь, количество товаров ограничено
                    <?php endif ?>
                    
                    
                </div>
            </div>
            <div class="prd-grid-wrap position-relative">
                <div class="prd-grid data-to-show-4 data-to-show-lg-4 data-to-show-md-3 data-to-show-sm-2 data-to-show-xs-2 js-category-grid" data-grid-tab-content>
                    <?php foreach ($sale_products as $key => $value): ?>
                        <?php $product = Products::getByDetailId($color_id,$value->product["id"]); ?>
                        <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="<?php echo Url::toRoute('site/products?id='.$value->id) ?>" class="prd-img image-hover-scale image-container">
                                        <img src="<?php echo $value->product["img"] ?>" data-src="<?php echo $value->product["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                        <div class="foxic-loader"></div>
                                        <div class="prd-big-squared-labels">
                                            
                                            <?php if ($value->product["status"] == 3 or $value->product["status"] == 4): ?>
                                                <div class="label-new">
                                                    <span>
                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                            Yangi
                                                        <?php else: ?>
                                                            Новый
                                                        <?php endif ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                            <div class="label-sale">
                                                <span><?php echo "-".round($value["sale_pracent"])."%" ?>
                                                    <span class="sale-text">
                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                            Chegirma
                                                        <?php else: ?>
                                                            Скидка
                                                        <?php endif ?>
                                                    </span>
                                                </span>
                                                
                                                <div class="countdown-circle">
                                                    <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                </div>
                                                
                                            </div>
                                            
                                            
                                        </div>
                                    </a>
                                    <div class="prd-circle-labels">
                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" id="add_wishlist" data-id="<?php echo $value->product["id"] ?>" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                        <a href="#" data-id="<?php echo $value->product["id"] ?>" id="open_quick_view_form" data-fancybox=""  data-src="#contactModal" class="circle-label-qview  prd-hide-mobile"><i class="icon-eye"></i><span>QUICK VIEW</span></a>
                                        <?php if (count($product) > 1): ?>
                                            <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                <ul>
                                                    <?php foreach ($product as $key2 => $value2): ?>
                                                        <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                        <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                            <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                    
                                                </ul>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    
                                    <ul class="list-options color-swatch">
                                        <?php $i = 1; ?>
                                        <?php if (isset($value->product->imgs) && !empty($value->product->imgs)): ?>
                                            <?php foreach ($value->product->imgs as $key2 => $value2): ?>
                                                <?php if ($i == 1): ?>
                                                    <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                <?php else: ?>
                                                    <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                <?php endif ?>
                                                <?php $i++; ?>
                                            <?php endforeach ?>
                                        <?php endif ?>

                                    </ul>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-info-wrap">
                                        <div class="prd-info-top">
                                            <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                        </div>
                                        <div class="prd-rating justify-content-center"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                        <div class="prd-tag">
                                            <a href="#">

                                                <?php if (Yii::$app->language == 'uz'): ?>
                                                    <?php echo $value->product->subCategory->title_uz ?>    
                                                <?php else: ?>
                                                    <?php echo $value->product->subCategory->title_ru ?>
                                                <?php endif ?>
                                                
                                            </a>
                                        </div>
                                        <h2 class="prd-title"><a href="<?php echo Url::toRoute('site/products?id='.$value['id']) ?>">
                                            <?php $name = ''; if (Yii::$app->language == "uz"): ?>
                                                <?php echo $name = $value->product["title_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $name = $value->product["title_ru"] ?>
                                            <?php endif ?>
                                        </a></h2>
                                        <div class="prd-description">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                            <?php echo $value->product["description_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $value->product["description_ru"] ?>
                                            <?php endif ?>
                                        </div>
                                        <div class="prd-action">
                                            <form action="#">
                                                <button class="btn js-prd-addtocart two"
                                                data-product='{"name": "Oversized Cotton Blouse", "path":"<?php echo $value->product['img'] ?>", "url":"product.html2", "aspect_ratio":0.778}' 
                                                product-name='<?php echo $name ?>'
                                                product-price='<?php echo $value->product['price']; ?>'
                                                product-img='<?php echo $value->product['img']; ?>'>
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="prd-hovers">
                                        <div class="prd-circle-labels">
                                            <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" id="add_wishlist" data-id="<?php echo $value->product["id"] ?>" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                            <div class="prd-hide-mobile"><a href="#" data-id="<?php echo $value->product["id"] ?>" id="open_quick_view_form" data-fancybox=""  data-src="#contactModal" class="circle-label-qview  prd-hide-mobile"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                        </div>
                                        <div class="prd-price">
                                            <?php if ($value->product->sale2): ?>
                                                <div class="price-old">
                                                    <?php echo price($value->product['price']) ?>
                                                </div>    
                                            <?php endif ?>
                                            <div class="price-new">
                                                <?php echo price($value->product->sale2) ?>
                                            </div>
                                        </div>
                                        <div class="prd-action">
                                            <div class="prd-action-left">
                                                <form action="#">
                                                    <button product-id="<?php echo $value->product->id ?>" class="js-prd-addtocart btn js-prd-addtocart three" 
                                                    data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html2", "aspect_ratio":0.778}' >
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="holder holder-mt-medium">
        <div class="container">
            <div class="title-wrap text-center ">
                <h2 class="h1-style text-center"><a href="blog.html" title="View all">Latest From Blog</a></h2>
                <div class="carousel-arrows" style="margin:0 auto 65px; width:50px;"></div>
            </div>
            <div class="post-prws post-prws-carousel post-prws--row js-post-prws-carousel" data-slick='{"slidesToShow": 3, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 2 }},{"breakpoint": 480,"settings": {"slidesToShow": 1 }}]}'>
                <div class="post-prw-vert col">
                    <a href="blog-post.html" class="post-prw-img image-hover-scale image-container" style="padding-bottom: 54.44%">
                        <img class="fade-up w-100 lazyload" alt="The High-Street Brand Fashion" src="data:/asset/image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="asset/images/skins/fashion/blog/blog-fashion-02.png">
                    </a>
                    <h4 class="post-prw-title"><a href="blog-post.html">The High-Street Brand Fashion</a></h4>
                    <div class="post-prw-links">
                        <div class="post-prw-date"><i class="icon-calendar1"></i>
                            June 9, 2020
                        </div>
                    </div>
                </div>
                <div class="post-prw-vert col">
                    <a href="blog-post.html" class="post-prw-img image-hover-scale image-container" style="padding-bottom: 54.44%">
                        <img class="fade-up w-100 lazyload" alt="The High-Street Brand Fashion" src="data:/asset/image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="asset/images/skins/fashion/blog/blog-fashion-03.png">
                    </a>
                    <h4 class="post-prw-title"><a href="blog-post.html">Trends to Try This Season</a></h4>
                    <div class="post-prw-links">
                        <div class="post-prw-date"><i class="icon-calendar1"></i>
                            June 3, 2020
                        </div>
                    </div>
                </div>
                <div class="post-prw-vert col">
                    <a href="blog-post.html" class="post-prw-img image-hover-scale image-container" style="padding-bottom: 54.44%">
                        <img class="fade-up w-100 lazyload" alt="The High-Street Brand Fashion" src="data:/asset/image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="asset/images/skins/fashion/blog/blog-fashion-04.png">
                    </a>
                    <h4 class="post-prw-title"><a href="blog-post.html">Working From Home</a></h4>
                    <div class="post-prw-links">
                        <div class="post-prw-date"><i class="icon-calendar1"></i>
                            June 1, 2020
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <?php if (isset($blogs) and !empty($blogs)): ?>
        <div class="holder holder-mt-medium">
        <div class="container">
            <div class="title-wrap text-center ">
                <h2 class="h1-style text-center"><a href="blog.html" title="View all">Latest From Blog</a></h2>
                <div class="carousel-arrows" style="margin:0 auto 65px; width:50px;"></div>
            </div>
            <div class="post-prws post-prws-carousel post-prws--row js-post-prws-carousel" data-slick='{"slidesToShow": 3, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 2 }},{"breakpoint": 480,"settings": {"slidesToShow": 1 }}]}'>
                <?php foreach ($blogs as $key => $value): ?>
                    
                    <div class="post-prw-vert col">
                        <a href="blog-post.html" class="post-prw-img image-hover-scale image-container" style="padding-bottom: 54.44%">
                            <img class="fade-up w-100 lazyload" alt="The High-Street Brand Fashion" src="/uploads/<?php echo $value->img ?>" data-src="/uploads/<?php echo $value->img ?>">
                        </a>
                        <h4 class="post-prw-title"><a href="blog-post.html">The High-Street Brand Fashion</a></h4>
                        <div class="post-prw-links">
                            <div class="post-prw-date"><i class="icon-calendar1"></i>
                                June 9, 2020
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>

    <?php endif ?>
</div>
<div class="footer-sticky">
    <div class="popup-addedtocart js-popupAddToCart closed" data-close="50000" style="height: 0px;">
        <div class="container">
            <div class="row">
                <div class="popup-addedtocart-close js-popupAddToCart-close"><i class="icon-close"></i></div>
                <div class="popup-addedtocart-cart js-open-drop" data-panel="#dropdnMinicart"><i class="icon-basket"></i></div>
                <div class="col-auto popup-addedtocart_logo">
                    <img src="../../html/images/logo-white-sm.png" data-src="/html/images/logo-white-sm.png" class="fade-up ls-is-cached lazyloaded" alt="">
                </div>
                <div class="col popup-addedtocart_info">
                    <div class="row">
                        <a href="" class="col-auto popup-addedtocart_image">
                            <span class="image-container w-100" href="product.html" style="">
                                <img src="/html/images/skins/fashion/products/product-06-1.jpg" alt="" style="opacity: 1;">
                            </span>
                        </a>
                        <div class="col popup-addedtocart_text">
                            <a href="" class="popup-addedtocart_title"><b></b></a>
                            <span class="popup-addedtocart_message">
                                <?php if (Yii::$app->language == 'uz'): ?>
                                    <a href="cart" class="underline">Savatchaga</a>
                                    qoshildi
                                <?php else: ?>
                                    Добавлен в 
                                    <a href="cart" class="underline">
                                        корзину
                                    </a>
                                <?php endif ?>
                                </span>
                            <span class="popup-addedtocart_error_message"></span>
                        </div>
                    </div>
                </div>
                <div class="col-auto popup-addedtocart_actions">
                    <span>
                        
                    </span>
                    <a href="<?php echo Url::toRoute('site/cart')?>" class="btn btn--grey btn--sm js-open-drop" data-panel="#dropdnMinicart">
                        <i class="icon-basket"></i>
                        <span>
                            <?php echo Yii::t("app","view_cart") ?>
                        </span>
                    </a> 
                    <span>
                        <?php echo Yii::t("app","or") ?>
                    </span> 
                    <a href="<?php echo Url::to(['/site/checkout']); ?>" class="btn btn--invert btn--sm">
                        <i class="icon-envelope-1"></i>
                        <span>
                            <?php echo Yii::t("app","checkout") ?>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="payment-notification-wrap js-pn" data-visible-time="3000" data-hidden-time="3000" data-delay="500"
         data-from="Aberdeen,Bakersfield,Birmingham,Cambridge,Youngstown"
         data-products='[{"productname":"Leather Pegged Pants", "productlink":"product.html","productimage":"images/skins/fashion/products/product-01-1.jpg"},{"productname":"Black Fabric Backpack", "productlink":"product.html","productimage":"images/skins/fashion/products/product-28-1.jpg"},{"productname":"Combined Chunky Sneakers", "productlink":"product.html","productimage":"images/skins/fashion/products/product-23-1.jpg"}]'>
        <div class="payment-notification payment-notification--squared">
            <div class="payment-notification-inside">
                <div class="payment-notification-container">
                    <a href="#" class="payment-notification-image js-pn-link">
                        <img src="images/products/product-01.jpg" class="js-pn-image" alt="">
                    </a>
                    <div class="payment-notification-content-wrapper">
                        <div class="payment-notification-content">
                            <div class="payment-notification-text">Someone purchased</div>
                            <a href="product.html" class="payment-notification-name js-pn-name js-pn-link">Applewatch</a>
                            <div class="payment-notification-bottom">
                                <div class="payment-notification-when"><span class="js-pn-time">32</span> min ago</div>
                                <div class="payment-notification-from">from <span class="js-pn-from">Riverside</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payment-notification-close"><i class="icon-close-bold"></i></div>
                <div class="payment-notification-qw prd-hide-mobile js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i></div>
            </div>
        </div>
    </div>
</div>