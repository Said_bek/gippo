<?php 

	use app\models\Products;
	use app\models\ProductDetailsCode;
    $session = Yii::$app->session;

?>
<div class="holder">
        <div class="container">
            <!-- Two columns -->
            <!-- Page Title -->
            <div class="page-title text-center">
                <h1>
                    <?php echo Yii::t("app","result_search") ?>
                </h1>
            </div>
            <!-- /Page Title -->
            <!-- Filter Row -->
            <div class="filter-row">
                <div class="row">
                    <div class="items-count">35 item(s)</div>
                    <div class="select-wrap d-none d-md-flex">
                        <div class="select-label">SORT:</div>
                        <div class="select-wrapper select-wrapper-xxs">
                            <?php $session = Yii::$app->session; ?>
                            <?php 
                                $first = '';
                                $second = '';
                                $thirst = '';
                                if (isset($session["list"]["select_search"]) and !empty($session["list"]["select_search"])) {
                                    if ($session["list"]["select_search"] == 1) {
                                        $first = "selected";
                                    } else if ($session["list"]["select_search"] == 2) {
                                        $second = 'selected';
                                    } else if ($session["list"]["select_search"] == 3) {
                                        $thirst = 'selected';
                                    }
                                } else {
                                    $first = "selected";
                                }
                            ?>
                            <select class="form-control input-sm" id="search_type">
                                <option <?php echo $first ?> value="new">
                                    <?php echo Yii::t('app', 'new_products'); ?>
                                </option>
                                <option <?php echo $second ?> value="trend">
                                    <?php echo Yii::t('app', 'trend_products'); ?>
                                </option>
                                <option <?php echo $thirst ?> value="price">
                                    <?php echo Yii::t('app', 'price_products'); ?>
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="viewmode-wrap">
                        <div class="view-mode">
                            <span class="js-horview d-none d-lg-inline-flex"><i class="icon-grid"></i></span>
                            <span class="js-gridview"><i class="icon-grid"></i></span>
                            <span class="js-listview"><i class="icon-list"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Filter Row -->
            <div class="row">
                <!-- Left column -->
             
                <!-- filter toggle -->
                <div class="filter-toggle js-filter-toggle">
                    <div class="loader-horizontal js-loader-horizontal">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated"
                                 style="width: 100%"></div>
                        </div>
                    </div>
                    <span class="filter-toggle-icons js-filter-btn"><i class="icon-filter"></i><i
                            class="icon-filter-close"></i></span>
                    <span class="filter-toggle-text"><a href="#" class="filter-btn-open js-filter-btn">REFINE & SORT</a><a
                            href="#" class="filter-btn-close js-filter-btn">RESET</a><a href="#"
                                                                                        class="filter-btn-apply js-filter-btn">APPLY & CLOSE</a></span>
                </div>
                <!-- /Left column -->
                <!-- Center column -->
                <div class="col-lg aside">
                    <div class="prd-grid-wrap">
                        <!-- Products Grid -->
                        <div class="loader_swap">
                            <div class="prd-grid product-listing data-to-show-4 data-to-show-md-4 data-to-show-sm-1 js-category-grid"
                                 data-grid-tab-content>
                                 <?php $session = Yii::$app->session; ?>
                                <?php foreach ($session["list"]["search"] as $key => $value): ?>
                                    <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
                                    <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                                        <div class="prd-inside">
                                            <div class="prd-img-area">
                                                <a href="product.html" class="prd-img image-hover-scale image-container">
                                                    <img src="data:<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                                    <div class="foxic-loader"></div>
                                                    <div class="prd-big-squared-labels">
                                                        
                                                        <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                                                            <div class="label-new">
                                                                <span>
                                                                    <?php if (Yii::$app->language == 'uz'): ?>
                                                                        Yangi
                                                                    <?php else: ?>
                                                                        Новый
                                                                    <?php endif ?>
                                                                </span>
                                                            </div>
                                                        <?php endif ?>
                                                        <?php $sale_product = $value->sale; ?>
                                                        <?php if (isset($sale_product) and !empty($sale_product)): ?>
                                                            
                                                            <div class="label-sale">
                                                                <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                                                    <span class="sale-text">
                                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                                            Chegirma
                                                                        <?php else: ?>
                                                                            Скидка
                                                                        <?php endif ?>
                                                                    </span>
                                                                </span>
                                                                
                                                                <div class="countdown-circle">
                                                                    <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                                </div>
                                                            </div>
                                                        <?php endif ?>
                                                    </div>
                                                </a>
                                                <div class="prd-circle-labels">
                                                    <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                                    <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                                    <?php if (count($product_block) > 1): ?>
                                                        <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                            <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                            <ul>
                                                                <?php foreach ($product_block as $key2 => $value2): ?>
                                                                    <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                                    <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                                        <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                            <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                                        </a>
                                                                    </li>
                                                                <?php endforeach ?>
                                                                
                                                            </ul>
                                                        </div>
                                                    <?php endif ?>
                                                </div>

                                                
                                                <ul class="list-options color-swatch">
                                                    <?php $i = 1; ?>
                                                    <?php foreach ($value->imgs as $key2 => $value2): ?>
                                                        <?php if ($i == 1): ?>
                                                            <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                                        <?php else: ?>
                                                            <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                                        <?php endif ?>
                                                        <?php $i++; ?>
                                                    <?php endforeach ?>

                                                </ul>
                                            </div>
                                            <div class="prd-info">
                                                <div class="prd-info-wrap">
                                                    <div class="prd-info-top">
                                                        <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                                    </div>
                                                    <?php $stars =  $value->stars; ?>
                                                    <div class="prd-rating justify-content-center">
                                                        <?php for ($i = 1; $i <= $stars; $i++): ?>
                                                            <i class="icon-star-fill fill"></i>
                                                        <?php endfor ?>
                                                    </div>
                                                    <div class="prd-tag"><a href="#">
                                                        <?php if (Yii::$app->language == "uz"): ?>
                                                                <?php echo $value->subCategory->title_uz ?>
                                                            <?php else: ?>
                                                                <?php echo $value->subCategory->title_ru ?>
                                                        <?php endif ?>
                                                    </a></div>
                                                    <h2 class="prd-title"><a href="product.html">
                                                        <?php if (Yii::$app->language == "uz"): ?>
                                                            <?php echo $value["title_uz"] ?>
                                                        <?php else: ?>
                                                            <?php echo $value["title_ru"] ?>
                                                        <?php endif ?>
                                                    </a></h2>
                                                    <div class="prd-description">
                                                        Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                                    </div>
                                                    <div class="prd-action">
                                                        <form action="#">
                                                            <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="prd-hovers">
                                                    <div class="prd-circle-labels">
                                                        <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                                        <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                                    </div>
                                                    <div class="prd-price">
                                                        <?php $product_sale = $value->sale; ?>
                                                        <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                                            <div class="price-old"><?php echo $value["price"] ?></div>
                                                            
                                                            <div class="price-new"><?php echo $product_sale["sale_price"] ?></div>
                                                            
                                                            <?php else: ?>
                                                            <div class="price-new"><?php echo $value["price"] ?></div>
                                                        <?php endif ?>
                                                    </div>
                                                    <div class="prd-action">
                                                        <div class="prd-action-left">
                                                            <form action="#">
                                                                <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                                <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>

                            <div id="search_block_<?php echo $session["list"]["last_id"] ?>" class="prd-grid product-listing data-to-show-4 data-to-show-md-4 data-to-show-sm-1 js-category-grid"
                                 data-grid-tab-content></div>
                        </div>
                        <div class="loader-horizontal-sm js-loader-horizontal-sm d-none" data-loader-horizontal
                             style="opacity: 0;"><span></span></div>
                        <?php $next_product = Products::hasProduct($session["list"]["last_id"]); ?>
                        <?php if (isset($next_product) and !empty($next_product)): ?>
                            <div class="circle-loader-wrap">
                                <div class="circle-loader">
                                    <a id="load_more" data-id="<?php echo $session["list"]["last_id"] ?>">
                                        <svg id="svg_d" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="50%" cy="50%" r="63" fill="transparent"></circle>
                                            <circle class="js-circle-bar" cx="50%" cy="50%" r="63"
                                                    fill="transparent"></circle>
                                        </svg>
                                        <svg id="svg_m" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                            <circle cx="50%" cy="50%" r="50" fill="transparent"></circle>
                                            <circle class="js-circle-bar" cx="50%" cy="50%" r="50"
                                                    fill="transparent"></circle>
                                        </svg>
                                        <div class="circle-loader-text">
                                            <?php 
                                                echo Yii::t("app","again");
                                            ?>
                                        </div>
                                        <div class="circle-loader-text-alt"><span class="js-circle-loader-start"></span>&nbsp;out
                                            of&nbsp;<span class="js-circle-loader-end"></span></div>
                                    </a>
                                </div>
                            </div>
                        <?php endif ?>
                        <!-- /Products Grid -->
                        <!--<div class="mt-2">-->
                        <!--<button class="btn" onclick="THEME.loaderHorizontalSm.open()">Show Small Loader</button>-->
                        <!--<button class="btn" onclick="THEME.loaderHorizontalSm.close()">Hide Small Loader</button>-->
                        <!--</div>-->
                        <!--<div class="mt-2">-->
                        <!--<button class="btn" onclick="THEME.loaderCategory.open()">Show Opacity</button>-->
                        <!--<button class="btn" onclick="THEME.loaderCategory.close()">Hide Opacity</button>-->
                        <!--</div>-->
                    </div>
                </div>
                <!-- /Center column -->
            </div>
            <!-- /Two columns -->
        </div>
    </div>

<?php //if (isset($session["list"]["sql"])): ?>
    <?php// echo $session["list"]["sql"] ?>
<?php //else: ?>
    <?php //echo  "fail" ?>
<?php //endif ?>