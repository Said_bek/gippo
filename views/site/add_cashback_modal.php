<div class="form-group">
    <label class="control-label" for="cashback_parcent">Cashback foizi</label>
    <?php if ($value_cashback != 0): ?>
        <input type="integer" value="<?php echo $value_cashback ?>" id="cashback_parcent" class="form-control">
    <?php else: ?>        
        <input type="integer" id="cashback_parcent" class="form-control">
    <?php endif ?>
    <small class='hidden cashback_error'>Maydon bo`sh bo`lishi mumkin emas</small>
    <br>
    <div class="help-block"></div>
</div>
<style type="text/css">
    .hidden{
        display: none;
    }
    .name_error {
        color: red;
        font-size:15px;

    }
    .name_error_ru {
        color: red;
        font-size:15px;
        
    }
    .border-red{
        border:1px solid red;
        border-radius:3px;
    }

</style>

