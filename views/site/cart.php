<?php  
	use yii\helpers\Url;
	use app\models\Products;
	use app\models\ProductDetailsCode;
?>
<div class="holder breadcrumbs-wrap mt-0">
	<div class="container">
		<ul class="breadcrumbs">
			<li>
				<a href="<?php echo Url::toRoute('site/index')?>">
					<?php echo Yii::t('app', 'home'); ?>
				</a>
			</li>
			<li>
				<span>
					<?php echo Yii::t('app', 'cart'); ?>	
				</span>
			</li>
		</ul>
	</div>
</div>
<div class="holder">
	<div class="container">
		<div class="page-title text-left">
			<h1>
				<?php echo Yii::t('app', 'cart'); ?>
			</h1>
			<hr>
		</div>
		<div class="row">
			<div class="col-lg-11 col-xl-13">
				<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
					<div class="cart-table">
						<div class="cart-table-prd cart-table-prd--head py-1 d-none d-md-flex">
							<div class="cart-table-prd-image text-center">
								<?php echo Yii::t('app', 'img'); ?>
							</div>
							<div class="cart-table-prd-content-wrap">
								<div class="cart-table-prd-info">
									<?php echo Yii::t('app', 'name_product'); ?>
								</div>
								<div class="cart-table-prd-qty">
									<?php echo Yii::t('app', 'qty'); ?>
								</div>
								<div class="cart-table-prd-price">
									<?php echo Yii::t('app', 'price'); ?>
								</div>
								<div class="cart-table-prd-action">&nbsp;</div>
							</div>
						</div>
						
						<?php foreach ($_SESSION['cart'] as $key => $value): ?>
							<div class="cart-table-prd">
								<div class="cart-table-prd-image">
									<a href="<?php echo Url::toRoute('site/products?id='.$value['id'])?>" class="prd-img"><img class="lazyload fade-up" src="<?php echo $value['img'] ?>" data-src="<?php echo $value['img'] ?>" alt=""></a>
								</div>
								<div class="cart-table-prd-content-wrap">
									<div class="cart-table-prd-info">
										<div class="cart-table-prd-price flex-column">
											<?php if ($value['sale_price'] != 0): ?>
												<div class="price-old">
													<?php echo price($value['price']) ?>
												</div>
												<div class="price-new">
													<?php echo price($value['sale_price']) ?>
												</div>
											<?php else: ?>
												<div class="price-new">
													<?php echo price($value['price']) ?>
												</div>
											<?php endif ?>
										</div>
										<h2 class="cart-table-prd-name">
											<a href="<?php echo Url::toRoute('site/products?id='.$value['id'])?>">
												<?php if (Yii::$app->language == 'uz'): ?>
													<?php echo $value['title_uz'] ?>
												<?php else: ?>
													<?php echo $value['title_ru'] ?>
												<?php endif ?>
											</a>
										</h2>
									</div>
									<div class="cart-table-prd-qty">
										<div class="qty qty-changer">
											<button product-id="<?php echo $value['id'] ?>" class="decrease"></button>
											<input disabled type="text" class="qty-input" value="<?php echo $value['count'] ?>" data-min="0" data-max="1000">
											<button product-id="<?php echo $value['id'] ?>" class="increase"></button>
										</div>
									</div>
									<div class="cart-table-prd-price-total">
										<?php echo price($value['total_price']) ?>
									</div>
								</div>
								<div class="cart-table-prd-action">
									<a class="cart-table-prd-remove js-product-remove" product-id="<?php echo $value['id'] ?>" data-line-number="1" data-tooltip="Remove Product"><i class="icon-recycle"></i></a>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				<?php else: ?>
					<div class="minicart-empty js-minicart-empty ">
						<div class="minicart-empty-text">
							<?php echo Yii::t("app","empty_cart") ?>
						</div>
						<div class="minicart-empty-icon">
							<i class="icon-shopping-bag"></i>
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 306 262" style="enable-background:new 0 0 306 262;" xml:space="preserve"><path class="st0" d="M78.1,59.5c0,0-37.3,22-26.7,85s59.7,237,142.7,283s193,56,313-84s21-206-69-240s-249.4-67-309-60C94.6,47.6,78.1,59.5,78.1,59.5z"/></svg>
						</div>
					</div>
				<?php endif ?>
				
			</div>
			<div class="col-lg-7 col-xl-5 mt-3 mt-md-0">
				<div class="card-total">
					<div class="card-text-info text-right">
						<h5>Standart shipping</h5>
						<p><b>10 - 11 business days</b><br>1 item ships from the U.S. and will be delivered in 10 - 11 business days</p>
					</div>
					<hr>
					<div class="row d-flex">
						<div class="col card-total-txt">
							<?php echo Yii::t("app","sub_total") ?>
						</div>
						<div id="card-total-price" class="col-auto card-total-price text-right">
							<?php if (isset($_SESSION['cart_tl_price'])): ?>
								<?php price($_SESSION['cart_tl_price']) ?>
							<?php else: ?>
								0
							<?php endif ?>
						</div>
					</div>
					<?php if (isset($_SESSION['cart']) && !empty($_SESSION['cart'])): ?>
					<a href="<?php echo Url::toRoute('site/checkout')?>" class="btn btn--full btn--lg">
						<span>
							<?php echo Yii::t("app","checkout") ?>
						</span>
					</a>
					<a id="clear_cart" href="<?php echo Url::toRoute('site/clear-cart')?>" class="btn btn--grey btn-block">
						<?php echo Yii::t("app","clear_all") ?>
					</a>
					<?php endif; ?>
				</div>
				<div class="mt-2"></div>
			</div>
		</div>
	</div>
</div>
<?php if (!empty($like_products)): ?>
<div class="holder">
        <div class="container">
            <div class="title-wrap text-center">
                <h2 class="h1-style">
                    <?php echo Yii::t("app","like_product") ?>
                </h2>
                <div class="carousel-arrows carousel-arrows--center"></div>
            </div>
            <div class="prd-grid prd-carousel js-prd-carousel slick-arrows-aside-simple slick-arrows-mobile-lg data-to-show-4 data-to-show-md-3 data-to-show-sm-3 data-to-show-xs-2"
                 data-slick='{"slidesToShow": 4, "slidesToScroll": 2, "responsive": [{"breakpoint": 992,"settings": {"slidesToShow": 3, "slidesToScroll": 1}},{"breakpoint": 768,"settings": {"slidesToShow": 2, "slidesToScroll": 1}},{"breakpoint": 480,"settings": {"slidesToShow": 2, "slidesToScroll": 1}}]}'>
                <?php foreach ($like_products as $key => $value): ?>
                        <?php $product_block = Products::getByDetailId($color_id,$value["id"]); ?>
                        <div class="prd prd--style2 prd-labels--max prd-labels-shadow ">
                            <div class="prd-inside">
                                <div class="prd-img-area">
                                    <a href="/site/products?id=<?php echo $value["id"] ?>" class="prd-img image-hover-scale image-container">
                                        <img src="data:<?php echo $value["img"] ?>" data-src="<?php echo $value["img"] ?>" alt="Oversized Cotton Blouse" class="js-prd-img lazyload fade-up">
                                        <div class="foxic-loader"></div>
                                        <div class="prd-big-squared-labels">
                                            
                                            <?php if ($value["status"] == 3 or $value["status"] == 4): ?>
                                                <div class="label-new">
                                                    <span>
                                                        <?php if (Yii::$app->language == 'uz'): ?>
                                                            Yangi
                                                        <?php else: ?>
                                                            Новый
                                                        <?php endif ?>
                                                    </span>
                                                </div>
                                            <?php endif ?>
                                            <?php $sale_product = $value->sale; ?>
                                            <?php if (isset($sale_product) and !empty($sale_product)): ?>
                                                
                                                <div class="label-sale">
                                                    <span><?php echo "-".round($sale_product["sale_pracent"])."%" ?>
                                                        <span class="sale-text">
                                                            <?php if (Yii::$app->language == 'uz'): ?>
                                                                Chegirma
                                                            <?php else: ?>
                                                                Скидка
                                                            <?php endif ?>
                                                        </span>
                                                    </span>
                                                    
                                                    <div class="countdown-circle">
                                                        <div class="countdown js-countdown" data-countdown="2021/07/01"></div>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </a>
                                    <div class="prd-circle-labels">
                                        <a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a>
                                        <a href="#" class="circle-label-qview js-prd-quickview prd-hide-mobile" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUIsK VIEW</span></a>
                                        <?php if (count($product_block) > 1): ?>
                                            <div class="colorswatch-label colorswatch-label--variants js-prd-colorswatch">
                                                <i class="icon-palette"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                                                <ul>
                                                    <?php foreach ($product_block as $key2 => $value2): ?>
                                                        <?php $product_img = ProductDetailsCode::MultiDetails($value2["product_id"],$value2["multi_detail_id"]); ?>
                                                        <li data-image="<?php if (isset($product_img)): ?><?php echo $product_img->img ?><?php endif ?>">
                                                            <a style="background: <?php echo $value2->multi_one->color_name; ?>;" class="js-color-toggle" data-toggle="tooltip" data-placement="left" title="Color Name">
                                                                <!-- <img src="asset/images/colorswatch/color-green.png" alt=""> -->
                                                            </a>
                                                        </li>
                                                    <?php endforeach ?>
                                                    
                                                </ul>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    
                                    <ul class="list-options color-swatch">
                                        <?php $i = 1; ?>
                                        <?php foreach ($value->imgs as $key2 => $value2): ?>
                                            <?php if ($i == 1): ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class="active"><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>
                                            <?php else: ?>
                                                <li data-image="<?php echo$value2["img"]  ?>" class=><a href="#" class="js-color-toggle" data-toggle="tooltip" data-placement="right" title="Color Name"><img src="data:<?php echo$value2["img"]  ?>" data-src="<?php echo$value2["img"]  ?>" class="lazyload fade-up" alt="Color Name"></a></li>

                                            <?php endif ?>
                                            <?php $i++; ?>
                                        <?php endforeach ?>

                                    </ul>
                                </div>
                                <div class="prd-info">
                                    <div class="prd-info-wrap">
                                        <div class="prd-info-top">
                                            <div class="prd-rating"><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i><i class="icon-star-fill fill"></i></div>
                                        </div>
                                        <?php $stars =  $value->stars; ?>
                                        <div class="prd-rating justify-content-center">
                                            <?php for ($i = 1; $i <= $stars; $i++): ?>
                                                <i class="icon-star-fill fill"></i>
                                            <?php endfor ?>
                                        </div>
                                        <div class="prd-tag"><a href="#">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                    <?php echo $value->subCategory->title_uz ?>
                                                <?php else: ?>
                                                    <?php echo $value->subCategory->title_ru ?>
                                            <?php endif ?>
                                        </a></div>
                                        <h2 class="prd-title"><a href="product.html">
                                            <?php if (Yii::$app->language == "uz"): ?>
                                                <?php echo $value["title_uz"] ?>
                                            <?php else: ?>
                                                <?php echo $value["title_ru"] ?>
                                            <?php endif ?>
                                        </a></h2>
                                        <div class="prd-description">
                                            Quisque volutpat condimentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam nec ante sed lacinia.
                                        </div>
                                        <div class="prd-action">
                                            <form action="#">
                                                <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="prd-hovers">
                                        <div class="prd-circle-labels">
                                            <div><a href="#" class="circle-label-compare circle-label-wishlist--add js-add-wishlist mt-0" title="Add To Wishlist"><i class="icon-heart-stroke"></i></a><a href="#" class="circle-label-compare circle-label-wishlist--off js-remove-wishlist mt-0" title="Remove From Wishlist"><i class="icon-heart-hover"></i></a></div>
                                            <div class="prd-hide-mobile"><a href="#" class="circle-label-qview js-prd-quickview" data-src="ajax/ajax-quickview.html"><i class="icon-eye"></i><span>QUICK VIEW</span></a></div>
                                        </div>
                                        <div class="prd-price">
                                            <?php $product_sale = $value->sale; ?>
                                            <?php if (isset($product_sale) and !empty($product_sale)): ?>
                                                <div class="price-old"><?php echo $value["price"] ?></div>
                                                
                                                <div class="price-new"><?php echo $product_sale["sale_price"] ?></div>
                                                
                                                <?php else: ?>
                                                <div class="price-new"><?php echo $value["price"] ?></div>
                                            <?php endif ?>
                                        </div>
                                        <div class="prd-action">
                                            <div class="prd-action-left">
                                                <form action="#">
                                                    <button class="btn js-prd-addtocart" data-product='{"name": "Oversized Cotton Blouse", "path":"images/skins/fashion/products/product-03-1.jpg", "url":"product.html", "aspect_ratio":0.778}'>
                                                    <?php echo Yii::t('app', 'add_to_cart'); ?>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php endif ?>