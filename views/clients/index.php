<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mijozlar';
?>
<div class="clients-index">
    <div class="row">
        <div class="col-md-6">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [ 
                'attribute' =>'full_name',
                'label'  => 'Ismi',
                'value' => function ($data) {
                    return base64_decode($data->full_name);
                }
            ],
            [ 
                'attribute' =>'phone_number',
                'label'  => 'Telefon Raqami',
                'value' => function ($data) {
                    return base64_decode($data->phone_number);
                }
            ],
            [ 
                'attribute' =>'tg_name',
                'label'  => 'Telegramdagi nomi',
                'value' => function ($data) {
                    return base64_decode($data->tg_name);
                }
            ],
            [ 
                'attribute' =>'tg_username',
                'label'  => 'Telegramdagi Foydalanuvchi nomi',
                'value' => function ($data) {
                    return base64_decode($data->tg_username);
                }
            ],
            [ 
                'attribute' =>'tg_username',
                'label'  => 'Cashback summasi',
                'value' => function ($data) {
                    return $data->cashback_sum;
                }
            ],
            'birth_date',
            //'sms_code',
            //'status',

        ],
    ]); ?>


</div>
