<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $chat_id
 * @property int|null $product_id
 * @property int|null $count
 * @property int|null $summ
 * @property string|null $create_date
 * @property int|null $status
 * @property string|null $status_description
 * @property int|null $client_id
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chat_id', 'product_id', 'count', 'summ', 'status', 'client_id'], 'default', 'value' => null],
            [['chat_id', 'product_id', 'count', 'summ', 'status', 'client_id'], 'integer'],
            [['create_date'], 'safe'],
            [['status_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'product_id' => 'Product ID',
            'count' => 'Count',
            'summ' => 'Summ',
            'create_date' => 'Create Date',
            'status' => 'Status',
            'status_description' => 'Status Description',
            'client_id' => 'Client ID',
        ];
    }
}
