<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_img".
 *
 * @property int $id
 * @property int|null $product_id
 * @property string|null $img
 * @property int|null $status
 */
class ProductImg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
    public static function tableName()
    {
        return 'product_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'status'], 'default', 'value' => null],
            [['product_id', 'status'], 'integer'],
            [['img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'img' => 'Img',
            'status' => 'Status',
        ];
    }
    public function getColor()
    {
       return $this->hasOne(MultiDetails::className(), ['id' => 'multi_detail_id']);
    }
}
