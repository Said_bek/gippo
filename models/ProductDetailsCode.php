<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_details_code".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $multi_detail_id
 * @property int|null $detail_id
 * @property int|null $detail_code
 */
class ProductDetailsCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_details_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'multi_detail_id', 'detail_id', 'detail_code'], 'default', 'value' => null],
            [['product_id', 'multi_detail_id', 'detail_id', 'detail_code'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'multi_detail_id' => 'Multi Detail ID',
            'detail_id' => 'Detail ID',
            'detail_code' => 'Detail Code',
        ];
    }
    
    public function getMulti() {
        return $this->hasMany(MultiDetails::className(),['id' => 'multi_detail_id']);
    }

    public function checkDates($array) 
    {
        $brand_id = $array["brand_id"];
        $multi_detail_id = $array["its_brand"];
        $details = Details::find()->where("id = :id and type = :type" ,["id" => $brand_id,":type" => 2])->one();
        if (isset($details) and !empty($details)) {
            $detail_code_find = $this->find()->orderBy(["detail_code" => SORT_DESC])->one();
            if (isset($detail_code_find) and !empty($detail_code_find)) {
                $detail_code = $detail_code_find->detail_code + 1; 
            } else {
                $detail_code = 1;
            }
            $product_id = '';
            $i = 1;
            foreach ($array as $key => $value) {
                if (intval($key)) {
                    foreach ($value as $key2 => $value2) {
                        $product_id = $value2;
                        $i++;
                        break;
                    }
                }
            }

            $sql_insert = "INSERT INTO product_details_code (product_id,multi_detail_id,detail_id,detail_code) VALUES ";
            $sql_value = "";
            $i = 1;
            // echo "<pre>";
            // print_r($array);
            // echo "</pre>";
            // die();
            $sql = '';
            foreach ($array as $key => $value) {
                if (intval($key)) {
                    foreach ($value as $key2 => $value2) {
                        if ($i == 1) {
                            $sql_value = $sql_value."(".$value2.",".$key2.",".$key.",".$detail_code.")";
                        } else {
                            $sql_value = $sql_value.",(".$value2.",".$key2.",".$key.",".$detail_code.")";
                        }
                        $i++;
                    }
                }
            }
            // $sql_value = $sql_value;
            $sql_value = $sql_value.", (".$value2.",".$multi_detail_id.",".$brand_id.",".$detail_code.")";
            $sql = $sql_insert.$sql_value;
            $result = Yii::$app->db->createCommand($sql)->execute();
            if ($result == true ){
                return 'true';
            }
        }
    }

    public function checkDatesUpdate($array) 
    {
        $detail_code = $array["detail_code"];
        $brand_id = $array["brand_id"];
        $multi_detail_id = $array["its_brand"];
        $details = Details::find()->where("id = :id and type = :type" ,["id" => $brand_id,":type" => 2])->one();
        if (isset($details) and !empty($details)) {
            $check_data = $this->find()->where("detail_code = :detail_code and multi_detail_id = :multi_detail_id",[":detail_code" => $detail_code, ":multi_detail_id" => $multi_detail_id ])->one();
            if (isset($check_data) and !empty($check_data)) {
                $product_id = '';
                $i = 1;
                foreach ($array as $key => $value) {
                    if (intval($key)) {
                        foreach ($value as $key2 => $value2) {
                            $product_id = $value2;
                            $i++;
                            break;
                        }
                    }
                }

                $sql_insert = "INSERT INTO product_details_code (product_id,multi_detail_id,detail_id,detail_code) VALUES ";
                $sql_value = "";
                $i = 1;
                foreach ($array as $key => $value) {
                    if (intval($key)) {
                        foreach ($value as $key2 => $value2) {
                            if ($i == 1) {
                                $sql_value = $sql_value."(".$value2.",".$key2.",".$key.",".$detail_code.")";
                            } else {
                                $sql_value = $sql_value.",(".$value2.",".$key2.",".$key.",".$detail_code.")";
                            }
                            $i++;
                        }
                    }
                }
                $sql_value = $sql_value.", (".$value2.",".$multi_detail_id.",".$brand_id.",".$detail_code.")";
                $sql = $sql_insert.$sql_value;
                $result = Yii::$app->db->createCommand($sql)->execute();
                if ($result == true ){
                    return 'true';
                }
            }
        }
    }
    public function getMulti_one() 
    {
        return $this->hasOne(MultiDetails::className(),['id' => 'multi_detail_id']);
    }
    public static function MultiDetails($product_id,$multi_id)
    {
        if (!$product_img = ProductImg::find()->where('product_id = :product_id and multi_detail_id = :multi_detail_id',[':product_id' => $product_id,'multi_detail_id' => $multi_id])->one()){
            return false;
        }
        return $product_img;
    }

}
