<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string|null $customer_name
 * @property string|null $customer_title
 * @property string|null $customer_post
 * @property int|null $star
 * @property string|null $created_date
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['star'], 'default', 'value' => null],
            [['star'], 'integer'],
            [['created_date'], 'safe'],
            [['customer_name', 'customer_title', 'customer_post'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_name' => 'Customer Name',
            'customer_title' => 'Customer Title',
            'customer_post' => 'Customer Post',
            'star' => 'Star',
            'created_date' => 'Created Date',
        ];
    }
}
