<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property int|null $chat_id
 * @property string|null $phone_number
 * @property string|null $tg_name
 * @property string|null $tg_username
 * @property string|null $password
 * @property int|null $sms_code
 * @property string|null $full_name
 * @property int|null $status
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chat_id', 'sms_code', 'status','birth_date'], 'default', 'value' => null],
            [['chat_id', 'sms_code', 'status'], 'integer'],
            [['phone_number', 'tg_name', 'tg_username', 'password', 'full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'phone_number' => 'Phone Number',
            'tg_name' => 'Tg Name',
            'tg_username' => 'Tg Username',
            'password' => 'Password',
            'sms_code' => 'Sms Code',
            'full_name' => 'Full Name',
            'cashback_sum' => 'Cashback',
            'status' => 'Status',
            'birth_date' => 'Tug`ilgan sana',
        ];
    }
    public function accountUpdate($name,$last_name,$phone_number,$client_id)
    {
        $errors = [];
        if (!(isset($name) and !empty($name))) {
            $errors[] = "name_error";
        }
        if (!(isset($last_name) and !empty($last_name))) {
            $errors[] = "last_name_error";
        }
        if (!(isset($phone_number) and !empty($phone_number))) {
            $errors[] = "phone_number_error";
        } else {
            $phone = preg_replace('/[^0-9]/', '', $phone_number);
            if (strlen($phone) < 12) {
                $errors[] = "phone_number_error";
            }

        }
        if (!(isset($client_id) and !empty($client_id))) {
            $errors[] = "client_id_error";
        } 
        if (empty($errors)) {
            $model_client = Clients::findOne($client_id);
            if (isset($model_client) and !empty($model_client)) {
                $model_client->full_name = $last_name. " ".$name;
                $model_client->phone_number = $phone;
                if ($model_client->save()) {
                    $_SESSION['account'] = [
                        'client_id' => $model_client->id,
                        'phone' => $model_client->phone_number,
                        'full_name' => $model_client->full_name,
                        'pass' => $model_client->password,
                    ];
                    return ["status" => "success"];
                }
            }
        } else {
            return ["status" => "fail", "errors" => $errors];
        }
    }
}
