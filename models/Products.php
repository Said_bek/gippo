<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int|null $sub_category_id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property int|null $price
 * @property string|null $img
 * @property string|null $description_uz
 * @property string|null $description_ru
 * @property int|null $status
 */

class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    const LIMIT_PRODUCT = 12;
    public $imageFile;

   
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    
    public function rules()
    {
        return [
            [['title_uz', 'title_ru','price'], 'required'],
            [['sub_category_id', 'price', 'status'], 'default', 'value' => null],
            [['price', 'status', 'count','quantity','main_category'], 'integer'],
            [['dollar'], 'double'],
            [['description_uz', 'description_ru','description_en'], 'string'],
            [['title_uz', 'title_ru', 'img','title_en','link','made','made_ru','made_uz','sku','sku_api'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'sub_category_id' => 'Kategoriya raqami',
            'title_uz' => 'Mahsulot nomi',
            'title_ru' => 'Mahsulot nomi ru',
            'price' => 'Narxi',
            'img' => 'Rasmi',
            'description_uz' => 'Tavsifi',
            'description_ru' => 'Tavsifi Ru',
            'status' => 'Status',
            'count' => 'Soni',
        ];
    }
    
    public function getSubCategory()
    {
           return $this->hasOne(SubCategory::className(), ['id' => 'sub_category_id']);
    }

    public function getCategory()
    {
      return $this->hasOne(Category::className(), ['id' => 'main_category']);
    }

    public function getDropdown()
    {
      return SubCategory::find()->all();
    }

    public function getImgs()
    {
      return $this->hasMany(ProductImg::className(),["product_id" => "id"])->limit(3);
    }

    public function getProductimg()
    {
      return  ProductImg::find()->where("product_id = :product_id",[":product_id" => $this->id])->orderBy(["id" => SORT_ASC])->all();
      
    }
    
    public function getColor() 
    {
      return $this->hasMany(ProductDetailsCode::class,["product_id" => "id"]);
    }
    
    public static function getByDetailId($detail_id,$id)
    {
        if (!$product_detail = ProductDetailsCode::find()->where(['product_id' => $id,'detail_id' => $detail_id])->all()){
            return [0];
        }
        return $product_detail;
    }

    public function getSale()
    {
      $current_date = date("Y-m-d H:i:s");
      $sql = "SELECT * FROM product_sale WHERE product_id = ".$this->id." and  enter_date <= '".$current_date."' and end_date >= '".$current_date."'";
      $model = ProductSale::findBySql($sql)->one();
      return $model;
      // return  $this->hasOne(ProductSale::className(), ['product_id' => 'id']);
    }

    public function getStars() 
    {
        $sql = "select sum(star) , count(id) from reviews where product_id = ".$this->id;
        $model = Yii::$app->db->createCommand($sql)->queryAll();
        $sum = $model[0]["sum"];
        $count = $model[0]["count"];
        $stars = 0;
        if ($sum > 0 and $count > 0) {
            $stars = $sum / $count;
        }
        return round($stars);
    }

    public static function hasProduct() 
    {
        $session = Yii::$app->session;
        $last_id =  $session["list"]["last_id"];
            // $product = Products::find()->where("id > :id",[":id" => $last_id])->limit(self::LIMIT_PRODUCT)->all();
        $str =  $session["list"]["search_key"];
        $whereType = "";
        if (isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) {
            $status = 0;
            $status2 = 0;
            if ($session["list"]["search_type"] == "trend") {
                $status = 2;
                $status2 = 4;
                $session["list"]["select_search"] = 2;
            }  else if ($session["list"]["search_type"] == "new") {
                $status = 4;
                $status2 = 3;
                $session["list"]["select_search"] = 1;
            }  
            if ($status != 0 and $status2 != 0) {
                $whereType = "where T.status = ".$status. " or T.status = ".$status2;
            }
        }
        
        if ((isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) and $session["list"]["search_type"] == "price") {
            $sql = "
                SELECT 
                  * 
                from 
                  (
                    SELECT 
                      pr.* 
                    FROM 
                      category as ct 
                      inner join products as pr on ct.id = pr.main_category
                      inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                    where 
                      (
                        lower(ct.title_uz) like '%".$str."%' 
                        or lower(ct.title_ru) like '%".$str."%'
                      ) 
                      and pr.id > ".$last_id." 
                    union 
                    select 
                      pr.* 
                    from 
                      sub_category as sc 
                      inner join sub_category as sc2 on sc.id = sc2.sub_category_id 
                      inner join products as pr on sc.id = pr.sub_category_id or sc2.id = pr.sub_category_id 
                      inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                    where 
                      (
                        (
                          lower(sc.title_uz) like '%".$str."%' 
                          or lower(sc.title_ru) like '%".$str."%'
                        ) 
                        or (
                          lower(sc2.title_uz) like '%".$str."%' 
                          or lower(sc2.title_ru) like '%".$str."%'
                        )
                      ) 
                      and pr.id > ".$last_id." 
                    union 
                    select 
                      pr.* 
                    from 
                      products as pr 
                    inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                    where 
                      (
                        lower(pr.title_uz) like '%".$str."%' 
                        or lower(pr.title_ru) like '%".$str."%'
                      ) 
                      and pr.id > ".$last_id."
                  ) as T 
                group by 
                  T.id, 
                  T.sub_category_id, 
                  t.main_category, 
                  T.sell_count, 
                  T.title_uz, 
                  T.title_ru, 
                  T.price, 
                  T.img, 
                  T.description_uz, 
                  T.description_ru, 
                  T.status, 
                  T.count, 
                  T.type, 
                  T.view, 
                  T.sku, 
                  T.made, 
                  T.dollar, 
                  T.sold_product, 
                  T.quantity
            ";
        } else {
            $sql = "
                SELECT 
                  * 
                from 
                  (
                    SELECT 
                      pr.* 
                    FROM 
                      category as ct 
                      inner join products as pr on ct.id = pr.main_category 
                    where 
                      (
                        lower(ct.title_uz) like '%".$str."%' 
                        or lower(ct.title_ru) like '%".$str."%'
                      ) 
                      and pr.id > ".$last_id." 
                    union 
                    select 
                      pr.* 
                    from 
                      sub_category as sc 
                      inner join sub_category as sc2 on sc.id = sc2.sub_category_id 
                      inner join products as pr on sc.id = pr.sub_category_id 
                      or sc2.id = pr.sub_category_id 
                    where 
                      (
                        (
                          lower(sc.title_uz) like '%".$str."%' 
                          or lower(sc.title_ru) like '%".$str."%'
                        ) 
                        or (
                          lower(sc2.title_uz) like '%".$str."%' 
                          or lower(sc2.title_ru) like '%".$str."%'
                        )
                      ) 
                      and pr.id > ".$last_id." 
                    union 
                    select 
                      pr.* 
                    from 
                      products as pr 
                    where 
                      (
                        lower(pr.title_uz) like '%".$str."%' 
                        or lower(pr.title_ru) like '%".$str."%'
                      ) 
                      and pr.id > ".$last_id."
                  ) as T 
                ".$whereType."
                group by 
                  T.id, 
                  T.sub_category_id, 
                  t.main_category, 
                  T.sell_count, 
                  T.title_uz, 
                  T.title_ru, 
                  T.price, 
                  T.img, 
                  T.description_uz, 
                  T.description_ru, 
                  T.status, 
                  T.count, 
                  T.type, 
                  T.view, 
                  T.sku, 
                  T.made, 
                  T.dollar, 
                  T.sold_product, 
                  T.quantity
            ";
        }

        $product = Products::findBySql($sql)->all();
        $session["list"]["sql"] = $sql;

        return $product;
    }
    public function loadMore()
    {
        $session = Yii::$app->session;
        if (isset($session["list"]["last_id"]) and !empty($session["list"]["last_id"])) {
            $last_id =  $session["list"]["last_id"];
            // $product = Products::find()->where("id > :id",[":id" => $last_id])->limit(self::LIMIT_PRODUCT)->all();
            $str =  $session["list"]["search_key"];
            $whereType = "";
            if (isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) {
                $status = 0;
                $status2 = 0;
                if ($session["list"]["search_type"] == "trend") {
                    $status = 2;
                    $status2 = 4;
                    $session["list"]["select_search"] = 2;
                }  else if ($session["list"]["search_type"] == "new") {
                    $status = 4;
                    $status2 = 3;
                    $session["list"]["select_search"] = 1;
                }  
                if ($status != 0 and $status2 != 0) {
                    $whereType = "where T.status = ".$status. " or T.status = ".$status2;
                }
            }
            if ((isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) and $session["list"]["search_type"] == "price") {
                $sql = "
                    SELECT 
                      * 
                    from 
                      (
                        SELECT 
                          pr.* 
                        FROM 
                          category as ct 
                          inner join products as pr on ct.id = pr.main_category
                          inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                        where 
                          (
                            lower(ct.title_uz) like '%".$str."%' 
                            or lower(ct.title_ru) like '%".$str."%'
                          ) 
                          and pr.id > ".$last_id." 
                        union 
                        select 
                          pr.* 
                        from 
                          sub_category as sc 
                          inner join sub_category as sc2 on sc.id = sc2.sub_category_id 
                          inner join products as pr on sc.id = pr.sub_category_id or sc2.id = pr.sub_category_id 
                          inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                        where 
                          (
                            (
                              lower(sc.title_uz) like '%".$str."%' 
                              or lower(sc.title_ru) like '%".$str."%'
                            ) 
                            or (
                              lower(sc2.title_uz) like '%".$str."%' 
                              or lower(sc2.title_ru) like '%".$str."%'
                            )
                          ) 
                          and pr.id > ".$last_id." 
                        union 
                        select 
                          pr.* 
                        from 
                          products as pr 
                        inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                        where 
                          (
                            lower(pr.title_uz) like '%".$str."%' 
                            or lower(pr.title_ru) like '%".$str."%'
                          ) 
                          and pr.id > ".$last_id."
                      ) as T 
                    group by 
                      T.id, 
                      T.sub_category_id, 
                      t.main_category, 
                      T.sell_count, 
                      T.title_uz, 
                      T.title_ru, 
                      T.price, 
                      T.img, 
                      T.description_uz, 
                      T.description_ru, 
                      T.status, 
                      T.count, 
                      T.type, 
                      T.view, 
                      T.sku, 
                      T.made, 
                      T.dollar, 
                      T.sold_product, 
                      T.quantity
                ";
            } else {
                $sql = "
                    SELECT 
                      * 
                    from 
                      (
                        SELECT 
                          pr.* 
                        FROM 
                          category as ct 
                          inner join products as pr on ct.id = pr.main_category 
                        where 
                          (
                            lower(ct.title_uz) like '%".$str."%' 
                            or lower(ct.title_ru) like '%".$str."%'
                          ) 
                          and pr.id > ".$last_id." 
                        union 
                        select 
                          pr.* 
                        from 
                          sub_category as sc 
                          inner join sub_category as sc2 on sc.id = sc2.sub_category_id 
                          inner join products as pr on sc.id = pr.sub_category_id 
                          or sc2.id = pr.sub_category_id 
                        where 
                          (
                            (
                              lower(sc.title_uz) like '%".$str."%' 
                              or lower(sc.title_ru) like '%".$str."%'
                            ) 
                            or (
                              lower(sc2.title_uz) like '%".$str."%' 
                              or lower(sc2.title_ru) like '%".$str."%'
                            )
                          ) 
                          and pr.id > ".$last_id." 
                        union 
                        select 
                          pr.* 
                        from 
                          products as pr 
                        where 
                          (
                            lower(pr.title_uz) like '%".$str."%' 
                            or lower(pr.title_ru) like '%".$str."%'
                          ) 
                          and pr.id > ".$last_id."
                      ) as T 
                    ".$whereType."
                    group by 
                      T.id, 
                      T.sub_category_id, 
                      t.main_category, 
                      T.sell_count, 
                      T.title_uz, 
                      T.title_ru, 
                      T.price, 
                      T.img, 
                      T.description_uz, 
                      T.description_ru, 
                      T.status, 
                      T.count, 
                      T.type, 
                      T.view, 
                      T.sku, 
                      T.made, 
                      T.dollar, 
                      T.sold_product,
                      T.quantity
                ";
            }
            $product = Products::findBySql($sql)->all();
            $count = count($product);
            $key = $count-1;
            $session["list"]["last_id"] = $product[$key]["id"];
            return $product;

        }
    }
    public function Search($str)
    {
        $session = Yii::$app->session;
        $whereType = "";
        if (isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) {
            $status = 0;
            $status2 = 0;
            if ($session["list"]["search_type"] == "trend") {
                $status = 2;
                $status2 = 4;
                $session["list"]["select_search"] = 2;
            }  else if ($session["list"]["search_type"] == "new") {
                $status = 4;
                $status2 = 3;
                $session["list"]["select_search"] = 1;
            }  
            if ($status != 0 and $status2 != 0) {
                $whereType = "where T.status = ".$status. " or T.status = ".$status2;
            }
        }
        if ((isset($session["list"]["search_type"]) and !empty($session["list"]["search_type"])) and $session["list"]["search_type"] == "price") {
                $session["list"]["select_search"] = 3;
            $sql = "
                select 
                  * 
                from 
                  (
                    SELECT 
                      pr.* 
                    FROM 
                      category as ct 
                      inner join products as pr on ct.id = pr.main_category 
                      inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                    where 
                      lower(ct.title_uz) like '%".$str."%' 
                      or lower(ct.title_ru) like '%".$str."%' 
                    union 
                    select 
                      pr.* 
                    from 
                      sub_category as sc 
                      inner join sub_category as sc2 on sc.id = sc2.sub_category_id 
                      inner join products as pr on sc.id = pr.sub_category_id 
                      inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                      or sc2.id = pr.sub_category_id 
                    where 
                      (
                        lower(sc.title_uz) like '%".$str."%' 
                        or lower(sc.title_ru) like '%".$str."%'
                      ) 
                      or (
                        lower(sc2.title_uz) like '%".$str."%' 
                        or lower(sc2.title_ru) like '%".$str."%'
                      ) 
                    union 
                    select 
                      pr.* 
                    from 
                      products as pr 
                      inner join product_sale as ps on ps.product_id = pr.id and  ps.enter_date <= now() and ps.end_date >= now()
                    where 
                      lower(pr.title_uz) like '%".$str."%' 
                      or lower(pr.title_ru) like '%".$str."%'
                  ) as T 
                group by 
                  T.id, 
                  T.sub_category_id, 
                  T.main_category, 
                  T.sell_count, 
                  T.title_uz, 
                  T.title_ru, 
                  T.price, 
                  T.img, 
                  T.description_uz, 
                  T.description_ru, 
                  T.status, 
                  T.count, 
                  T.type, 
                  T.view, 
                  T.sku, 
                  T.made, 
                  T.dollar,
                  T.sold_product, 
                  T.quantity 
                limit 
                  2
            ";
        } else {
            $sql = "
                select * from (
                    SELECT pr.* FROM category as ct
                    inner join products as pr on ct.id = pr.main_category
                    where lower(ct.title_uz) like '%".$str."%' or lower(ct.title_ru) like '%".$str."%'
                    union
                    select pr.* from sub_category as sc 
                    inner join sub_category as sc2 on sc.id = sc2.sub_category_id
                    inner join products as pr on sc.id = pr.sub_category_id or sc2.id = pr.sub_category_id
                    where (lower(sc.title_uz) like '%".$str."%' or lower(sc.title_ru) like '%".$str."%') or
                    (lower(sc2.title_uz) like '%".$str."%' or lower(sc2.title_ru) like '%".$str."%')
                    union
                    select pr.* from products as pr where lower(pr.title_uz) like '%".$str."%' or lower(pr.title_ru) like '%".$str."%'
                ) as T
                ".$whereType."
                group by 
                    T.id,
                    T.sub_category_id,
                    t.main_category,
                    T.sell_count,
                    T.title_uz,
                    T.title_ru,
                    T.price,
                    T.img,
                    T.description_uz,
                    T.description_ru,
                    T.status,
                    T.count,
                    T.type,
                    T.view,
                    T.sku,
                    T.made,
                    T.dollar,
                    T.sold_product,
                    T.quantity limit ".Products::LIMIT_PRODUCT;
        }

        
        $product = Products::findBySql($sql)->all();
        if (isset($product) and !empty($product)) {
            $count_product = count($product);
            if (!(isset($session["list"]) and !empty($session["list"]))) {
                $session["list"] = new \ArrayObject();
                $session["list"]["search"] = [];
                for ($i=0; $i < $count_product ; $i++) { 
                    $session["list"]["search"][$i] = $product[$i];
                }
                $key = $count_product-1;
                $session["list"]["last_id"] = $product[$key]["id"];
            } else {
                $session["list"]["search"] = [];
                for ($i=0; $i < $count_product ; $i++) { 
                    $session["list"]["search"][$i] = $product[$i];
                }
                $key = $count_product-1;
                $session["list"]["last_id"] = $product[$key]["id"];
            }
            $session["list"]["search_key"] = $str;
            return true;
        } else {
            return false;
        }
    }


    public function getSale2()
    {
        $model = ProductSale::find()
        ->where([
            'product_id' => $this->id
        ])
        ->andWhere([
            '<=','enter_date', date('Y-m-d H:i:s')
        ])
        ->andWhere([
            '>=','end_date', date('Y-m-d H:i:s')
        ])
        ->one();


        if (isset($model))
            return $model->sale_price;
        else
            return 0;
        
        
    }
    
}
