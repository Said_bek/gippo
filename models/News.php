<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property string|null $file_id
 * @property string|null $description_uz
 * @property string|null $description_ru
 * @property string|null $type
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_uz', 'description_ru'], 'string'],
            [['title_uz', 'title_ru', 'file_id', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title_uz' => 'Yangilik Nomi',
            'file_id' => 'Rasm',
            'description_uz' => 'Yangilik matni',
        ];
    }
}
