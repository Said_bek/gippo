<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_address".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $longs
 * @property string|null $lat
 * @property string|null $address
 * @property int|null $chat_id
 */
class OrderAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'chat_id'], 'default', 'value' => null],
            [['order_id', 'chat_id'], 'integer'],
            [['longs', 'lat', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'longs' => 'Longs',
            'lat' => 'Lat',
            'address' => 'Address',
            'chat_id' => 'Chat ID',
        ];
    }
}
