<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cashbacks".
 *
 * @property int $id
 * @property int|null $cashback
 */
class Cashbacks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashbacks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cashback'], 'required'],
            [['cashback'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cashback' => 'Cashback',
        ];
    }
}
