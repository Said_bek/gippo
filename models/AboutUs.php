<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_us".
 *
 * @property int $id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property string|null $file_id
 * @property string|null $description_uz
 * @property string|null $description_ru
 * @property string|null $type
 */
class AboutUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $imageFile;
    public static function tableName()
    {
        return 'about_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_uz', 'title_ru','description_uz','description_ru'], 'required'],
            [['description_uz', 'description_ru'], 'string'],
            [['title_uz', 'title_ru', 'file_id', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_uz' => 'Sarlavha',
            'title_ru' => 'Saravha ru',
            'file_id' => 'Rasm',
            'description_uz' => 'Text',
            'type' => 'Type',
        ];
    }
}
