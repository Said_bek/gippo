<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_code".
 *
 * @property int $id
 * @property string|null $order_code
 * @property string|null $created_date
 * @property int|null $stage
 * @property int|null $client_id
 * @property string|null $order_date
 * @property int|null $address_id
 * @property int|null $delivery_price
 * @property int|null $delivery
 * @property string|null $description
 * @property string|null $description_ru
 * @property int|null $pay
 * @property int|null $billing_status
 * @property int|null $bot
 */
class OrderCode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'order_date'], 'safe'],
            [['stage', 'client_id', 'address_id', 'delivery_price', 'delivery', 'pay', 'billing_status', 'bot'], 'default', 'value' => null],
            [['stage', 'client_id', 'address_id', 'delivery_price', 'delivery', 'pay', 'billing_status', 'bot'], 'integer'],
            [['description', 'description_ru'], 'string'],
            [['order_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_code' => 'Order kodi',
            'created_date' => 'Buyurtma qilingan sana',
            'stage' => 'Etap',
            'client_id' => 'client ismi',
            'order_date' => 'Order Date',
            'address_id' => 'Address ID',
            'delivery_price' => 'Delivery Price',
            'delivery' => 'Delivery',
            'description' => 'Description',
            'description_ru' => 'Description Ru',
            'pay' => 'Pay',
            'pay' => 'To`lov turi',
            'file_id' => 'file id',
            'billing_status' => 'To`lov holati',
            'bot' => 'Bot',
        ];
    }
    public function getOrders()
    {
        return $this->hasMany(ArriveOrders::class, ['order_code_id' => 'order_code']);
    }
    public function getAddress()
    {
        return $this->hasOne(OrderAddress::className(), ['order_code_id' => 'order_code']);
    }
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
    public function getTotal()
    {
        $order_code = $this->order_code;
        $model = ArriveOrders::find()->where("order_code_id = :order_code",[":order_code"=>$order_code])->sum("sell_price");
        return $model;
    }
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
        // return $model->title_uz;
    }

    public function getCamment()
    {
        return $this->hasOne(ClientOrderCamments::className(), ['order_code_id' => 'id']);
        // return $model->title_uz;
    }
    
}
