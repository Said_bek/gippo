<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "multi_details".
 *
 * @property int $id
 * @property string|null $title_uz
 * @property string|null $title_ru
 * @property int|null $details_id
 * @property int|null $status
 */
class MultiDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'multi_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['details_id', 'status'], 'default', 'value' => null],
            [['details_id', 'status'], 'integer'],
            [['title_uz', 'title_ru','title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_uz' => 'Detal nomi',
            'title_ru' => 'Detal nomi',
            'details_id' => 'Details ID',
            'status' => 'Status',
        ];
    }
    
}
