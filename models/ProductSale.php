<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_sale".
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $sale_price
 * @property int|null $sale_pracent
 * @property string|null $enter_date
 * @property string|null $end_date
 */
class ProductSale extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_sale';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'sale_price','sale_dollar'], 'required'],
            [['product_id', 'sale_price',], 'integer'],
            [['enter_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Mahsulot nomi',
            'sale_price' => 'Chegirma summasi',
            'sale_pracent' => 'Chegirma foizi',
            'enter_date' => 'Chegirma boshlanish vaqti',
            'end_date' => 'Chegirma tugash vaqti',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }   
    
}
