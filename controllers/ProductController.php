<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Products;
use app\models\Details;
use app\models\MultiDetails;
use app\models\SubCategory;
use app\models\ProductImg;
use app\models\ProductDetailsMini;
use app\models\ProductDetailsCode;

/**
 * ProductController implements the CRUD actions for Products model.
 */
class ProductController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
            if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Products::find(),
            'sort'=> ['defaultOrder' => ['id' => SORT_ASC]],


            'pagination' => [
              'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $arr = [];
        $product_img = ProductImg::find()->where("product_id = :product_id",[':product_id' => $id])->all();
        $product_code = ProductDetailsCode::find()->where("product_id = :product_id",[":product_id" => $id])->orderBy(["detail_code" => SORT_ASC])->all();
        $details = Details::find()->all();
        $brand_details = Details::find()->where("type = :type",[":type" => 2])->one();
        $arr_multi_id = [];
        $brand_ids = [];
        if (isset($brand_details->multiDetails) and !empty($brand_details->multiDetails)) {
            foreach ($brand_details->multiDetails as $key => $value) {
                $brand_ids[] = $value->id;
            }
        }
        if (isset($product_code) and !empty($product_code)) {
            foreach ($product_code as $key => $value) {
                if (in_array($value->multi_detail_id, $brand_ids)) {
                    foreach ($product_code as $key2 => $value2) {

                        if ($value->detail_code == $value2->detail_code) {
                            $arr_multi_id[$value->multi_detail_id]["detail_code"] = $value->detail_code;
                            if ($value2->multi_detail_id  != $value->multi_detail_id) {
                                $arr_multi_id[$value->multi_detail_id]["multi_detail_id"][] = $value2->multi_detail_id;
                            }
                        }
                    }
                }
            }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'product_img' => $product_img,
            'brand_details' => $brand_details,
            'details' => $details,
            'arr_multi_id' => $arr_multi_id,
            'id' => $id,

        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $dropdown = SubCategory::find()->where('type = 1')->all();
        // pre($dropdown);
        $details = Details::find()->all();
        $errors = '';
        if ($model->load(Yii::$app->request->post()) ) {
            $post = Yii::$app->request->post();
            $SubCategory = $post["Products"]["sub_category_id"];
            if (isset($post["its_new"]) and !empty($post["its_new"])) {
                $model->type = 2; 
            } else {
                $model->type = 1;
            }
            // pre($post);
            $model->sub_category_id = $SubCategory;
            $model->imageFile = UploadedFile::getInstance($model, 'img');
            if ($model->imageFile) {
                $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
            }
            if (!(isset($model->title_uz) and !empty($model->title_uz))) {
                $errors = 'Title_uz';
            }
            if (!(isset($model->title_ru) and !empty($model->title_ru))) {
                $errors = 'Title_ru';
            }
            if (!(isset($model->price) and !empty($model->price))) {
                $errors = 'price';
                Yii::$app->session->setFlash('danger', "Narx kiriting!");

            }
            if (!(isset($model->sub_category_id) and !empty($model->sub_category_id))) {
                $errors = 'sub_category_id';
            }
            if (!(isset($model->img) and !empty($model->img))) {
                $errors = 'img';
                Yii::$app->session->setFlash('danger', "Rasim yuklang!");
            }

            if ($errors == '' and empty($errors)) {
                $model->status = 1;
                if($model->save()){
                    $product_id = $model->id;
                    $post = Yii::$app->request->post();
                    if (isset($post["MultiDetails"]) and !empty($post["MultiDetails"])) {
                        foreach ($post["MultiDetails"] as $key => $value) {
                            $details_id = $value; 
                            $multi_details_id = $key;
                            $sql = "INSERT INTO product_details_ (product_id, details_id, status, multi_details_id) VALUES (".$product_id.",".$details_id.",1,".$multi_details_id.")";
                            $result = Yii::$app->db->createCommand($sql)->execute();
                        }      
                    }
                    if ($model->imageFile) {
                        $model->imageFile->saveAs('uploads/'.$model->img);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        } 

        return $this->render('create', [
            'model' => $model,
            'details' => $details,
            'dropdown' => $dropdown
        ]);
    }

    public function actionAddImg($id)
    {
        $model = new ProductImg();
        $errors = '';
        if ($model->load(Yii::$app->request->post()) ) {
            $post = Yii::$app->request->post();
            $model->product_id = $id;
            $model->status = 1;
            $model->imageFile = UploadedFile::getInstance($model, 'img');
            if ($model->imageFile) {
                $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
            } else {
                Yii::$app->session->setFlash('danger', "Rasim yuklang!");
                return $this->redirect(["add-img",'id' => $id]);
            }
            
            if ($errors == '' and empty($errors)) {
                if($model->save()){
                    $product_id = $model->id;
                    $post = Yii::$app->request->post();
                    if ($model->imageFile) {
                        $model->imageFile->saveAs('uploads/'.$model->img);
                    }
                    return $this->redirect(['view','id' => $model->product_id]);
                }
            }
        } 

        return $this->render('add_img', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $details = Details::find()->all();

        $arr = [];
        $model_details = ProductDetailsMini::find()->where("product_id = :product_id",[':product_id' => $id])->all();
        foreach ($model_details as $key => $value) {
            $arr[$value->multi->id] = $value->multi->id;            
        }
        $file_id = $model->img;
        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'img');
            $post = Yii::$app->request->post();
            $SubCategory = $post["Products"]["sub_category_id"];
            $model->sub_category_id = $SubCategory;
            if (isset($model->imageFile) and !empty($model->imageFile)) {
                $data = Products::findOne($id);
                if (isset($data->img) and !empty($data->img)) {

                    unlink(dirname(__FILE__).'/../web/uploads/'.$data->img);
                }

                $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
                // echo $model->img;
            } else {
                $model->img = $file_id;
            }
            if ($model->save()) {
                
                $product_id = $model->id;
                $post = Yii::$app->request->post();
                $delete_sql = "delete from product_details_ where product_id = ".$product_id;
                $delete = Yii::$app->db->createCommand($delete_sql)->execute();
                if (isset($post["MultiDetails"]) and !empty($post["MultiDetails"])) {
                    foreach ($post["MultiDetails"] as $key => $value) {
                        $details_id = $value; 
                        $multi_details_id = $key;
                        $sql = "INSERT INTO product_details_ (product_id, details_id, status, multi_details_id) VALUES (".$product_id.",".$details_id.",1,".$multi_details_id.")";
                        $result = Yii::$app->db->createCommand($sql)->execute();
                    }      
                }
                if ($model->imageFile) {
                    $model->imageFile->saveAs('uploads/'.$model->img);
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }

            // if ($model->imageFile) {
                // $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
            // }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'details' => $details,
            'checked_details' => $arr

        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (isset($model->img) and !empty($model->img)) {
            unlink(dirname(__FILE__).'/../web/uploads/'.$model->img);
        }
        if ($model->delete()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDeleteImg($id)
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET['id'];
            $product_img = ProductImg::find()->where('id = :id',[":id" => $id])->one();
            if($product_img->delete()) {
                return [
                    'status' => 'success'
                ];
            }
        }
    }

    public function actionSaveMultiDetails()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            foreach ($_POST as $key => $value) {
                if (intval($key)) {
                    foreach ($value as $key2 => $value2) {
                        $product_id = $value2;
                        break;
                    }
                }
            }
            if ($_POST['detail_code'] != 0) {
                $detail_code = $_POST['detail_code'];
                $model = ProductDetailsCode::find()->where("product_id = :product_id and detail_code = :detail_code",[":product_id" => $product_id,":detail_code" => $detail_code])->all();
                if (isset($model) and !empty($model)) {
                    $check_date = new ProductDetailsCode;
                    $return = $check_date->checkDatesUpdate($_POST);
                    if ($return == 'true') {
                        foreach ($model as $key => $value) {
                            $value->delete();
                        }
                        return ["status" => "success"];
                    } else {
                        return $return;
                    }
                } else {
                    return ["status" => "failed",'error' => "Xatolik, Iltimos sahifani qayta yuklang!"];
                }
            } else {
                $model = new ProductDetailsCode;
                $return = $model->checkDates($_POST);
                if ($return = "true") {
                    return ["status" => "success"];
                }
            }
        }
        die("123");
    }

    public function actionDeleteCode() 
    {

        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_POST['id'];
            $delete_code = ProductDetailsCode::find()->where("detail_code = :detail_code",[":detail_code" => $id])->all();
            if (isset($delete_code) and !empty($delete_code)) {
                foreach ($delete_code as $key => $value) {
                    $value->delete();
                }
                return ["status" => "success"];
            }
        }       
    }

    public function actionTrend()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET["id"];
            $model = Products::find()->where("id = :id",["id" => $id])->one();
            // echo $model->status;
            // die();
            if ($model->status == 1) {
                $model->status = 2;
            } else if ($model->status == 2) {
                $model->status = 1;
            } else if ($model->status == 3) {
                $model->status = 4;
            } else if ($model->status == 4) {
                $model->status = 3;
            }
            if ($model->save()) {
                return ["status" => "success"];
            }
        }
    }
    
    public function actionNewProducts() 
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET["id"];
            $model = Products::find()->where("id = :id",["id" => $id])->one();
            // echo $model->status;
            // die();
            if ($model->status == 1) {
                $model->status = 3;
            } else if ($model->status == 3) {
                $model->status = 1;
            } else if ($model->status == 2) {
                $model->status = 4;
            } else if ($model->status == 4) {
                $model->status = 2;
            }
            if ($model->save()) {
                return ["status" => "success"];
            }
        }
    }

    public function actionSoldProducts()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET["id"];
            $model = Products::find()->where("id = :id",["id" => $id])->one();

            if ($model->sold_product == 0) {
                $model->sold_product = 1;
            } else if ($model->sold_product = 1) {
                $model->sold_product = 0;
            }
            if ($model->save()) {
                return ["status" => "success"];
            }
        }   
    }
}