<?php

namespace app\controllers;

use Yii;
use app\models\OrderCode;
use app\models\ArriveOrders;
use app\models\ClientOrderCamments;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderCodeController implements the CRUD actions for OrderCode model.
 */
class OrderCodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
            if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderCode models.
     * @return mixed
     */
    public function actionIndex()
    {   
        $dataProvider = new ActiveDataProvider([
            'query' => OrderCode::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderCode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);
        // echo "<pre>";
        // foreach ($model->orders as $key => $value) {
        //     foreach ($value->details as $key2 => $value2) {
        //         foreach($value2->multidetails as $key3 => $value3) {
        //             print_r($value3);
        //             // echo $value3["title_uz"] . "<br>" ;
        //         }
        //     }

        // }
        // pre($model->orders);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new OrderCode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrderCode();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrderCode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderCode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model2 = ArriveOrders::find()->where('order_code_id = :order_code_id',[":order_code_id" => $id])->all();
        // foreach ($model2 as $value) {
        //     $value->delete();
        // }
        if ($model->delete()) {
            return $this->redirect(["index"]);
        }
    }
    public function actionConfirm($id)
    {
        
        $model = $this->findModel($id);

        $chat_id = $model->client["chat_id"];
        if (isset($chat_id) && !empty($chat_id)) {

            function bot($method, $data = []){
                $url = 'https://api.telegram.org/bot1975190483:AAHZBeqUbPWr3UXvjiSC9rSenjRBF69jFDM/'.$method;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $res = curl_exec($ch);

                if(curl_error($ch))
                {
                 var_dump(curl_error($ch));   
                }
                else{
                    return json_decode($res);
                }
            }

            $get_result = bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "Sizning buyurtmangiz qabul qilindi!\nTez orada siz bilan aloqaga chiqamiz"
            ]);
            // pre();
            if ($get_result->ok === true) {
                $model->billing_status = 5;
                if ($model->save()) {
                    $this->redirect(["index"]);
                }
            }

        } else {
            echo "Chat id is empty";
            die();
        }
    }

    public function actionCheck($id)
    {
        $model = $this->findModel($id);

        $chat_id = $model->client["chat_id"];
        if (isset($chat_id) && !empty($chat_id)) {

            function bot($method, $data = []){
                $url = 'https://api.telegram.org/bot1975190483:AAHZBeqUbPWr3UXvjiSC9rSenjRBF69jFDM/'.$method;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $res = curl_exec($ch);

                if(curl_error($ch))
                {
                 var_dump(curl_error($ch));   
                }
                else{
                    return json_decode($res);
                }
            }

            $get_result = bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "Sizning buyurtmangiz tasdiqlandi!\nTez orada siz bilan aloqaga chiqamiz"
            ]);
            // pre();
            if ($get_result->ok === true) {
                $model->billing_status = 6;
                if ($model->save()) {
                    $this->redirect(["index"]);
                }
            }

        } else {
            echo "Chat id is empty";
            die();
        }
    }
    public function actionWay($id)
    {
        $model = $this->findModel($id);

        $chat_id = $model->client["chat_id"];
        if (isset($chat_id) && !empty($chat_id)) {

            function bot($method, $data = []){
                $url = 'https://api.telegram.org/bot1975190483:AAHZBeqUbPWr3UXvjiSC9rSenjRBF69jFDM/'.$method;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $res = curl_exec($ch);

                if(curl_error($ch))
                {
                 var_dump(curl_error($ch));   
                }
                else{
                    return json_decode($res);
                }
            }

            $get_result = bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "Sizning Buyurtmangiz yo`lga chiqdi!\nTez orada siz bilan aloqaga chiqamiz"
            ]);
            // pre();
            if ($get_result->ok === true) {
                $model->billing_status = 7;
                if ($model->save()) {
                    $this->redirect(["index"]);
                }
            }

        } else {
            echo "Chat id is empty";
            die();
        }
    }
    public function actionDone($id)
    {
        $model = $this->findModel($id);

        $chat_id = $model->client["chat_id"];
        if (isset($chat_id) && !empty($chat_id)) {

            function bot($method, $data = []){
                $url = 'https://api.telegram.org/bot1975190483:AAHZBeqUbPWr3UXvjiSC9rSenjRBF69jFDM/'.$method;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                $res = curl_exec($ch);

                if(curl_error($ch))
                {
                 var_dump(curl_error($ch));   
                }
                else{
                    return json_decode($res);
                }
            }
            $clb_camment = "camment_".$id;
            $order_camment_btn = json_encode([
                'inline_keyboard' =>[
                    [
                        [
                            'callback_data' => $clb_camment,
                            'text'=> "✉️ Xabar yuborish"
                        ],
                        
                    ],
                ]
            ]);

            $get_result = bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "Sizning Buyurtmangiz yetkazib berildi!\nTez orada siz bilan aloqaga chiqamiz.Bu yurtma haqida fikringizni bildirishingizni so`rab qolamiz!",
                'reply_markup' => $order_camment_btn
            ]);
            if ($get_result->ok === true) {
                $model->billing_status = 8;
                if ($model->save()) {
                    $this->redirect(["index"]);
                }
            }

        } else {
            echo "Chat id is empty";
            die();
        }
    }

    /**
     * Finds the OrderCode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderCode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderCode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
