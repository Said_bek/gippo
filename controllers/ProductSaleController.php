<?php

namespace app\controllers;

use Yii;
use app\models\ProductSale;
use app\models\Products;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductSaleController implements the CRUD actions for ProductSale model.
 */
class ProductSaleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductSale models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductSale::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductSale model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductSale model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductSale();
        $products = Products::find()->where(["status" => 1])->all();
        $current_date = date("Y-m-d h:i");
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $product_id = $post["ProductSale"]["product_id"];
            $sale_price = $post["ProductSale"]["sale_price"];
            $enter_date = $post["Section"]["product_sale"]["enter_date"];
            $end_date = $post["Section"]["product_sale"]["end_date"];
            $product = Products::find()->where('id = :id' ,[":id" => $product_id])->one();
            if (isset($product) and !empty($product)) {
                $price_product = $product->price;
                if ($sale_price > $price_product) {
                    Yii::$app->session->setFlash('danger', "Chegirma narxi mahsulot narxidan katta bo`lishi kerak emas!");
                    return $this->redirect(Yii::$app->request->referrer);
                } else if ($sale_price < 0) {
                    Yii::$app->session->setFlash('danger', "Chegirma narxi 0dan kichik bo`lishi mumkin emas!");
                    return $this->redirect(Yii::$app->request->referrer);
                } else if ($enter_date < $current_date or $end_date < $current_date) {
                    Yii::$app->session->setFlash('danger', "Chegirma boshlanish va tugash vaqti hozirgi vaqtdan katta bo`lishi kerak");
                    return $this->redirect(Yii::$app->request->referrer);
                } else if ($end_date < $enter_date) {
                    Yii::$app->session->setFlash('danger', "Chegirma boshlanish vaqti  tugash vaqtidan keyin bo`lmasligi kerak!");
                    return $this->redirect(Yii::$app->request->referrer);
                } else {
                    $sale_product = Products::findOne($product_id);
                    $product_price = $sale_product["price"];
                    $one_pracent = $product_price / 100;
                    // echo $one_pracent;
                    $sale_pracent = 100 - ($sale_price / $one_pracent);
                    $model->product_id = $product_id;
                    $model->sale_price = $sale_price;
                    $model->sale_pracent = $sale_pracent;
                    $model->enter_date = $enter_date;
                    $model->end_date = $end_date;
                    if ($model->save()) {
                        return $this->redirect(['index']);
                    } else {
                        print_r($model->errors);
                        die();
                    }

                }
            }
        }


        return $this->render('create', [
            'model' => $model,
            'products' => $products
        ]);
    }

    /**
     * Updates an existing ProductSale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $products = Products::find()->where("status != :status",["status" => 2])->all();
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            $product_id = $post["ProductSale"]["product_id"];
            $sale_price = $post["ProductSale"]["sale_price"];
            $enter_date = $post["Section"]["product_sale"]["enter_date"];
            $end_date = $post["Section"]["product_sale"]["end_date"];
            $sale_product = Products::findOne($product_id);
            $product_price = $sale_product["price"];
            $one_pracent = $product_price / 100;
            // echo $one_pracent;
            $sale_pracent = 100 - ($sale_price / $one_pracent);
            $model->product_id = $product_id;
            $model->sale_price = $sale_price;
            $model->sale_pracent = $sale_pracent;
            $model->enter_date = $enter_date;
            $model->end_date = $end_date;
            if ($model->save()) {
                return $this->redirect(['index']);
            } else {
                print_r($model->errors);
                die();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'products' => $products
        ]);
    }

    /**
     * Deletes an existing ProductSale model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionFind()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
            $id = intval($_GET['input']);

            $model = Products::findOne($id);

            if (isset($model)) {
                return [
                    'status' => 'success',
                    'sum' => $model->price,
                    'dollar' => $model->dollar
                 ];    
            }
            

        }
    }

    /**
     * Finds the ProductSale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductSale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductSale::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
