<?php

namespace app\controllers;
use app\models\Control;
use app\components\ClickData;
use app\models\ClickTransactions;
use app\models\OrderCode;
use app\models\Orders;
use app\models\User;
use app\models\Client;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;

class ClickController extends Controller
{
    private $reqData = [];
    private $user; // init into validateData()
    private $userID = '';
    public function beforeAction($action)
    {
        if ($action->id == "prepare" || $action->id == "complete") {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionPrepare()
    {
        $this->reqData = $_POST;
        
        /*$model = new Control();
        $model->error='action prepare';
        $model->save();*/
        
        $this->validateData();
        $checkExists = ClickTransactions::find()
            ->where(['click_trans_id' => $this->reqData['click_trans_id']])->one();
        if ($checkExists !== NULL) {
            if ($checkExists->status == ClickTransactions::STATUS_CANCEL) {
                //Transaction cancelled
                die(json_encode(ClickData::getMessage('9')));
            } //Already paid
            else die(json_encode(ClickData::getMessage('4')));
        }
        //Error in request from click
        if (!$this->reqData['error'] == 0) die(json_encode(ClickData::getMessage('8')));
        $newTransaction = new ClickTransactions;
        $newTransaction->user_id        = $this->reqData['merchant_trans_id'];
        $newTransaction->click_trans_id = $this->reqData['click_trans_id'];
        $newTransaction->service_id     = $this->reqData['service_id'];
        $newTransaction->amount         = $this->reqData['amount'];
        $newTransaction->sign_time      = $this->reqData['sign_time'];
        $newTransaction->click_paydoc_id = $this->reqData['click_paydoc_id'];
        $newTransaction->create_time    = time();
        $newTransaction->status         = ClickTransactions::STATUS_INACTIVE;
        if ($newTransaction->save(false)) {
            $merchant_prepare_id = $newTransaction->id;
            $return_array = array(
                'click_trans_id' => $this->reqData['click_trans_id'],        // ID Click Trans
                'merchant_trans_id' => $this->reqData['merchant_trans_id'],  // ID платежа в биллинге Поставщика
                'merchant_prepare_id' => $merchant_prepare_id                // ID платежа для подтверждения
            );
            
             

            
           
            
            $result = array_merge(ClickData::getMessage('0'), $return_array);
            $token = '1941602594:AAHkdOh2vtj5jgq7TrBbhXeYOSAZw4srzXw';

            $msg = json_encode($result);
            $user_id = 284914591;
            $arr = [
              'chat_id' => $user_id,
              'parse_mode' => 'html',
              'text' => $msg
            ];

            $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?' . http_build_query($arr);
            file_get_contents($url);
            die(json_encode($result));
        }
        // other case report: Unknown Error
        die(json_encode(1));
    }

    public function actionComplete()
    {
        $this->reqData = $_POST;
        
        /*$model = new Control();
        $model->error='Action Complete';
        $model->save();*/
        
        //if not validated it is end point
        //-------------------------------------------
        
        $this->validateData();
        //-------------------------------------------
        //Error in request from click
        
        if(empty($this->reqData['merchant_prepare_id'])){
            $model = new Control();
            $model->error='Post malumotlar kelmayopti';
            $model->save();
        }
        
        if(empty($this->reqData['merchant_prepare_id'])) die(json_encode(ClickData::getMessage('8')));
        // --------------------------------------------------------------------------- Start trasaction DB
        
        $transaction = ClickTransactions::findOne(
            [
                'id' => $this->reqData['merchant_prepare_id'],
                'user_id' => $this->reqData['merchant_trans_id'],
                'click_trans_id' => $this->reqData['click_trans_id'],
                'click_paydoc_id' => $this->reqData['click_paydoc_id'],
                'service_id' => $this->reqData['service_id'],
            ]
        );
        
        
        
        if ($transaction !== NULL) {
            if ($this->reqData['error'] == 0) {
                
                /*$model = new Control();
                $model->error='Error '.$this->reqData['error'];
                $model->tr_id=$transaction->id;
                $model->save();*/
                
                if ($this->reqData['amount'] == $transaction->amount) {
                    
                    /*$model = new Control();
                    $model->error='Narx teng. Error '.$this->reqData['error'];
                    $model->tr_id=$transaction->id;
                    $model->save();*/
                    
                    
                    // status = 0
                    if($transaction->status == ClickTransactions::STATUS_INACTIVE) {
                        
                        /*$model = new Control();
                        $model->error='Status=0 Error '.$this->reqData['error'];
                        $model->tr_id=$transaction->id;
                        $model->save();*/
                
                        $db = \Yii::$app->db;
                        $db_transaction = $db->beginTransaction();
                        $transaction->status = ClickTransactions::STATUS_ACTIVE;
                        if (!$transaction->save(false)){
                            
                            /*$model = new Control();
                            $model->error='Transaction save Orders pasti '.$this->reqData['error'];
                            $model->tr_id=$transaction->id;
                            $model->save();*/
                            
                            $db_transaction->rollback();
                            die(json_encode(ClickData::getMessage('n')));
                        }
                        
                        // $token = '1324203346:AAGdPORQJTAKX8zayO-lamW9NcZV-qh-fys';
                        // $msg = 'click';
                        // $user_id = -1001461544450;
                        // $arr = [
                        //   'chat_id' => $user_id,
                        //   'text' => $msg
                        // ];
                        // $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?' . http_build_query($arr);
                        // file_get_contents($url);


                        
                        $db_transaction->commit();
                        
                        $return_array = [
                            'click_trans_id' => $transaction->click_trans_id,
                            'merchant_trans_id' => $transaction->user_id,
                            'merchant_confirm_id' => $transaction->id,
                        ];

                        
                        $result = array_merge(ClickData::getMessage('0'), $return_array);
    
                        $token = '1941602594:AAHkdOh2vtj5jgq7TrBbhXeYOSAZw4srzXw';
            
                        $msg = json_encode($result);
                        $user_id = 284914591;
                        $arr = [
                          'chat_id' => $user_id,
                          'parse_mode' => 'html',
                          'text' => $msg
                        ];
            
                        $url = 'https://api.telegram.org/bot' . $token . '/sendMessage?' . http_build_query($arr);
                        file_get_contents($url);

                        // // ---- sms -----

                        // $orderCode = OrderCode::findOne($transaction->user_id);
                        // $orderCode->billing_status = 4;
                        // $orderCode->save();
                        // $client = Client::findOne($orderCode->client_id);
                        
                        // // $ph = "998998949889";
                        // $ph = "998935779247";
                        // $json = array();
                        // $json['messages'][] = [
                        //     'recipient' => $ph,
                        //     'message-id' => 'ext1580891537ivVH',
                        //     'sms' => [
                        //         'originator' => '3700',
                        //         'content' => [
                        //             'text' => '+'.$client->phone_number.' рақамли мижоз тўлов қилди. Буюртма рақами #'.$orderCode->order_code,
                        //         ]
                        //     ],
                        // ];
                        // $json = json_encode($json);

                        // // bu yerda sms ketadigan joy boladi
                        // $curl = curl_init();
                        // curl_setopt_array($curl, array(
                        //     // CURLOPT_PORT => "8080",
                        //     CURLOPT_URL => "http://91.204.239.44/broker-api/send",
                        //     CURLOPT_RETURNTRANSFER => true,
                        //     CURLOPT_ENCODING => "",
                        //     CURLOPT_MAXREDIRS => 10,
                        //     CURLOPT_TIMEOUT => 30,
                        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        //     CURLOPT_CUSTOMREQUEST => "POST",
                        //     CURLOPT_POSTFIELDS => "$json",
                        //     CURLOPT_HTTPHEADER => array(
                        //         "Authorization:  Basic bXlncnV6OmZFU3pwdzVQ",
                        //         "Cache-Control: no-cache",
                        //         "Content-Type: application/json",
                        //     ),
                        // ));

                        // $response = curl_exec($curl);
                        // $err = curl_error($curl);
                        // curl_close($curl);


                        // ---- end sms ----
                        
                        
                        die(json_encode($result));
                        
                    }
                    elseif ($transaction->status == ClickTransactions::STATUS_CANCEL) {
                        /*$model = new Control();
                        $model->error='Status=-1 Error '.$this->reqData['error'];
                        $model->tr_id=$transaction->id;
                        $model->save();*/
                        //"Transaction cancelled"
                        die(json_encode(ClickData::getMessage('9')));
                    }
                    // status = 1
                    elseif ($transaction->status == ClickTransactions::STATUS_ACTIVE) {
                        /*$model = new Control();
                        $model->error='Status=1 Error '.$this->reqData['error'];
                        $model->tr_id=$transaction->id;
                        $model->save();*/
                        
                        die(json_encode(ClickData::getMessage('4')));
                    } else{ 
                        
                        /*$model = new Control();
                        $model->error='Status=-n Error '.$this->reqData['error'];
                        $model->tr_id=$transaction->id;
                        $model->save();*/
                        
                        die(json_encode(ClickData::getMessage('n')));
                    }
                }else{
                    if ($transaction->status == ClickTransactions::STATUS_INACTIVE)
                        //$transaction->delete();
                        //"Incorrect parameter amount"
                        die(json_encode(ClickData::getMessage('2')));
                }
            } else if ($this->reqData['error'] < 0) {
                
                /*$model = new Control();
                $model->error='Error '.$this->reqData['error'];
                $model->save();*/
                
                if ($this->reqData['error'] == -5017) {           // "Transaction cancelled"
                    if ($transaction->status != ClickTransactions::STATUS_ACTIVE) {
                        $transaction->status = ClickTransactions::STATUS_CANCEL;
                        if ($transaction->save(false)) {
                            // "Transaction cancelled"
                            $this->send_mail_complete($this->reqData, true);
                            die(json_encode(ClickData::getMessage('9')));
                        }
                        die(json_encode(ClickData::getMessage('n')));
                    } else die(json_encode(ClickData::getMessage('n')));
                } elseif ($this->reqData['error'] == -1 && $transaction->status == ClickTransactions::STATUS_ACTIVE) {
                    die(json_encode(ClickData::getMessage('4')));
                } else die(json_encode(ClickData::getMessage('n')));
            } // error > 0
            else {
                /*$model = new Control();
                $model->error='Error '.$this->reqData['error'];
                $model->save();*/
                
                die(json_encode(ClickData::getMessage('n')));
            }
        } // Transaction is null
        else{
            /*$model = new Control();
            $model->error='Bazada malumotlar yo\'q';
            $model->save();*/
            
            // Transaction does not exist
            /*$model = new Control();
            $model->error='Complete. Bazadan malumotlar topilmadi';
            $model->save();*/
            die(json_encode(ClickData::getMessage('6')));
        }
        //echo "Hello from Complete // ";
        print_r(ClickData::getMessage(0));
        // var_dump(ClickData::$messages);

    }

    private function validateData()
    {
        //check complete parameters: Unknown Error
        if ((!isset($this->reqData['click_trans_id'])) ||
            (!isset($this->reqData['service_id'])) ||
            (!isset($this->reqData['click_paydoc_id'])) ||
            (!isset($this->reqData['merchant_trans_id'])) ||
            (!isset($this->reqData['amount'])) ||
            (!isset($this->reqData['action'])) ||
            (!isset($this->reqData['sign_time'])) ||
            (!isset($this->reqData['sign_string'])) ||
            (!isset($this->reqData['error']))
        ) {
            die(json_encode(ClickData::getMessage('n')));
        }
        // Формирование ХЭШ подписи
        $sign_string_veryfied = md5(
            $this->reqData['click_trans_id'] .
            $this->reqData['service_id'] .
            ClickData::SECRET_KEY .
            $this->reqData['merchant_trans_id'] .
            (($this->reqData['action'] == 1) ? $this->reqData['merchant_prepare_id'] : '') .
            $this->reqData['amount'] .
            $this->reqData['action'] .
            $this->reqData['sign_time']
        );
        if ($this->reqData['sign_string'] != $sign_string_veryfied) {
            die(json_encode(ClickData::getMessage('1')));
        }
        // Check Actions: Action not found
        if (!in_array($this->reqData['action'], [0, 1])) die(json_encode(ClickData::getMessage('3')));
        // Check sum: Incorrect parameter amount
        if (($this->reqData['amount'] < ClickData::MIN_AMOUNT) || ($this->reqData['amount'] > ClickData::MAX_AMOUNT)) {
            die(json_encode(ClickData::getMessage('2')));
        }
        //
        $this->user = OrderCode::findOne($this->reqData['merchant_trans_id']);
        if ($this->user === NULL) {
            // User does not exist
            die(json_encode(ClickData::getMessage('5')));
        }
    }
    
    private function send_mail_complete($data, $notcomplete = false)
    {
        if (!$notcomplete) {
            $message = <<<MESSAGE
                        <p>Message</p>
MESSAGE;
            $subject_text = 'Оплата CLICK';
        } else {
            $message = <<<MESSAGE
                        <p>Message</p>
MESSAGE;
            $subject_text = 'Отмена CLICK';
        }
        Yii::$app->mailer->compose()
            ->setFrom('')
            ->setTo([''])
            ->setSubject($subject_text)
            ->setHtmlBody($message)
            ->send();
    }
    
    
    public function actionDone(){

        $sql = "SELECT 
            click_transactions.id as id,
            order_code.id as oc_id 
        from order_code 
        inner join click_transactions on order_code.id = click_transactions.user_id 
        where order_code.client_id = ".Yii::$app->user->identity->id." 
        order by order_code.id DESC limit 1";

        $ordercode = OrderCode::findBySql($sql)->asArray()->one();
        
        $status = $_GET['payment_status'];
        
        $status = Html::encode($status);
        if($status == -10000){
            $oc = OrderCode::find()->orderBy(['id'=>SORT_DESC])->limit(1)->one();
                if($oc->delete())
                    return $this->redirect('/site/index');
        }
        
        if($status <= 0){
            if(empty($ordercode)){
                $oc = OrderCode::find()->orderBy(['id'=>SORT_DESC])->limit(1)->one();
                if($oc->delete())
                    return $this->redirect('/site/click');
            }else{
                $oc = OrderCode::findOne($ordercode['oc_id']);
                $ct = ClickTransactions::findOne($ordercode['id']);
                if($oc->delete() and $ct->delete()){
                    return $this->redirect('/site/click');
                }
            }
        }
        
        if(!empty($ordercode)){
            Yii::$app->db->createCommand("UPDATE click_transactions SET status=".$status." WHERE id=".$ordercode['id']."")->execute();
        }
        
        return $this->redirect('/site/click');
    }
}




