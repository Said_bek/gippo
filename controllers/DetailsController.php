<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Details;
use app\models\MultiDetails;

/**
 * DetailsController implements the CRUD actions for Details model.
 */
class DetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
            if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Details models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Details::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Details model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = MultiDetails::find()->where("details_id = :details_id",[":details_id" => $id])->all();
        $detail = Details::find()->where("id = :id",[":id" => $id])->one();
        $its_color = false;
        if ($detail->type == 3){
            $its_color = true;
        }
        return $this->render('view',[
            'model' => $model,
            'detail' => $detail,
            'its_color' => $its_color
        ]);
    }

    /**
     * Creates a new Details model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Details();
        $brand = Details::find()->where("type = 2")->one();
        $color = Details::find()->where("type = 3")->one();
        if (isset($brand) and !empty($brand)) {
            $is_brand = true;
        } else {
            $is_brand = false;
        }
        if (isset($color) and !empty($color)) {
            $is_color = true;
        } else {
            $is_color = false;
        }
        if ($model->load(Yii::$app->request->post())) {
            $post = Yii::$app->request->post();
            if (isset($post["is_brand"]) and !empty($post["is_brand"]) and isset($post["is_color"]) and !empty($post["is_color"])) {
                Yii::$app->session->setFlash('danger', "Bir vaqtda brand va rangni kiritb bo`lmaydi!");
                return $this->redirect(Yii::$app->request->referrer);
            } else if (isset($post["is_brand"]) and !empty($post["is_brand"])) {
                $type = $post["is_brand"];
                if ($type == 2) {
                    $model->type = 2;
                }

            } else if (isset($post["is_color"]) and !empty($post["is_color"])) {
                $color = $post["is_color"];
                if ($color == 3) {
                    $model->type = 3;
                }
            }
            $model->status = 1;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'is_brand' => $is_brand,
            'is_color' => $is_color
        ]);
    }

    /**
     * Updates an existing Details model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Details model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Details model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Details the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Details::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAddMultiDetail()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                $error = '';
                if ($_GET["s"] == 'true') {
                    $id = $_GET['id'];
                    $title_uz = $_GET['title_uz'];
                    $title_ru = $_GET['title_ru'];

                    if (!(isset($title_uz) and !empty($title_uz))) {
                        $error = true;
                        return [
                            'status' => 'error_name',
                        ];
                    }

                    if (!(isset($title_ru) and !empty($title_ru))) {
                        $error = true;
                        return [
                            'status' => 'error_name_ru',
                        ];
                    }
                    if (!(isset($id) and !empty($id)) and $id > 0) {
                        $error = true;
                        return [
                            'status' => 'id',
                        ];
                    }
                    if (!$error){
                        $model = new MultiDetails();
                        $model->title_uz = $title_uz;
                        $model->title_ru = $title_ru;
                        $model->details_id = $id;
                        $model->status = 1;
                        if ($model->save()) {
                            return [
                                'status' => 'success'
                            ];
                        }
                    }
                } else {
                    $model = new MultiDetails();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('add-multi-detail-modal.php',[
                            'model' => $model,
                        ]),
                    ];
                }
            }
        }
    }

    public function actionDeleteMultiDetail()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET['id'];
            $sub_category = MultiDetails::find()->where('id = :id',[":id" => $id])->one();
            if($sub_category->delete()) {
                return [
                    'status' => 'success'
                ];
            }
        }
    }

    public function actionUpdateMultiDetail()
    {
        if (Yii::$app->request->isAjax) {
            $error ='';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($_GET['s'] == 'true') {
                $id = $_GET['id'];
                $title_uz = $_GET['title_uz'];
                $title_ru = $_GET['title_ru'];

                if (!(isset($title_uz) and !empty($title_uz))) {
                    $error = true;
                    return [
                        'status' => 'error_name',
                    ];
                }

                if (!(isset($title_ru) and !empty($title_ru))) {
                    $error = true;
                    return [
                        'status' => 'error_name_ru',
                    ];
                }
                if (!(isset($id) and !empty($id)) and $id > 0) {
                    $error = true;
                    return [
                        'status' => 'id',
                    ];
                }
                if (!$error){
                    $model = MultiDetails::find()->where('id = :id', [":id" => $id])->one();
                    $model->title_uz = $title_uz;
                    $model->title_ru = $title_ru;
                    if ($model->save()) {
                        return [
                            'status' => 'success'
                        ];
                    }
                }
            } else {
                $id = $_GET["id"];
                $model = MultiDetails::find()->where("id = :id",[":id" => $id])->one();
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('update-detail-modal.php',[
                        'model' => $model,
                    ]),
                ];
            }
        } 
    }

}
