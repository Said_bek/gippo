<?php

namespace app\controllers;

date_default_timezone_set('Asia/Tashkent');

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Carusel;
use app\models\Category;
use app\models\Products;
use app\models\Sponsors;
use app\models\ProductSale;
use app\models\Blogs;
use app\models\Details;
use app\models\ProductDetailsCode;
use app\models\ProductImg;
use app\models\Reviews;
use app\models\Region;
use app\models\OrderCode;
use app\models\Orders;
use app\models\ArriveOrders;
use app\models\Jwt;
use app\models\Clients;
use app\models\SubCategory;
use app\models\Cashback;

class SiteController extends Controller

{
    /**
     * {@inheritdoc}
     */

    public function init()
    {
        Yii::$app->session->open();
        parent::init();
        if (isset($_SESSION['lang']) && $_SESSION['lang'] != NULL){
            Yii::$app->language = $_SESSION['lang'];
        }
        else {
            Yii::$app->language = 'uz';
        }
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'frontend',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        // echo Yii::$app->language;
        $this->layout = 'frontend';
        $current_date = date("Y-m-d H:i:s");
        $carusel = Carusel::find()->all();

        $category = Category::find()->where('status = :status',[":status" => 1])->orderBy(['id' => SORT_ASC])->limit(2)->all();
        $sponsors = Sponsors::find()->all();
        $trend = Products::find()->where("status = :status or status = :status2", [':status' => 2,':status2' => 4])->all();
        $best_sale_arr = [];
        foreach ($category as $key => $value) {
            $best_sale_arr[$value["id"]] = Products::find()->where("main_category = :main_category",[":main_category" => $value["id"]])->orderBy(["sell_count" => SORT_DESC ])->limit(10)->all(); 
        }
        $blogs = Blogs::find()->all();
        $color_id = 0;
        $color = Details::find()->where("type = :type",[":type" => 3])->one();
        if (isset($color) and !empty($color)) {
            $color_id = $color->id;
        }
        // $sale_products = ProductSale::find()->where('enter_date < :current_date and :current_date < end_date',[":current_date" => $current_date])->all();
        $sale_products = ProductSale::find()->all();
        // pre($best_sale_arr);

        return $this->render('/site/index',[
            'carusel' => $carusel,
            'category' => $category,
            'sponsors' => $sponsors,
            'trend' => $trend,
            'best_saler' => $best_sale_arr,
            'blogs' => $blogs,
            'color_id' => $color_id,
            'sale_products' => $sale_products,
        ]);
    }

    public function actionCategory()
    {
        $this->layout = 'frontend';
        if (Yii::$app->request->get()) {
            $get = Yii::$app->request->get();
            $id = intval($get["id"]);
            $session = Yii::$app->session;
            $category = Category::find()->all();
            $details = Details::find()->all();
            $color = Details::find()->where("type = :type",[":type" => 3])->one();
            $color_id = 0;
            if (isset($color) and !empty($color)) {
                $color_id = $color->id;
            }
            $sql = "SELECT * FROM products as pr WHERE main_category = ".$id." or pr.sub_category_id=".$id." order by id asc limit ".Products::LIMIT_PRODUCT;
            $products = Products::findBySql($sql)->all();
            $count_product = count($products);
            if (isset(Yii::$app->session["list"]) and !empty(Yii::$app->session["list"])) {
                $session["list"]["product"] = $products;
            } else{
                $session["list"] = new \ArrayObject();
                $session["list"]["product"] = $products;
            }
            $session["list"]["sql"] =  "pr.sub_category_id = ".$id." or main_category = ".$id;
            $key = $count_product - 1;

            $session["list"]["last_id"] = $products[$key]->id;

            $product_trend = Products::find()->where("status = :status or status = :status2",[":status" => 2, ":status2" => 4])->all();
        
            return $this->render('category',[
                'category' => $category,
                'color_id' => $color_id,
                'details' => $details,
                'product_trend' => $product_trend,
            ]);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {


        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/order-code/index');
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/web/site/login');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
        return $this->render('about');
    }
    
    public function actionErrors()
    {
        $this->layout = 'frontend';
        return $this->render("errors");
    }
   
}