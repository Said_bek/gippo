<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yandex\Translate\Translator;
use Yandex\Translate\Exception;
use app\models\Category;
use app\models\SubCategory;
use app\models\Details;
use app\models\MultiDetails;
use app\models\Products;
use app\models\CurrencyPrice;
use app\models\ProductDetailsCode;
use app\models\ProductImg;

ini_set('display_errors', true);



function yandex_ru($word){
    return Yii::$app->translate->translate('en-US', 'ru-RU', $word)['text'][0];
}

function yandex_uz($word){
    return Yii::$app->translate->translate('en-US', 'uz-UZ', $word)['text'][0];
}

// https://app.automatio.co/instances?page=1

/**
 * AboutUsController implements the CRUD actions for AboutUs model.
 */
class ApiController extends Controller
{
    public function actionDetails()
    {

        // API1
        $url = 'https://app.automatio.co/api/instance/ckrz94tyg1458822hppbl87abgli/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=0';
           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $data = json_decode($result);

        $take = $data->rows;

        // API1
        $url = "https://app.automatio.co/api/instance/ckrz94tyg1458822hppbl87abgli/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=".$take;
        
        
           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $products = json_decode($result);
        $products = (array)$products->data;


        // pre($products);

        
        if (!empty($products)) {
            $arr = [];
            foreach ($products as $key => $value) {
                
                $arr[] = (array)$value;
            }

        }
        $categories = [];
        $links = [];
        $nolinks = [];
        $sku_products = [];
        $brands = [];
        $sizes = [];
        
        foreach ($arr as $key => $value) {
            $categories[$value['CATEGORY_1.text']][$value['CATEGORY 2.text']] = $value['CATEGORY 2.text'];

            $brands[$value['BRAND.text']] = $value['BRAND.text'];
            $sizes[$value['SIZE.text']] = $value['SIZE.text'];

            $sku_products[$value['SKU.text']] = $value['SKU.text'];
            if (isset($value['link'])) {
                $links[$value['link']] = $value;    
            }
            else{
                $nolinks[] = $value;
            }
            
        }
        $all_sizes = [];
        foreach ($sizes as $key => $value) {
            
            $new_arr = explode(' - ', $value);
            foreach ($new_arr as $key2 => $value2) {
                if ($value2 != '-') {
                    $all_sizes[$value2] = $value2;

                }
            }

        }

        

        // pre(explode('.', $sku_products['0002.9999.00.019.00139']));

        

        // ----------- start for category -------------- //
        
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!empty($categories)) {
                foreach ($categories as $key => $value) {
                    $find_cat = Category::find()
                    ->where(['title_en' => $key])
                    ->one();

                    if (!isset($find_cat)) {
                        $title_uz = yandex_uz($key);
                        $title_ru = yandex_ru($key);

                        $new_model = new Category(); 
                        $new_model->title_uz = $title_uz;       
                        $new_model->title_ru = $title_ru;       
                        $new_model->title_en = $key;       
                        $new_model->status = 1;
                        $new_model->save();
                    }

                    foreach ($value as $key2 => $value2) {
                        $find_sub_cat = SubCategory::find()
                        ->where([
                            'title_en' => $value2,
                        ])
                        ->one();

                        if (!isset($find_sub_cat)) {
                            $sub_title_uz = yandex_uz($value2);
                            $sub_title_ru = yandex_ru($value2);

                            $sub_cat = new SubCategory();
                            $sub_cat->title_uz = $sub_title_uz;
                            $sub_cat->title_ru = $sub_title_ru;
                            $sub_cat->title_en = $value2;
                            $sub_cat->status = 1;
                            $sub_cat->type = 0;
                            $sub_cat->main_category = ((isset($new_model))? $new_model->id : $find_cat->id);
                            $sub_cat->category_id = ((isset($new_model))? $new_model->id : $find_cat->id);
                            $sub_cat->save();
                        }
                    }
                }
            }
            
            $transaction->commit(); 
            

        }catch(NotFoundHttpException $e) {
            $transaction->rollback();
            pre('Failure Category');
        }

        // ----------- end for category -------------- //


        // ----------- start for brand -------------- //
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!empty($brands)) {
                foreach ($brands as $key => $value) {
                    $find_br = MultiDetails::find()
                    ->where(['title_en' => $key])
                    ->one();

                    if (!isset($find_br)) {
                        
                        $detail = Details::find()
                        ->where(['title_en' => 'BRAND.text'])
                        ->one();

                        if (isset($detail)) {
                            $new_model = new MultiDetails(); 
                            $new_model->title_uz = $value;       
                            $new_model->title_ru = $value;       
                            $new_model->title_en = $value;       
                            $new_model->status = 1;
                            $new_model->details_id = $detail->id;
                            $new_model->save();
                        }
                        else{
                            return false;
                        }

                        
                    }

                    
                }
            }
            
            $transaction->commit(); 
            // pre('Success');
            

        }catch(NotFoundHttpException $e) {
            $transaction->rollback();
            pre('Failure Brand');
        }
        // ----------- end for brand -------------- //


        // ----------- start for size -------------- //
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!empty($all_sizes)) {
                foreach ($all_sizes as $key => $value) {
                    $find_br = MultiDetails::find()
                    ->where(['title_en' => $key])
                    ->one();

                    if (!isset($find_br)) {
                        
                        $detail = Details::find()
                        ->where(['title_en' => 'SIZE.text'])
                        ->one();

                        if (isset($detail)) {
                            $new_model = new MultiDetails(); 
                            $new_model->title_uz = $value;       
                            $new_model->title_ru = $value;       
                            $new_model->title_en = $value;       
                            $new_model->status = 1;
                            $new_model->details_id = $detail->id;
                            $new_model->save();
                        }
                        else{
                            return false;
                        }

                        
                    }

                    
                }
            }
            
            $transaction->commit(); 
            // pre('Success Size');
            

        }catch(NotFoundHttpException $e) {
            $transaction->rollback();
            pre('Failure Size');
        }
        // ----------- end for size -------------- //
        
        
        

        curl_close($ch);
        return $this->render('index');
    }

    public function actionColors()
    {

        $success = '';
        // API3
        $url = "https://app.automatio.co/api/instance/cks018lxr1609425hppbq0espn26/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=0";

           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $data = json_decode($result);

        $take = $data->rows;

        // API3
        $url = "https://app.automatio.co/api/instance/cks018lxr1609425hppbq0espn26/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=".$take;
           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $products = json_decode($result);
        $products = (array)$products->data;


        if (!empty($products)) {
            $arr = [];
            foreach ($products as $key => $value) {
                
                $arr[] = (array)$value;
            }

        }

        $colors = [];
        
        
        foreach ($arr as $key => $value) {
            $colors[$value['COLOR.text']] = $value['COLOR.text'];

        }

        // ----------- start for colors -------------- //
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!empty($colors)) {
                foreach ($colors as $key => $value) {
                    $find_br = MultiDetails::find()
                    ->where(['title_en' => $key])
                    ->one();

                    if (!isset($find_br)) {
                        
                        $detail = Details::find()
                        ->where(['title_en' => 'COLOR.text'])
                        ->one();

                        if (isset($detail)) {
                            $new_model = new MultiDetails(); 
                            $new_model->title_uz = $value;       
                            $new_model->title_ru = $value;       
                            $new_model->title_en = $value;       
                            $new_model->color_name = $value;       
                            $new_model->status = 1;
                            $new_model->details_id = $detail->id;
                            $new_model->save();
                        }
                        else{
                            return false;
                        }

                        
                    }

                    
                }
            }
            
            $transaction->commit();
            $success .= 'Success Color';    
            

        }catch(NotFoundHttpException $e) {
            $transaction->rollback();
            $success .='Failure Color';
        }
        // ----------- end for colors -------------- //
        
        pre($success);

    }


    public function actionProductApi()
    {
        $success = '';
        $count = 0;
        $ssss = 0;
        $eeee = 0;
        $test = [];
         // API1
        $url = 'https://app.automatio.co/api/instance/ckrz94tyg1458822hppbl87abgli/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=0';
           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $data = json_decode($result);

        $take = $data->rows;

        // API1
        $url = "https://app.automatio.co/api/instance/ckrz94tyg1458822hppbl87abgli/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=".$take;

           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $products = json_decode($result);

        $products = (array)$products->data;

        $arr = [];
        $links_products = [];
        $nolinks = [];
        $errors = [];
        if (!empty($products)) {
            $arr = [];
            
            foreach ($products as $key => $value) {
                $arr[] = (array)$value;
            }
            
            $i = 0;
            foreach ($arr as $key => $value) {
                if ($i <= 100) {
                    if (!empty($value['link'])) {
                        $links_products[$value['link']] = $value; 
                        $i++;   
                    }
                    else{
                        $nolinks[] = $value;
                    }
                }
                
            }
        }

        
        // pre(count($links_products));


        // ----------- start for size -------------- //
        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (!empty($links_products)) {
                foreach ($links_products as $key => $value) {
                    $find_br = Products::find()
                    ->where(['sku' => $value['SKU.text']])
                    ->one();


                    if (!isset($find_br)) {
                        
                        $sub_category = SubCategory::find()
                        ->where(['title_en' => $value['CATEGORY 2.text']])
                        ->one();

                        $category = Category::find()
                        ->where(['title_en' => $value['CATEGORY_1.text']])
                        ->one();


                        $currency = CurrencyPrice::find()
                        ->where([
                            '<=','start_date',date('Y-m-d')
                        ])
                        ->andWhere([
                            '>=','end_date',date('Y-m-d')
                        ])
                        ->one();


                        $deatail_size = Details::find()
                        ->where([
                            'title_en' => 'SIZE.text'
                        ])
                        ->one();

                        $product_sizes = explode(' - ', $value['SIZE.text']);

                        // pre($product_sizes);

                        if (isset($deatail_size)) {
                            $sizes = MultiDetails::find()
                            ->where([
                                'details_id' => $deatail_size->id
                            ])
                            ->andWhere([
                                'in','title_en',$product_sizes
                            ])
                            ->all();

                        }

                        
                        if (
                            isset($sub_category) && 
                            isset($category) && 
                            isset($currency) &&
                            (isset($sizes) && !empty($sizes))
                        ) {

                            $title_uz = $value['NAME.text'];
                            $title_ru = $value['NAME.text'];
                            $made_uz = $value['MADE IN.text'];
                            $made_ru = $value['MADE IN.text'];
                            $desc_uz = ((!empty($value['SPEC.text'])) ? $value['SPEC.text'] : '' );
                            $desc_ru = ((!empty($value['SPEC.text'])) ? $value['SPEC.text'] : '' );
                            $dollar = 0;
                            if (!empty($value['PRICE.text'])) {
                                $dollar = preg_replace('#[^0-9\.,]#','',$value['PRICE.text']);    
                            }

                            

                            $price = $currency->sum*$dollar;

                            $sku = explode('.',$value['SKU.text']);
                            array_pop($sku);
                            $element = implode('.', $sku);


                            $new_model = new Products(); 
                            $new_model->title_uz = $title_uz;       
                            $new_model->title_ru = $title_ru;       
                            $new_model->title_en = $value['NAME.text'];       
                            $new_model->main_category = $category->id;       
                            $new_model->sub_category_id = $sub_category->id;       
                            $new_model->status = 1;
                            $new_model->sku_api = $element;
                            $new_model->dollar = $dollar;
                            $new_model->price = $price;
                            $new_model->sku = $value['SKU.text'];
                            $new_model->made = $value['MADE IN.text'];
                            $new_model->made_uz = $made_uz;
                            $new_model->made_ru = $made_ru;
                            $new_model->description_en = ((isset($value['SPEC.text']))? yandex_uz($value['SPEC.text']) : '' );
                            $new_model->description_uz = $desc_uz;
                            $new_model->description_ru = $desc_ru;
                            $new_model->link = $value['link'];
                            
                            
                            if(!$new_model->save()){
                                $success .= 'Failure products <br>';
                                $errors[] = $new_model->errors;
                            }
                            
                        }
                        else{

                            if (!isset($sizes)) {
                                $ssss++;
                            }
                            if (empty($sizes)) {
                                $eeee++;
                            }
                            $count++;
                        }

                    }
                    else{
                        $test[] = $find_br->id;
                    }

                    
                }
            }
            
            $transaction->commit(); 
            $success .= 'Success add new products <br>';
            

        }catch(NotFoundHttpException $e) {
            $transaction->rollback();
            $success .= 'Failure add new products <br>';
        }


        pre($success."<br>".$count."<br> Size Isset".$ssss."<br> size empty ".$eeee."<br> old product ");
        // ----------- end for size -------------- //

    }


    public function actionImages()
    {
        $success = '';
        // API3
        $url = "https://app.automatio.co/api/instance/cks018lxr1609425hppbq0espn26/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=0";

           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $data = json_decode($result);


        $take = $data->rows;

        // API3
        $url = "https://app.automatio.co/api/instance/cks018lxr1609425hppbq0espn26/00d1b44a533bfa2436d7acf023ef5cea5eb435bc/?take=".$take;
           
        $ch = curl_init($url);
        $headers = [];
        $headers[] = 'Content-Type:application/json';
        $headers[] = 'Authorization:eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmaXJzdE5hbWUiOiJTYXJkb3JiZWsiLCJsYXN0TmFtZSI6IkF0YXhhbm92IiwiZW1haWwiOiJhdGF4YW5vdi4yM0BtYWlsLnJ1IiwiaWQiOiJja3JiNG85YmwxNTE3NWhvbnRlN3ZqbThhYSIsImp0aSI6ImNrcmI0bzlibDE1MTc1aG9udGU3dmptOGFhIiwiaWF0IjoxNjI3Mjg1NjQ1fQ.0CtuoD9uSwZ0G6p8Mvkv4ulJeTCkf3hO90bjUOOKoM0';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           
        $result = curl_exec($ch);
        $products = json_decode($result);
        $products = (array)$products->data;


        // pre($products);

        
        if (!empty($products)) {
            $arr = [];
            foreach ($products as $key => $value) {
                $arr[] = (array)$value;
            }

        }
        
        $links_products = [];
        $nolinks = [];

        foreach ($arr as $key => $value) {
            
            $sku_arr = explode('.',$value['SKU.text']);
            array_pop($sku_arr);
            $element_arr = implode('.', $sku_arr);
            $quantity = 0;
            if (isset($value['Quantity.text'])) {
                $quantity = preg_replace('/[^0-9]/', '', $value['Quantity.text']);
            }
            /* 
                Quantity.text
                link
                SKU.text
                COLOR.text
                Images.image
            */


            $links_products[$element_arr][$value['COLOR.text']][$value['Images.image']] = [
                'main_image' => ((isset($value['link'])) ? $value['Images.image'] : ''),
                'quantity' => $quantity,
                'sku' => $value['SKU.text'],
            ];
            
        }

        
        foreach ($links_products as $key => $value) {

            $model = Products::find()
            ->where([
                'sku_api' => $key
            ])
            ->one();


            


            if (isset($model) && ($key == $model->sku_api)) {
                foreach ($value as $key2 => $value2) {
                    $multi_detail = MultiDetails::find()
                    ->where([
                        'title_en' => $key2
                    ])
                    ->one();


                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        if (isset($multi_detail)) {
                            $multi_detail_id = $multi_detail->id;
                            $detail_id = $multi_detail->details_id;
                            
                            $test_detail = ProductDetailsCode::find()
                            ->where([
                                'product_id' => $model->id,
                                'multi_detail_id' => $multi_detail_id,
                                'detail_id' => $detail_id,
                            ])
                            ->one();

                            $test_img = ProductImg::find()
                            ->where([
                                'product_id' => $model->id,
                                'multi_detail_id' => $multi_detail_id,
                            ])
                            ->one();

                            

                            if (!isset($test_detail) && !isset($test_img)) {
                                $product_details = new ProductDetailsCode();
                                $product_details->product_id = $model->id;
                                $product_details->multi_detail_id = $multi_detail_id;
                                $product_details->detail_id = $detail_id;

                                $code = ProductDetailsCode::find()
                                ->orderBy([
                                    'detail_code' => SORT_DESC
                                ])
                                ->one();

                                if (isset($code) && !empty($code)){
                                    $new_code = $code->detail_code+1;
                                    $product_details->detail_code = $new_code;
                                }
                                else{
                                    $product_details->detail_code = 1;
                                }

                                if (!$product_details->save()) {
                                    pre($product_details->errors);
                                }
                                else{
                                    $i = 1;
                                    foreach ($value2 as $key3 => $value3) {
                                        if (!empty($key3)) {
                                            $img = new ProductImg();
                                            $img->product_id = $model->id;
                                            $img->img = $key3;
                                            if ($i == 1) {
                                                $img->multi_detail_id = $multi_detail_id;
                                            }
                                            $i++;
                                            if (!$img->save())
                                                pre($img->errors);
                                        }
                                        
                                    }
                                }

                                $transaction->commit(); 
                                $success .= ' Success Images <br>';
                            }
                            
                        }

                        
                    }catch(NotFoundHttpException $e) {
                        $transaction->rollback();
                        $success .= ' Failure Images <br>';
                    }
                }
            }
            
        }

        pre($success);


    }

    public function actionUpdateProductImgs()
    {
        $model = Products::find()
        ->orderBy([
            'id' => SORT_ASC
        ])
        ->all();

        foreach ($model as $key => $value) {
            $img = ProductImg::find()
            ->where([
                'product_id' => $value->id
            ])
            ->orderBy(['product_id' => SORT_ASC])
            ->one();

            if (isset($img)) {
                $value->img = $img->img;
                if (!$value->save()) {
                    pre($value->errors);
                }    
            }

            
        }


        
    }


}
