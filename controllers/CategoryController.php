<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\models\Category;
use app\models\SubCategory;
use app\models\ProjectHelper;
use app\models\Products;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{

    /**
     * {@inheritdoc}
     */
    
    public function init()
    {
        parent::init();

        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Category::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = SubCategory::find()->where('category_id = :category_id and status = :status',[":category_id" => $id,':status' => 1])->all();
        $category = Category::find()->where('id = :category_id',[":category_id" => $id])->one();


        return $this->render('view', [
            'model' => $model,
            'category' => $category
        ]);
    }

    public function actionSubView($id)
    {
        $find_product = Products::find()->where('sub_category_id = :sub_category_id',[":sub_category_id" => $id])->all();
        if (isset($find_product) and !empty($find_product)) {
            $sub_category = SubCategory::find()->where("id = :id",[":id" => $id])->one();
            return $this->render('sub-product-view', [
                'sub_category' => $sub_category,
                'product' => $find_product
            ]);
            echo "SuccessFull11";
            die();
        } else {
            $model = SubCategory::find()->where("sub_category_id = :sub_category_id and status = :status",[":sub_category_id" => $id,':status' => 1])->all();
            $category = SubCategory::find()->where("id = :id",[":id" => $id])->one();
            return $this->render('sub-view', [
                'model' => $model,
                'category' => $category
            ]);
        }
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post())) {
            
            $model->status = 1;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                echo "<pre>";
                print_r($model->errors);
                echo "</pre>";
                exit;
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = 2;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAddSubCategory()
    {

        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset($_GET['s']) && !empty($_GET['s'])) {
                $error = '';
                if ($_GET['s'] == 'true') {
                    $id = $_GET['id'];
                    $title_uz = $_GET['title_uz'];
                    $title_ru = $_GET['title_ru'];

                    if (!(isset($title_uz) and !empty($title_uz))) {
                        $error = true;
                        return [
                            'status' => 'error_name',
                        ];
                    }

                    if (!(isset($title_ru) and !empty($title_ru))) {
                        $error = true;
                        return [
                            'status' => 'error_name_ru',
                        ];
                    }
                    if (!(isset($id) and !empty($id)) and $id > 0) {
                        $error = true;
                        return [
                            'status' => 'id',
                        ];
                    }
                    if (!$error){
                        $model = new SubCategory();
                        $model->title_uz = $title_uz;
                        $model->title_ru = $title_ru;
                        $model->category_id = $id;
                        $model->main_category = $id;
                        $model->type = 1;   
                        $model->status = 1;
                        if ($model->save()) {
                            return [
                                'status' => 'success'
                            ];
                        }
                    }
                } else {
                    $model = new SubCategory();
                    return [
                        'status' => 'success',
                        'content' => $this->renderAjax('add-sub-category-modal.php',[
                            'model' => $model,
                        ]),
                    ];
                }
            }
        }
    }

    public function actionAddSubCategoryStep($id)
    {
        $model = new SubCategory;
        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();
            $title_uz = $post["SubCategory"]["title_uz"];
            $title_ru = $post["SubCategory"]["title_ru"];
            $id;
            $category = SubCategory::find()->where("id = :id",[":id" => $id])->one();
            if (isset($category) and !empty($category)) {
                $model->main_category = $category->main_category;
            }
            $model->title_uz = $title_uz;
            $model->title_ru = $title_ru;
            $model->sub_category_id = $id;
            $sub_category_check = SubCategory::find()->where('id = :sub_category_id',[":sub_category_id" => $id])->one();
            if (isset($sub_category_check) and !empty($sub_category_check)) {
                $sub_category_check->type = 0;
                $sub_category_check->save();
                $model->type = 1;
                $model->status = 1;
                $model->step = 2;
                if ($model->save()) {
                     return $this->redirect(['sub-view', 'id' => $id]);
                }
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdateSubCategory()
    {
        if (Yii::$app->request->isAjax) {
            $error ='';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($_GET['s'] == 'true') {
                $id = $_GET['id'];
                $title_uz = $_GET['title_uz'];
                $title_ru = $_GET['title_ru'];

                if (!(isset($title_uz) and !empty($title_uz))) {
                    $error = true;
                    return [
                        'status' => 'error_name',
                    ];
                }

                if (!(isset($title_ru) and !empty($title_ru))) {
                    $error = true;
                    return [
                        'status' => 'error_name_ru',
                    ];
                }
                if (!(isset($id) and !empty($id)) and $id > 0) {
                    $error = true;
                    return [
                        'status' => 'id',
                    ];
                }
                if (!$error){
                    $model = SubCategory::find()->where('id = :id', [":id" => $id])->one();
                    $model->title_uz = $title_uz;
                    $model->title_ru = $title_ru;
                    if ($model->save()) {
                        return [
                            'status' => 'success'
                        ];
                    }
                }
            } else {
                $id = $_GET["id"];
                $model = SubCategory::find()->where("id = :id",[":id" => $id])->one();
                return [
                    'status' => 'success',
                    'content' => $this->renderAjax('update-sub-category-modal.php',[
                        'model' => $model,
                    ]),
                ];
            }
        } 
    }

    public function actionDeleteSubCategory()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = $_GET['id'];
            $sub_category = SubCategory::find()->where('id = :id',[":id" => $id])->one();
            $sub_category->status = 2;
            if($sub_category->save()) {
                return [
                    'status' => 'success'
                ];
            }
        }
    }

    public function actionChangePhoto($id)
    {

        $model = SubCategory::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->img) and !empty($model->img)) {
                unlink(dirname(__FILE__).'/../web/uploads/'.$model->img);
            }
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
            }

            if($model->save()){
                if ($model->imageFile) {
                    $model->imageFile->saveAs('uploads/'.$model->img);
                }

                return $this->redirect(['view', 'id' => $model->category_id]);
            }
            
            
        }

        return $this->render('change-photo',[
            'model' => $model,
        ]);
    }

    public function actionChangePhotoSub($id)
    {

        $model = SubCategory::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if (isset($model->img) and !empty($model->img)) {
                unlink(dirname(__FILE__).'/../web/uploads/'.$model->img);
            }
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->img = rand(1,100).strtotime(date('Y-m-d H:i:s')).'.'.$model->imageFile->extension;
            }

            if($model->save()){
                if ($model->imageFile) {
                    $model->imageFile->saveAs('uploads/'.$model->img);
                }

                return $this->redirect(['sub-view', 'id' => $model->sub_category_id]);
            }
            
            
        }

        return $this->render('change-photo',[
            'model' => $model,
        ]);
    }
}
