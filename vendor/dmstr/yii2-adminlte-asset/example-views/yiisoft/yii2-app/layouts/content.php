<?php
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <section class="content-header">
    </section>

    <section class="content box">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>