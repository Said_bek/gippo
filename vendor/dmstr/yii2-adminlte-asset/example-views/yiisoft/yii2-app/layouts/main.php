<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/select2.min.css">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript">
        // create sub_category
        $("#category").select2({
            tags: true,
        });
        $(document).on("click",".delete_cashback",function(){
            var id = $(this).attr('data-delete');
            $.ajax({
                url: '/cashbacks/delete-cashback',
                data: {
                    data_id: id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload();
                    }
                }
            })
        });
        $(document).on("click",".on_cashback",function(){
            $.ajax({
                url: '/cashbacks/on-cashback',
                dataType: 'json',
                type: 'get',
            })
            $(this).addClass("off_checkbox");
            $(this).removeClass("on_cashback");
        });
        $(document).on("click",".off_checkbox",function(){
            $.ajax({
                url: '/cashbacks/off-cashback',
                dataType: 'json',
                type: 'get',
            })
            $(this).addClass("on_cashback");
            $(this).removeClass("off_checkbox");
        });
        
        $(document).on("click",".add_sub_category",function(){
            var id = $(this).attr('data-id')
            $.ajax({
                url: 'add-sub-category',
                data: {
                    s: false,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $('.modal-body').html(response.content)
                        $('.modal-header').html("<h3>Sub Kategoriya qo'shish</h3>")
                        $('.modal-footer .btn-success').addClass('save_sub_category')
                        $('.modal-footer .btn-success').attr('data-id',id)
                    }
                }
            })
        });

        $(document).on('click', '.save_sub_category', function(){
            var id = $(this).attr('data-id')
            var title_uz = $('#title_uz').val()
            var title_ru = $('#title_ru').val()

            if (title_uz != '' && title_ru != ''){
                $.ajax({
                    url: 'add-sub-category',
                    data: {
                        s: true,
                        id:id,
                        title_uz: title_uz,
                        title_ru: title_ru,
                    },
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        if (response.status == 'success') {
                            location.reload()
                        } else if (response.status == 'error_name') {
                            $('#title_uz').addClass('border-red');
                            $('.name_error').removeClass('hidden')
                        }
                    }
                })
            } else {
                if (title_uz == '') {
                    $('#title_uz').addClass('border-red');
                    $('.name_error').removeClass('hidden')
                }
                if (title_ru == '') {
                    $('#title_ru').addClass('border-red');
                    $('.name_error_ru').removeClass('hidden')
                }
            }
        })
        // update sub_category
        $(document).on('click', '.update', function(){
            var id = $(this).attr('data-update')
            $.ajax({
                url: 'update-sub-category',
                data: {
                    s: false,
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $('.modal-body').html(response.content)
                        $('.modal-header').html("<h3>Saqlash</h3>")
                        $('.modal-footer .btn-success').addClass('change_sub_category')
                        $('.modal-footer .btn-success').attr('data-id',id)
                    }
                }
            })
        })

        $(document).on('click', '.change_sub_category', function(){
            var id = $(this).attr('data-id')
            var title_uz = $('#title_uz').val()
            var title_ru = $('#title_ru').val()

            if (title_uz != '' && title_ru != ''){
                $.ajax({
                    url: 'update-sub-category',
                    data: {
                        s: true,
                        id:id,
                        title_uz: title_uz,
                        title_ru: title_ru,
                    },
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        if (response.status == 'success') {
                            location.reload()
                        } else if (response.status == 'error_name') {
                            $('#title_uz').addClass('border-red');
                            $('.name_error').removeClass('hidden')
                        }
                    }
                })
            } else {
                if (title_uz == '') {
                    $('#title_uz').addClass('border-red');
                    $('.name_error').removeClass('hidden')
                }
                if (title_ru == '') {
                    $('#title_ru').addClass('border-red');
                    $('.name_error_ru').removeClass('hidden')
                }
            }
        })

        // delete sub_category
        $(document).on('click', '.delete_sub_category', function(){
            if(confirm('Ushbu mahsulotni o`chirishni tasdiqlaysizmi') == true){
                var pr_id = $(this).attr('data-pr_id');
                var id = $(this).attr('data-delete')
            }
            $.ajax({
                url: 'delete-sub-category',
                data: {
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        });

        // create details
        $(document).on("click",".add_multi_details",function(){
            var id = $(this).attr('data-id')
            $.ajax({
                url: 'add-multi-detail',
                data: {
                    s: false,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $('.modal-body').html(response.content)
                        $('.modal-header').html("<h3>Qo'shimcha malumot turi qo'shish</h3>")
                        $('.modal-footer .btn-success').addClass('save_multi_detail')
                        $('.modal-footer .btn-success').attr('data-id',id)
                    }
                }
            });
        });

        $(document).on('click', '.save_multi_detail', function(){
            var id = $(this).attr('data-id')
            var title_uz = $('#title_uz').val()
            var title_ru = $('#title_ru').val()

            if (title_uz != '' && title_ru != ''){
                $.ajax({
                    url: 'add-multi-detail',
                    data: {
                        s: true,
                        id:id,
                        title_uz: title_uz,
                        title_ru: title_ru,
                    },
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        if (response.status == 'success') {
                            location.reload()
                        } else if (response.status == 'error_name') {
                            $('#title_uz').addClass('border-red');
                            $('.name_error').removeClass('hidden')
                        }
                    }
                })
            } else {
                if (title_uz == '') {
                    $('#title_uz').addClass('border-red');
                    $('.name_error').removeClass('hidden')
                }
                if (title_ru == '') {
                    $('#title_ru').addClass('border-red');
                    $('.name_error_ru').removeClass('hidden')
                }
            }
        })
        // delete details
        
        $(document).on('click', '.delete_multi_detail', function(){
            if(confirm('Ushbu mahsulotni o`chirishni tasdiqlaysizmi') == true){
                var pr_id = $(this).attr('data-pr_id');
                var id = $(this).attr('data-delete')
            }
            $.ajax({
                url: 'delete-multi-detail',
                data: {
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        });
        // update details

        $(document).on('click', '.update_multi_detail', function(){
            var id = $(this).attr('data-update')
            $.ajax({
                url: 'update-multi-detail',
                data: {
                    s: false,
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        $('.modal-body').html(response.content)
                        $('.modal-header').html("<h3>Saqlash</h3>")
                        $('.modal-footer .btn-success').addClass('change_mutli_detail')
                        $('.modal-footer .btn-success').attr('data-id',id)
                    }
                }
            })
        })

        $(document).on('click', '.change_mutli_detail', function(){
            var id = $(this).attr('data-id')
            var title_uz = $('#title_uz').val()
            var title_ru = $('#title_ru').val()

            if (title_uz != '' && title_ru != ''){
                $.ajax({
                    url: 'update-multi-detail',
                    data: {
                        s: true,
                        id:id,
                        title_uz: title_uz,
                        title_ru: title_ru,
                    },
                    dataType: 'json',
                    type: 'get',
                    success: function(response) {
                        if (response.status == 'success') {
                            location.reload()
                        } else if (response.status == 'error_name') {
                            $('#title_uz').addClass('border-red');
                            $('.name_error').removeClass('hidden')
                        }
                    }
                })
            } else {
                if (title_uz == '') {
                    $('#title_uz').addClass('border-red');
                    $('.name_error').removeClass('hidden')
                }
                if (title_ru == '') {
                    $('#title_ru').addClass('border-red');
                    $('.name_error_ru').removeClass('hidden')
                }
            }
        })
        $(document).on('click', '.delete_img', function(){
             if(confirm('Ushbu Rasmni o`chirishni tasdiqlaysizmi') == true){
                var deleted_id = $(this).val()
            }
            $.ajax({
                url: 'delete-img',
                data: {
                    id:deleted_id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        })
        $(document).on('click', '.input', function(){
            var id = $(this).attr('data-id');
            $.ajax({
                url: '/site/checkbox',
                data: {
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        })
        
        // $('.switch_trend').on('switchChange', function (e, data) {
        //     alert()
        // }); 

        $('.switch_trend').on('change.bootstrapSwitch', function(e) {
           var id = $(this).attr('data-id');
           $.ajax({
                url: 'trend',
                data: {
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        });
        $('.switch_new').on('change.bootstrapSwitch', function(e) {
           var id = $(this).attr('data-id');
           $.ajax({
                url: 'new-products',
                data: {
                    id:id,
                },
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'success') {
                        location.reload()
                    }
                }
            })
        });
        $('#myonoffswitch').on('change.bootstrapSwitch', function(e) {
            $.ajax({
                url: 'client-cashbacks/cashback-control',
                dataType: 'json',
                type: 'get',
                success: function(response) {
                    if (response.status == 'active' || response.status == "passive") {
                        location.reload();
                    }
                }
            })
        });
       


    </script>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
