<aside class="main-sidebar">

    <section class="sidebar">

        

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Buyurtmalar', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'order-code') ? 'active' : ''), 'url' => ['order-code/index']],
                    ['label' => 'Kategoriyalar', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'category') ? 'active' : ''), 'url' => ['category/index']],
                    ['label' => 'Mahsulotlar', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'product') ? 'active' : ''), 'url' => ['product/index']],
                    ['label' => 'Mijozlar', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'clients') ? 'active' : ''), 'url' => ['clients/index']],
                    ['label' => 'Biz Haqimizda', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'about-us') ? 'active' : ''), 'url' => ['about-us/index']],
                    ['label' => 'Yangilikar', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'news') ? 'active' : ''), 'url' => ['news/index']],
                    ['label' => 'cashback', 'icon' => 'fa fa-th-large', 'class' => ((Yii::$app->controller->id == 'cashbacks') ? 'active' : ''), 'url' => ['cashbacks/index']],
                    ['label' => 'Sklad', 'icon' => 'fa fa-th-large', 'url' => ['storage/index']],
                ],
            ]
        ) ?>

    </section>

</aside>