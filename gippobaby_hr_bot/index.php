<?php

    require 'vendor/autoload.php';

    use Dompdf\Dompdf;

    $servername = "localhost";
    $username = "gippobab_hr_user";
    $password = "gippobab_123";
    $dbname = "gippobab_hr";
    $conn = mysqli_connect($servername,$username,$password,$dbname);
    if(!$conn){
        die("Connection failed:".mysqli_connect_error());
    } else {
        echo "SuccessFull";
    }

    const TOKEN = '1859303960:AAE3LpHtu1X1Fd6VVj1DZ1TH3vESvobQuJY';
    const BASE_URL = 'https://api.telegram.org/bot'.TOKEN;

    function bot($method, $data = []){
        $url = BASE_URL.'/'.$method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($ch);

        if(curl_error($ch))
        {
            var_dump(curl_error($ch));
        }
        else{
            return json_decode($res);
        }
    }

    $update = file_get_contents('php://input');
    $update = json_decode($update);
    $message = $update->message;
    $text = $message->text;
    $chat_id = $message->chat->id;
    $message_id = $message->message_id;
    $first_name = $message->chat->first_name;
    $user_name = $message->chat->username;
    $last_name = $message->chat->last_name;
    if (isset($update->callback_query)){
        $data = $update->callback_query->data;
        $chat_id = $update->callback_query->message->chat->id;
        $message_id = $update->callback_query->message->message_id;
    }

    function deleteMessageUser($conn, $chat_id){
        $selectMessageId = "SELECT * FROM message_id WHERE chat_id = ".$chat_id;
        $resultMessageId = $conn->query($selectMessageId);
        if ($resultMessageId->num_rows > 0){
            while($row = $resultMessageId->fetch_assoc()){
                $user_message_id = $row['message_id'];

                bot('deleteMessage',[
                    'chat_id' => $chat_id,
                    'message_id' => $user_message_id,
                ]);
            }
            $deleteMessageId = "DELETE FROM message_id WHERE chat_id = ".$chat_id;
            $resultMessageId = $conn->query($deleteMessageId);
        }
    }

    function insertMessageId($response,$conn, $chat_id){
        $message_id = $response->result->message_id;
        $insertMessageId = "INSERT INTO message_id (chat_id,message_id) VALUES (".$chat_id.",".$message_id.")";
        $resultMessageId = $conn->query($insertMessageId);
    }

    function cancelAllAnswer($step_1, $conn, $chat_id, $message_id){
        $filing = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => "📑 Hujjat topshirish",'callback_data' => 'filing'],
                ],
                [
                    ['text' => "🔙 Ortga",'callback_data' => 'back'],
                ]
            ]
        ]);

        $filing_ru = json_encode([
            'inline_keyboard' => [
                [
                    ['text' => "📑 Подача заявки",'callback_data' => 'filing'],
                ],
                [
                    ['text' => "🔙 Назад",'callback_data' => 'back'],
                ]
            ]
        ]);

        $stepUpdate = "UPDATE step SET step_2 = 1 WHERE chat_id = ".$chat_id;
        $resultStep = $conn->query($stepUpdate);

        $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id;
        $resultDelete = $conn->query($deleteAnswer);

        if ($step_1 == 1){
            $text_lang = "Asosiy bo'limga xush keldingiz 😊";
            $button_lang = $filing;
        } else {
            $text_lang = "Добро пожаловать в основной раздел 😊";
            $button_lang = $filing_ru;
        }

        bot('editMessageText',[
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $text_lang,
            'parse_mode' => 'markdown',
            'reply_markup' => $button_lang
        ]);
    }

    $language = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "🇺🇿 O'zbekcha",'callback_data' => 'uz'],
                ['text' => "🇷🇺 Русский",'callback_data' => 'ru'],
            ],
        ]
    ]);

    $filing = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "📑 Hujjat topshirish",'callback_data' => 'filing'],
            ],
            [
                ['text' => "🔙 Ortga",'callback_data' => 'back'],
            ]
        ]
    ]);

    $filing_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "📑 Подача заявки",'callback_data' => 'filing'],
            ],
            [
                ['text' => "🔙 Назад",'callback_data' => 'back'],
            ]
        ]
    ]);

    $position = json_encode([
        'inline_keyboard' => [
            [
                ['text'=>"🧑‍💻 Sotuv menejeri", 'callback_data' => 'position'],
            ],
            [
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ]
        ]
    ]);

    $position_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=>"🧑‍💻 Менеджер по продажам", 'callback_data' => 'position'],
            ],
            [
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $section = json_encode([
        'inline_keyboard' => [
            [
                ['text'=>"#️⃣ Sotuv bo'limi", 'callback_data' => "section_Sotuv bo'limi"],
            ],
            [
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ]
        ]
    ]);

    $section_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=>"#️⃣ Отдел продаж", 'callback_data' => 'section_Отдел продаж'],
            ],
            [
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $back = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ],
        ]
    ]);

    $back_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ],
        ]
    ]);

    $married = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "👨‍👩‍👦 Turmush qurganman",'callback_data' => 'married_Turmush qurganman']
            ],
            [
                ['text'=> "🙅 Turmush qurmaganman", 'callback_data' => "married_Turmush qurmaganman"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ]
        ]
    ]);

    $married_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "👨‍👩‍👦 Я женат",'callback_data' => 'married_Я женат']
            ],
            [
                ['text'=> "🙅 Я не женат", 'callback_data' => "married_Я не женат"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ],
        ]
    ]);

    $study = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "O'rta",'callback_data' => "study_O'rta"]
            ],
            [
                ['text'=> "O'rta maxsus", 'callback_data' => "study_O'rta maxsus"]
            ],
            [
                ['text'=> "Oliy/Bakalavr", 'callback_data' => "study_Oliy/Bakalavr"]
            ],
            [
                ['text'=> "Oliy/Magistr", 'callback_data' => "study_Oliy/Magistr"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ],
        ]
    ]);

    $study_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "Середина",'callback_data' => "study_Середина"]
            ],
            [
                ['text'=> "Средний специальный", 'callback_data' => "study_Средний специальный"]
            ],
            [
                ['text'=> "Высшее/Бакалавр", 'callback_data' => "study_Высшее/Бакалавр"]
            ],
            [
                ['text'=> "Высшее/Магистр", 'callback_data' => "study_Высшее/Магистр"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $certificate = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "B",'callback_data' => "certificate_B"]
            ],
            [
                ['text'=> "B, C", 'callback_data' => "certificate_B, C"]
            ],
            [
                ['text'=> "Yo'q", 'callback_data' => "certificate_Yo'q"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ]
        ]
    ]);

    $certificate_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "B",'callback_data' => "certificate_B"]
            ],
            [
                ['text'=> "B, C", 'callback_data' => "certificate_B, C"]
            ],
            [
                ['text'=> "Нет", 'callback_data' => "certificate_Нет"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $internet = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "Telegram",'callback_data' => "internet_Telegram"]
            ],
            [
                ['text'=> "Instagram", 'callback_data' => "internet_Instagram"]
            ],
            [
                ['text'=> "Facebook", 'callback_data' => "internet_Facebook"]
            ],
            [
                ['text'=> "TikTok", 'callback_data' => "internet_TikTok"]
            ],
            [
                ['text'=> "Tanishimdan", 'callback_data' => "internet_Tanishimdan"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ],
        ]
    ]);

    $internet_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text' => "Telegram",'callback_data' => "internet_Telegram"]
            ],
            [
                ['text'=> "Instagram", 'callback_data' => "internet_Instagram"]
            ],
            [
                ['text'=> "Facebook", 'callback_data' => "internet_Facebook"]
            ],
            [
                ['text'=> "TikTok", 'callback_data' => "internet_TikTok"]
            ],
            [
                ['text'=> "От знакомого", 'callback_data' => "internet_От знакомого"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ],
        ]
    ]);

    $next_question = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Keyingi savolga o'tish", 'callback_data' => "next"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ],
        ]
    ]);

    $next_question_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Перейти к следующему вопросу", 'callback_data' => "next"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $confirm = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Bekor qilish ❌", 'callback_data' => "delete"],
                ['text' => "Tasdiqlash ✅",'callback_data' => 'done']
            ]
        ]
    ]);

    $confirm_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Отмена ❌", 'callback_data' => "delete"],
                ['text' => "Подтверждение ✅",'callback_data' => 'done']
            ]
        ]
    ]);

    $yesOrNo = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Ha", 'callback_data' => "yesOrNo_Ha_1"]
            ],
            [
                ['text'=> "Yo'q", 'callback_data' => "yesOrNo_Yo'q_2"]
            ],
            [
                ['text'=> "❌ Bekor qilish", 'callback_data' => "remove"],
                ['text' => "🔙 Ortga",'callback_data' => 'back']
            ]
        ]
    ]);

    $yesOrNo_ru = json_encode([
        'inline_keyboard' => [
            [
                ['text'=> "Да", 'callback_data' => "yesOrNo_Да_1"]
            ],
            [
                ['text'=> "Нет", 'callback_data' => "yesOrNo_Нет_2"]
            ],
            [
                ['text'=> "❌ Отмена", 'callback_data' => "remove"],
                ['text' => "🔙 Назад",'callback_data' => 'back']
            ]
        ]
    ]);

    $back_text_uz = "🔙 Ortga";
    $back_text_ru = "🔙 Назад";

    if ($text == "/start") {
        $checkUser = "SELECT * FROM users WHERE chat_id = ".$chat_id;
        $resultCheck = $conn->query($checkUser);
        if (!$resultCheck->num_rows > 0) {
            $addUser = "INSERT INTO users (chat_id,username) VALUES (".$chat_id.",'".$user_name."')";
            $resultAddUser = $conn->query($addUser);
            $addLastId = "INSERT INTO step (chat_id,step_1, step_2) VALUES (".$chat_id.",0,0)";
            $resultLastId = $conn->query($addLastId);
            $addLastId = "INSERT INTO last_id (chat_id,last_id) VALUES (".$chat_id.",0)";
            $resultLastId = $conn->query($addLastId);
        } else {
            $stepUpdate = "UPDATE step SET step_1 = 0, step_2 = 0 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);
            $updateLastId = "UPDATE last_id SET last_id = 0 WHERE chat_id = ".$chat_id;
            $resultLastId = $conn->query($updateLastId);
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id;
            $resultDelete = $conn->query($deleteAnswer);
            deleteMessageUser($conn, $chat_id);
        }

        bot('sendMessage',[
            'chat_id' => $chat_id,
            'text' => "*🇺🇿 Assalomu alaykum, tilni tanlang
🇷🇺 Здравствуйте, выберите язык*",
            'parse_mode' => 'markdown',
            'reply_markup' => $language
        ]);
    }

    $selectStep = "SELECT * FROM step WHERE chat_id = ".$chat_id;
    $resultStep = $conn->query($selectStep);
    if ($resultStep->num_rows > 0) {
        $row = $resultStep->fetch_assoc();
        $step_1 = $row["step_1"];
        $step_2 = $row["step_2"];
    }

    $lastId = "SELECT * FROM last_id WHERE chat_id = ".$chat_id;
    $resultLastId = $conn->query($lastId);
    if ($resultLastId->num_rows > 0) {
        $row = $resultLastId->fetch_assoc();
        $last_id = $row["last_id"];
    }

    if ($step_1 == 0 and $step_2 == 0 and $text != "/start"){
        if ($data == "uz"){
            $stepUpdate = "UPDATE step SET step_1 = 1, step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $text_lang = "Asosiy bo'limga xush keldingiz 😊";
            $button_lang = $filing;

        }
        else if ($data == "ru"){
            $stepUpdate = "UPDATE step SET step_1 = 2, step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $text_lang = "Добро пожаловать в основной раздел 😊";
            $button_lang = $filing_ru;
        } else {
            bot('deleteMessage', [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }

        bot('editMessageText',[
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $text_lang,
            'parse_mode' => 'markdown',
            'reply_markup' => $button_lang
        ]);
    }

    if ($step_2 == 1 and $text != "/start"){
        if ($data == "filing"){
            $stepUpdate = "UPDATE step SET step_2 = 2 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Lavozimlardan birini tanlang 👇";
                $button_lang = $position;
            } else {
                $text_lang = "Выберите одну из должностей 👇";
                $button_lang = $position_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }

        else if ($data == "back"){
            $stepUpdate = "UPDATE step SET step_1 = 0, step_2 = 0 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => "🇺🇿 Assalomu alaykum, tilni tanlang
🇷🇺 Здравствуйте, выбрать язык",
                'parse_mode' => 'markdown',
                'reply_markup' => $language
            ]);
        }

        else {
            bot('deleteMessage', [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 2 and $text != "/start"){
        if ($data == "position"){
            $stepUpdate = "UPDATE step SET step_2 = 3 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Qaysi bo'lim uchun hujjat topshirmoqchisiz?";
                $button_lang = $section;
            } else {
                $text_lang = "В какой отдел вы хотите устроиться на работу?";
                $button_lang = $section_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $stepUpdate = "UPDATE step SET step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Asosiy bo'limga xush keldingiz 😊";
                $button_lang = $filing;
            } else {
                $text_lang = "Добро пожаловать в основной раздел 😊";
                $button_lang = $filing_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        } else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 3 and $text != "/start"){
        $explode_section = explode("_", $data);
        if ($explode_section[0] == "section"){
            $stepUpdate = "UPDATE step SET step_2 = 4 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);
            $txt = base64_encode($explode_section[1]);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",1,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            if ($step_1 == 1){
                $text_lang = "Familiya, ism-sharifingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу фамилию и имя 👇";
                $button_lang = $back_ru;
            }

            $response = bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $stepUpdate = "UPDATE step SET step_2 = 2 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Lavozimlardan birini tanlang 👇";
                $button_lang = $position;
            } else {
                $text_lang = "Выберите одну из должностей 👇";
                $button_lang = $position_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        } else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 4 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",2,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 5 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Tug'ilgan sanangizni kiriting 👇".PHP_EOL."_Namuna: 17.04.2002_";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу дату рождения 👇".PHP_EOL."_Образец: 17.04.2002_";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 3 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Qaysi bo'lim uchun hujjat topshirmoqchisiz?";
                $button_lang = $section;
            } else {
                $text_lang = "В какой отдел вы хотите устроиться на работу?";
                $button_lang = $section_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 5 and $text != "/start"){
        if (isset($text)){
            $insertMessageId = "INSERT INTO message_id (chat_id,message_id) VALUES (".$chat_id.",".$message_id.")";
            $resultMessageId = $conn->query($insertMessageId);
            $check_data = explode('.',$text);
            if (checkdate($check_data[1],$check_data[0],$check_data[2]) && preg_match('~^\d{2}\.\d{2}\.\d{4}$~',$text)){
                deleteMessageUser($conn, $chat_id);

                $txt = base64_encode($text);
                $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",3,'".$txt."')";
                $resultAnswer = $conn->query($insertAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 6 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Doimiy yashash joyingizni kiriting 👇";
                    $button_lang = $back;
                } else {
                    $text_lang = "Укажите свое постоянное место жительства 👇";
                    $button_lang = $back_ru;
                }

                bot('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => $text_lang,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $button_lang
                ]);
            } else {
                if ($step_1 == 1){
                    $error_text = "*Tug'ilgan yilinigizni to'g'ri kiriting ❗️*".PHP_EOL."_Namuna: 17.04.2002_";
                    $error_button = $back;
                } else {
                    $error_text = "*Введите свой год рождения правильно ❗️*".PHP_EOL."_Образец: 17.04.2002_";
                    $error_button = $back_ru;
                }

                $response = bot('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => $error_text,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $error_button
                ]);
                insertMessageId($response, $conn, $chat_id);
            }
        }
        else if ($data == "back"){
            deleteMessageUser($conn, $chat_id);

            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 4 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Familiya, ism-sharifingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу фамилию и имя 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "remove"){
            deleteMessageUser($conn, $chat_id);
            $stepUpdate = "UPDATE step SET step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id;
            $resultDelete = $conn->query($deleteAnswer);

            if ($step_1 == 1){
                $text_lang = "Asosiy bo'limga xush keldingiz 😊";
                $button_lang = $filing;
            } else {
                $text_lang = "Добро пожаловать в основной раздел 😊";
                $button_lang = $filing_ru;
            }

            bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
    }

    if ($step_2 == 6 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",4,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 7 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Telefon raqamingizni kiriting 👇".PHP_EOL."_Namuna: +998001234567_";
                $button_lang = $back;
            } else {
                $text_lang = "Введите свой номер телефона 👇".PHP_EOL."_Образец: +998001234567_";
                $button_lang = $back_ru;
            }
            $message_id = $message_id - 1;
            $response = bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 5 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Tug'ilgan sanangizni kiriting 👇".PHP_EOL."_Namuna: 17.04.2002_";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу дату рождения 👇".PHP_EOL."_Образец: 17.04.2002_";
                $button_lang = $back_ru;
            }

            $response = bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 7 and $text != "/start"){
        if (isset($text)){
            $insertMessageId = "INSERT INTO message_id (chat_id,message_id) VALUES (".$chat_id.",".$message_id.")";
            $resultMessageId = $conn->query($insertMessageId);
            if (preg_match('~^\+998\d{9}$~', $text)){
                deleteMessageUser($conn, $chat_id);

                $txt = base64_encode($text);
                $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",5,'".$txt."')";
                $resultAnswer = $conn->query($insertAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 8 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                $updatePhoneNumber = "UPDATE users SET phone_number = '".$txt."' WHERE chat_id = ".$chat_id;
                $resultUpdatePhoneNumber = $conn->query($updatePhoneNumber);

                if ($step_1 == 1){
                    $text_lang = "Oilaviy ahvolingizni tanlang 👇";
                    $button_lang = $married;
                } else {
                    $text_lang = "Выберите свое семейное положение 👇";
                    $button_lang = $married_ru;
                }
                $response = bot('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => $text_lang,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $button_lang
                ]);
                insertMessageId($response, $conn, $chat_id);
            } else {
                if ($step_1 == 1){
                    $error_text = "*Telefon raqamingizni to'g'ri kiriting ❗️*".PHP_EOL."_Namuna: +998001234567_";
                    $error_button = $back;
                } else {
                    $error_text = "*Введите свой номер телефона правильно ❗️*".PHP_EOL."_Образец: +998001234567_";
                    $error_button = $back_ru;
                }

                $response = bot('sendMessage', [
                    'chat_id' => $chat_id,
                    'text' => $error_text,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $error_button
                ]);
                insertMessageId($response, $conn, $chat_id);
            }
        }
        else if ($data == "back"){
            deleteMessageUser($conn, $chat_id);
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 6 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Doimiy yashash joyingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Укажите свое постоянное место жительства 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "remove"){
            deleteMessageUser($conn, $chat_id);
            $stepUpdate = "UPDATE step SET step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id;
            $resultDelete = $conn->query($deleteAnswer);

            if ($step_1 == 1){
                $text_lang = "Asosiy bo'limga xush keldingiz 😊";
                $button_lang = $filing;
            } else {
                $text_lang = "Добро пожаловать в основной раздел 😊";
                $button_lang = $filing_ru;
            }

            bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
    }

    if ($step_2 == 8 and $text != "/start"){
        $explode_married = explode("_", $data);
        if ($explode_married[0] == "married"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",6,'".base64_encode($explode_married[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 9 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ta'lim darajangizni tanlang 👇";
                $button_lang = $study;
            } else {
                $text_lang = "Выберите свой уровень образования 👇";
                $button_lang = $study_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 7 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Telefon raqamingizni kiriting 👇".PHP_EOL."_Namuna: +998001234567_";
                $button_lang = $back;
            } else {
                $text_lang = "Введите свой номер телефона 👇".PHP_EOL."_Образец: +998001234567_";
                $button_lang = $back_ru;
            }

            $response = bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 9 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "study"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",7,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 10 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan o'quv yurtingizni nomini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите название учебного заведения, в котором вы учились 👇";
                $button_lang = $back_ru;
            }
            $response = bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 8 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilaviy ahvolingizni tanlang 👇";
                $button_lang = $married;
            } else {
                $text_lang = "Выберите свое семейное положение 👇";
                $button_lang = $married_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 10 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",8,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 11 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan yillaringizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите количество годов, в которые вы учились 👇";
                $button_lang = $back_ru;
            }
            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 9 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ta'lim darajangizni tanlang 👇";
                $button_lang = $study;
            } else {
                $text_lang = "Выберите свой уровень образования 👇";
                $button_lang = $study_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 11 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",9,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 12 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan yo'nalishingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите сферу обучения 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 10 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan o'quv yurtingizni nomini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите название учебного заведения, в котором вы учились 👇";
                $button_lang = $back_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 12 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",10,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 13 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Bundan avval boshqa bir korxona yoki tashkilotda ishlaganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Работали ли вы раньше в другой компании или организации? 👇";
                $button_lang = $yesOrNo_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 11 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan yillaringizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите количество годов, в которые вы учились 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 13 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            if ($explode_data[2] == 1){
                $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",11,'".base64_encode($explode_data[1])."')";
                $resultAnswer = $conn->query($insertAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 14 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                $lastIdUpdate = "UPDATE last_id SET last_id = 1 WHERE chat_id = ".$chat_id;
                $resultLastId = $conn->query($lastIdUpdate);

                if ($step_1 == 1){
                    $text_lang = "Ishlagan korxona yoki tashkilot nomini kiriting 👇";
                    $button_lang = $back;
                } else {
                    $text_lang = "Введите название компании или организации, в которой вы работали 👇";
                    $button_lang = $back_ru;
                }
                bot('editMessageText', [
                    'chat_id' => $chat_id,
                    'message_id' => $message_id,
                    'text' => $text_lang,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $button_lang
                ]);
            }
            else if ($explode_data[2] == 2){
                $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",11,'".base64_encode($explode_data[1])."')";
                $resultAnswer = $conn->query($insertAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 19 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                $lastIdUpdate = "UPDATE last_id SET last_id = 2 WHERE chat_id = ".$chat_id;
                $resultLastId = $conn->query($lastIdUpdate);

                if ($step_1 == 1){
                    $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Otam F.I.SH) 👇";
                    $button_lang = $next_question;
                } else {
                    $text_lang = "Введите информацию о членах вашей семьи. (Папа Ф.И.О) 👇";
                    $button_lang = $next_question_ru;
                }
                bot('editMessageText', [
                    'chat_id' => $chat_id,
                    'message_id' => $message_id,
                    'text' => $text_lang,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $button_lang
                ]);
            }
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 12 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'qigan yo'nalishingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите сферу обучения 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 14 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",12,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 15 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ishlagan yillaringizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите свои годы службы в компании 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 13 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Bundan avval boshqa bir korxona yoki tashkilotda ishlaganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Работали ли вы раньше в другой компании или организации? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 15 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",13,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 16 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Faoliyat yuritgan lavozimingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу предыдущую должность в компании 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 14 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ishlagan korxona yoki tashkilot nomini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите название компании или организации, в которой вы работали 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 16 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",14,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 17 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ishdan ketganingizni sababini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Укажите причину вашего увольнения 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 15 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ishlagan yillaringizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите свои годы службы в компании 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 17 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",15,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 18 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Agar yana qayerdadur ishlagan bo'lsangiz, kiritishingiz yoki keyingi so'rovlarga o'tishingiz mumkin 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Если вы ещё работали в другом месте, вы можете ввести или перейти к следующему вопросу 👇";
                $button_lang = $next_question_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 16 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Faoliyat yuritgan lavozimingizni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите вашу предыдущую должность в компании 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 18 and $text != "/start"){
        if (isset($text) or $data == "next"){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            if (isset($text)){
                $txt = base64_encode($text);
                $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",16,'".$txt."')";
                $resultAnswer = $conn->query($insertAnswer);
            }

            $stepUpdate = "UPDATE step SET step_2 = 19 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Otam F.I.SH) 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Введите информацию о членах вашей семьи. (Папа Ф.И.О) 👇";
                $button_lang = $next_question_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 17 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ishdan ketganingizni sababini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Укажите причину вашего увольнения 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 19 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",17,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 20 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Otangizni tug'ilgan sanalarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите дату рождения вашего отца 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "next"){
            $stepUpdate = "UPDATE step SET step_2 = 22 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Onam F.I.SH) 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Введите информацию о членах вашей семьи. (Мама Ф.И.О) 👇";
                $button_lang = $next_question_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $selectQuestionId = "select * from answers where question_id = 16";
            $resultQuestionId = $conn->query($selectQuestionId);
            if ($resultQuestionId->num_rows > 0){
                $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
                $resultDelete = $conn->query($deleteAnswer);
            }

            if ($last_id == 1){
                $stepUpdate = "UPDATE step SET step_2 = 18 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Agar yana qayerdadur ishlagan bo'lsangiz, kiritishingiz yoki keyingi so'rovlarga o'tishingiz mumkin 👇";
                    $button_lang = $next_question;
                } else {
                    $text_lang = "Если вы ещё работали в другом месте, вы можете ввести или перейти к следующему вопросу 👇";
                    $button_lang = $next_question_ru;
                }
            } else {
                $stepUpdate = "UPDATE step SET step_2 = 13 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Bundan avval boshqa bir korxona yoki tashkilotda ishlaganmisiz? 👇";
                    $button_lang = $yesOrNo;
                } else {
                    $text_lang = "Работали ли вы раньше в другой компании или организации? 👇";
                    $button_lang = $yesOrNo_ru;
                }
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 20 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",18,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 21 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Otangizni ishlash joylarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите рабочее место вашего отца 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 19 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Otam F.I.SH) 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Введите информацию о членах вашей семьи. (Папа Ф.И.О) 👇";
                $button_lang = $next_question_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 21 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",19,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 22 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Onam F.I.SH) 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Введите информацию о членах вашей семьи. (Мама Ф.И.О) 👇";
                $button_lang = $next_question_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 20 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Otangizni tug'ilgan sanalarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите дату рождения вашего отца 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 22 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",20,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 23 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Onangizni tug'ilgan sanalarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите дату рождения вашей матери 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "next"){
            $stepUpdate = "UPDATE step SET step_2 = 25 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolari haqida ma'lumot bering. 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Сообщите информацию о других членах вашей семьи. 👇";
                $button_lang = $next_question_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $selectQuestionId = "select * from answers where question_id = 17";
            $resultQuestionId = $conn->query($selectQuestionId);
            if ($resultQuestionId->num_rows > 0){
                $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
                $resultDelete = $conn->query($deleteAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 21 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Otangizni ishlash joylarini kiriting 👇";
                    $button_lang = $back;
                } else {
                    $text_lang = "Введите рабочее место вашего отца 👇";
                    $button_lang = $back_ru;
                }
            } else {
                $stepUpdate = "UPDATE step SET step_2 = 19 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Otam F.I.SH) 👇";
                    $button_lang = $next_question;
                } else {
                    $text_lang = "Введите информацию о членах вашей семьи. (Папа Ф.И.О) 👇";
                    $button_lang = $next_question_ru;
                }
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 23 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",21,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 24 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Onangizni ishlash joylarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите рабочее место вашей матери 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 22 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Onam F.I.SH) 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Введите информацию о членах вашей семьи. (Мама Ф.И.О) 👇";
                $button_lang = $next_question_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 24 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",22,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 25 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolari haqida ma'lumot bering. 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Сообщите информацию о других членах вашей семьи. 👇";
                $button_lang = $next_question_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 23 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Onangizni tug'ilgan sanalarini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите дату рождения вашей матери 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 25 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",23,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 26 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolarini tug'ilgan sanalarini kiriting. 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите даты рождения других членов вашей семьи. 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "next"){
            $stepUpdate = "UPDATE step SET step_2 = 28 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Harbiy xizmatga borganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Вы служили в армии? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $selectQuestionId = "select * from answers where question_id = 20";
            $resultQuestionId = $conn->query($selectQuestionId);
            if ($resultQuestionId->num_rows > 0){
                $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
                $resultDelete = $conn->query($deleteAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 24 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Onangizni ishlash joylarini kiriting 👇";
                    $button_lang = $back;
                } else {
                    $text_lang = "Введите рабочее место вашей матери 👇";
                    $button_lang = $back_ru;
                }
            } else {
                $stepUpdate = "UPDATE step SET step_2 = 22 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Onam F.I.SH) 👇";
                    $button_lang = $next_question;
                } else {
                    $text_lang = "Введите информацию о членах вашей семьи. (Мама Ф.И.О) 👇";
                    $button_lang = $next_question_ru;
                }
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 26 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",24,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 27 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolarini ishlash joylarini kiriting. 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите данные о месте работы других членов вашей семьи. 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 25 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolari haqida ma'lumot bering. 👇";
                $button_lang = $next_question;
            } else {
                $text_lang = "Сообщите информацию о других членах вашей семьи. 👇";
                $button_lang = $next_question_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }


    if ($step_2 == 27 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",25,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 28 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Harbiy xizmatga borganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Вы служили в армии? 👇";
                $button_lang = $yesOrNo_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 26 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolarini tug'ilgan sanalarini kiriting. 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите даты рождения других членов вашей семьи. 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 28 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",26,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 29 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Umringiz davomida sudlanganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Были ли вы судимы раньше? 👇";
                $button_lang = $yesOrNo_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $selectQuestionId = "select * from answers where question_id = 23";
            $resultQuestionId = $conn->query($selectQuestionId);
            if ($resultQuestionId->num_rows > 0){
                $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
                $resultDelete = $conn->query($deleteAnswer);

                $stepUpdate = "UPDATE step SET step_2 = 27 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Onangizni ishlash joylarini kiriting 👇";
                    $button_lang = $back;
                } else {
                    $text_lang = "Введите рабочее место вашей матери 👇";
                    $button_lang = $back_ru;
                }
            } else {
                $stepUpdate = "UPDATE step SET step_2 = 22 WHERE chat_id = ".$chat_id;
                $resultStep = $conn->query($stepUpdate);

                if ($step_1 == 1){
                    $text_lang = "Oila a'zolaringiz haqida ma'lumot bering. (Onam F.I.SH) 👇";
                    $button_lang = $next_question;
                } else {
                    $text_lang = "Введите информацию о членах вашей семьи. (Мама Ф.И.О) 👇";
                    $button_lang = $next_question_ru;
                }
            }

            if ($step_1 == 1){
                $text_lang = "Oilangizning boshqa a'zolarini ishlash joylarini kiriting. 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Введите данные о месте работы других членов вашей семьи. 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 29 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",27,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 30 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Avtomobil haydash guvohnomangiz (prava) bormi? 👇";
                $button_lang = $certificate;
            } else {
                $text_lang = "У вас есть водительские права? 👇";
                $button_lang = $certificate_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 28 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Harbiy xizmatga borganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Вы служили в армии? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 30 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "certificate"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",28,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 31 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Agar mashinangiz bo'lsa, rusumini kiriting 👇";
                $button_lang = $back;
            } else {
                    $text_lang = "Если у вас есть машина, укажите модель 👇";
                $button_lang = $back_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 29 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Umringiz davomida sudlanganmisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Были ли вы судимы раньше? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 31 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",29,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 32 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Bizning korxona haqida qayerdan ma'lumot oldingiz? 👇";
                $button_lang = $internet;
            } else {
                $text_lang = "Откуда вы узнали о нашей компании? 👇";
                $button_lang = $internet_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 30 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Avtomobil haydash guvohnomangiz (prava) bormi? 👇";
                $button_lang = $certificate;
            } else {
                $text_lang = "У вас есть водительские права? 👇";
                $button_lang = $certificate_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 32 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "internet"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",30,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 33 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Korxonada ishdan keyin qolib ishlashga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Согласны ли вы на сверхурочную работу? 👇";
                $button_lang = $yesOrNo_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 31 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Agar mashinangiz bo'lsa, rusumini kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Если у вас есть машина, укажите модель 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 33 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",31,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 34 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Korxona majlislariga qatnashishga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Согласны ли вы на деловые встречи? 👇";
                $button_lang = $yesOrNo_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 32 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Bizning korxona haqida qayerdan ma'lumot oldingiz? 👇";
                $button_lang = $internet;
            } else {
                $text_lang = "Откуда вы узнали о нашей компании? 👇";
                $button_lang = $internet;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 34 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",32,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 35 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Kollektiv ishlay olasizmi? Kollektiv ish deganda nimani tushunasiz? 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Можете ли вы работать в команде? Что вы подразумеваете под командной работой? 👇";
                $button_lang = $back_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 33 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Korxonada ishdan keyin qolib ishlashga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Согласны ли вы на сверхурочную работу? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 35 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",33,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 36 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ota-onangizni korxonamizga chaqirishimizga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Вы согласны с тем, что мы должны пригласить ваших родителей в нашу компанию? 👇";
                $button_lang = $yesOrNo_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 34 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Korxona majlislariga qatnashishga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Согласны ли вы на деловые встречи? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 36 and $text != "/start"){
        $explode_data = explode("_", $data);
        if ($explode_data[0] == "yesOrNo"){
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",34,'".base64_encode($explode_data[1])."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 37 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Sog'lig'ingizda muammo yo'qmi? Agar bo'lsa uni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "У вас проблемы со здоровьем? Введите, если есть 👇";
                $button_lang = $back_ru;
            }
            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 35 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Kollektiv ishlay olasizmi? Kollektiv ish deganda nimani tushunasiz? 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Можете ли вы работать в команде? Что вы подразумеваете под командной работой? 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 37 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",35,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 38 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'zingiz haqingizda nimalar qo'shmoqchisiz? Ijobiy taraflaringiz 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Что вы хотите добавить о себе? Ваши положительные стороны 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 36 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Ota-onangizni korxonamizga chaqirishimizga rozimisiz? 👇";
                $button_lang = $yesOrNo;
            } else {
                $text_lang = "Вы согласны с тем, что мы должны пригласить ваших родителей в нашу компанию? 👇";
                $button_lang = $yesOrNo_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 38 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",36,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 39 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'zingiz haqingizda nimalar qo'shmoqchisiz? Salbiy taraflaringiz 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Что вы хотите добавить о себе? Ваши недостатки 👇";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 37 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Sog'lig'ingizda muammo yo'qmi? Agar bo'lsa uni kiriting 👇";
                $button_lang = $back;
            } else {
                $text_lang = "У вас проблемы со здоровьем? Введите, если есть 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 39 and $text != "/start"){
        if (isset($text)){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $txt = base64_encode($text);
            $insertAnswer = "INSERT INTO answers (chat_id,question_id,answer) VALUES (".$chat_id.",37,'".$txt."')";
            $resultAnswer = $conn->query($insertAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 40 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "Iltimos o'zingizni rasmingizni jo'nating 🤳";
                $button_lang = $back;
            } else {
                $text_lang = "Пожалуйста, пришлите свое фото 🤳";
                $button_lang = $back_ru;
            }

            $response = bot('sendMessage', [
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);

            insertMessageId($response, $conn, $chat_id);
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 38 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'zingiz haqingizda nimalar qo'shmoqchisiz? Ijobiy taraflaringiz 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Что вы хотите добавить о себе? Ваши положительные стороны 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
    }

    if ($step_2 == 40 and $text != "/start"){
        if (isset($message->photo)) {
            if (isset($message->photo[2])) {
                $file_id = $message->photo[2]->file_id;
            } else if (isset($message->photo[1])) {
                $file_id = $message->photo[1]->file_id;
            } else {
                $file_id = $message->photo[0]->file_id;
            }
            bot('deleteMessage', [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);

            deleteMessageUser($conn, $chat_id);

            $updateFileId = "UPDATE users SET file_id = '".$file_id."' WHERE chat_id = ".$chat_id;
            $resultUpdateFileId = $conn->query($updateFileId);

            $stepUpdate = "UPDATE step SET step_2 = 41 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $selectUserAnswer = "SELECT * FROM answers WHERE chat_id = ".$chat_id;
            $resultUserAnswer = $conn->query($selectUserAnswer);

            if ($resultUserAnswer->num_rows > 0){
                $txtSend = '';
                $i = 1;
                if ($step_1 == 1){
                    $pdf_answer_title = "Sizning ma'lumotlaringiz ⤵️";
                    $pdf_button = $confirm;
                    $pdf_section = "Bo'lim: ";
                    $pdf_fio = "F.I.O: ";
                    $pdf_birthday = "Tug'ilgan kuningiz: ";
                    $pdf_address = "Manzilingiz: ";
                    $pdf_number = "Telefon raqamingiz: ";
                    $pdf_family = "Oilaviy ahvolingiz: ";
                    $pdf_study = "Ta'lim darajangiz: ";
                    $pdf_study_place = "O'qigan o'quv yurtingiz: ";
                    $pdf_study_time = "O'qigan yillaringiz: ";
                    $pdf_study_direction = "O'qigan yo'nalishingiz: ";
                    $pdf_work = "Bundan avval boshqa bir korxona yoki tashkilotda ishlaganmisiz: ";
                    $pdf_work_name = "Ishlagan korxona yoki tashkilotingiz nomi: ";
                    $pdf_work_time = "Ishlagan yillaringiz: ";
                    $pdf_work_direction = "Faoliyat yuritgan lavozimingiz: ";
                    $pdf_work_cause = "Ishdan ketgan sababingiz: ";
                    $pdf_work_any = "Yana boshqa tashkilotda ishlaganligingiz haqida: ";

                    $pdf_father_name = "Otangizning F.I.O: ";
                    $pdf_father_birthday = "Otangizning tu'gilgan sanasi: ";
                    $pdf_father_work = "Otangizning ish joyi: ";
                    $pdf_mather_name = "Onangizning F.I.O: ";
                    $pdf_mather_birthday = "Onangizning tug'ilgan sanasi: ";
                    $pdf_mather_work = "Onangizning ish joyi: ";
                    $pdf_family_name = "Oilangizning boshqa a'zolarini F.I.O: ";
                    $pdf_family_birthday = "Oilangizning boshqa a'zolarini tugilgan sanasi: ";
                    $pdf_family_work = "Oilangizning boshqa a'zolarini ishlash joyi:: ";

                    $pdf_military = "Harbiy xizmatga borganmisiz: ";
                    $pdf_convicted = "Umringiz davomida sudlanganmisiz: ";
                    $pdf_certificate = "Avtomobil haydash guvohnomangiz: ";
                    $pdf_car = "Shaxsiy avtomabilingiz rusumi: ";
                    $pdf_our_work_about = "Bizning korxona haqida qayerdan ma'lumot oldingiz: ";
                    $pdf_our_work_time = "Korxonada ishdan keyin qolib ishlashga rozimisiz: ";
                    $pdf_meeting = "Korxona majlislariga qatnashishga rozimisiz: ";
                    $pdf_collective = "Kollektiv ishlay olasizmi, Kollektiv ish deganda nimani tushunasiz: ";
                    $pdf_parents = "Ota-onangizni korxonamizga chaqirishimizga rozimisiz: ";
                    $pdf_health = "Sog'lig'ingizda muammo yo'qmi, Agar bo'lsa uni kiriting: ";
                    $pdf_positive = "O'zingiz haqingizda, Ijobiy taraflaringiz: ";
                    $pdf_negative = "O'zingiz haqingizda, Salbiy taraflaringiz: ";
                }
                else {
                    $pdf_answer_title = "Ваша информация ⤵️";
                    $pdf_button = $confirm_ru;
                    $pdf_section = "Раздел: ";
                    $pdf_fio = "Ф.И.О: ";
                    $pdf_birthday = "Ваш день рождения: ";
                    $pdf_address = "Ваш адресс: ";
                    $pdf_number = "Ваш номер телефон: ";
                    $pdf_family = "Ваше семейное положение: ";
                    $pdf_study = "Ваш уровень образования: ";
                    $pdf_study_place = "Ваше учебное заведение: ";
                    $pdf_study_time = "Годы учебы: ";
                    $pdf_study_direction = "Направление, которое вы изучаете: ";
                    $pdf_work = "Вы когда-нибудь работали в другой компании или организации: ";
                    $pdf_work_name = "Название компании или организации, в которой вы работали: ";
                    $pdf_work_time = "Лет службы в компании или организации: ";
                    $pdf_work_direction = "Ваше предыдущую должность в компании: ";
                    $pdf_work_cause = "Причина, по которой вы ушли: ";
                    $pdf_work_any = "Подробнее о работе в другой организации: ";

                    $pdf_father_name = "Ф.И.О вашего отца: ";
                    $pdf_father_birthday = "Дата рождение вашего отца: ";
                    $pdf_father_work = "Место работы вашего отца: ";
                    $pdf_mather_name = "Ф.И.О вашей матери: ";
                    $pdf_mather_birthday = "Дата рождение вашей матери: ";
                    $pdf_mather_work = "Место работы вашей матери: ";
                    $pdf_family_name = "Ф.И.О других членов вашей семьи: ";
                    $pdf_family_birthday = "Дата рождения других членов вашей семьи: ";
                    $pdf_family_work = "Место работы других членов вашей семьи: ";

                    $pdf_military = "Вы служили в армии: ";
                    $pdf_convicted = "Были ли вы судимы раньше: ";
                    $pdf_certificate = "Ваши водительские права: ";
                    $pdf_car = "Модель вашего личного автомобиля: ";
                    $pdf_our_work_about = "Откуда вы взяли информацию о нашей компании: ";
                    $pdf_our_work_time = "Вы соглашаетесь остаться и работать в компании после работы: ";
                    $pdf_meeting = "Согласны ли вы на участие в деловых встречах: ";
                    $pdf_collective = "Можете ли вы работать в команде? Что вы подразумеваете под командной работой?: ";
                    $pdf_parents = "Вы согласны пригласить своих родителей в нашу компанию: ";
                    $pdf_health = "Если у вас есть проблема со здоровьем, укажите ее, если она у вас есть: ";
                    $pdf_positive = "О себе, Ваши положительные стороны: ";
                    $pdf_negative = "О себе, Ваши недостатки: ";
                }
                while ($row = $resultUserAnswer->fetch_assoc()){
                    $questionId = $row['question_id'];
                    $userAnswer = base64_decode($row['answer']);
                    if ($questionId == 1){
                        $questionTitle = $pdf_section;
                    }
                    else if ($questionId == 2){
                        $questionTitle = $pdf_fio;
                    }
                    else if ($questionId == 3){
                        $questionTitle = $pdf_birthday;
                    }
                    else if ($questionId == 4){
                        $questionTitle = $pdf_address;
                    }
                    else if ($questionId == 5){
                        $questionTitle = $pdf_number;
                    }
                    else if ($questionId == 6){
                        $questionTitle = $pdf_family;
                    }
                    else if ($questionId == 7){
                        $questionTitle = $pdf_study;
                    }
                    else if ($questionId == 8){
                        $questionTitle = $pdf_study_place;
                    }
                    else if ($questionId == 9){
                        $questionTitle = $pdf_study_time;
                    }
                    else if ($questionId == 10){
                        $questionTitle = $pdf_study_direction;
                    }
                    else if ($questionId == 11){
                        $questionTitle = $pdf_work;
                    }
                    else if ($questionId == 12){
                        $questionTitle = $pdf_work_name;
                    }
                    else if ($questionId == 13){
                        $questionTitle = $pdf_work_time;
                    }
                    else if ($questionId == 14){
                        $questionTitle = $pdf_work_direction;
                    }
                    else if ($questionId == 15){
                        $questionTitle = $pdf_work_cause;
                    }
                    else if ($questionId == 16){
                        $questionTitle = $pdf_work_any;
                    }
                    else if ($questionId == 17){
                        $questionTitle = $pdf_father_name;
                    }
                    else if ($questionId == 18){
                        $questionTitle = $pdf_father_birthday;
                    }
                    else if ($questionId == 19){
                        $questionTitle = $pdf_father_work;
                    }
                    else if ($questionId == 20){
                        $questionTitle = $pdf_mather_name;
                    }
                    else if ($questionId == 21){
                        $questionTitle = $pdf_mather_birthday;
                    }
                    else if ($questionId == 22){
                        $questionTitle = $pdf_mather_work;
                    }
                    else if ($questionId == 23){
                        $questionTitle = $pdf_family_name;
                    }
                    else if ($questionId == 24){
                        $questionTitle = $pdf_family_birthday;
                    }
                    else if ($questionId == 25){
                        $questionTitle = $pdf_family_work;
                    }
                    else if ($questionId == 26){
                        $questionTitle = $pdf_military;
                    }
                    else if ($questionId == 27){
                        $questionTitle = $pdf_convicted;
                    }
                    else if ($questionId == 28){
                        $questionTitle = $pdf_certificate;
                    }
                    else if ($questionId == 29){
                        $questionTitle = $pdf_car;
                    }
                    else if ($questionId == 30){
                        $questionTitle = $pdf_our_work_about;
                    }
                    else if ($questionId == 31){
                        $questionTitle = $pdf_our_work_time;
                    }
                    else if ($questionId == 32){
                        $questionTitle = $pdf_meeting;
                    }
                    else if ($questionId == 33){
                        $questionTitle = $pdf_collective;
                    }
                    else if ($questionId == 34){
                        $questionTitle = $pdf_parents;
                    }
                    else if ($questionId == 35){
                        $questionTitle = $pdf_health;
                    }
                    else if ($questionId == 36){
                        $questionTitle = $pdf_positive;
                    }
                    else if ($questionId == 37){
                        $questionTitle = $pdf_negative;
                    }
                    $txtSend = $txtSend."*".$questionTitle."*".$userAnswer.PHP_EOL;
                }
                bot('sendMessage',[
                    'chat_id' => $chat_id,
                    'text' => "*".$pdf_answer_title."*".PHP_EOL.PHP_EOL.$txtSend,
                    'parse_mode' => 'markdown',
                    'reply_markup' => $pdf_button
                ]);
            }
        }
        else if ($data == "back"){
            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id." ORDER BY id DESC LIMIT 1";
            $resultDelete = $conn->query($deleteAnswer);

            $stepUpdate = "UPDATE step SET step_2 = 34 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            if ($step_1 == 1){
                $text_lang = "O'zingiz haqingizda nimalar qo'shmoqchisiz? Salbiy taraflaringiz 👇";
                $button_lang = $back;
            } else {
                $text_lang = "Что вы хотите добавить о себе? Ваши недостатки 👇";
                $button_lang = $back_ru;
            }

            bot('editMessageText', [
                'chat_id' => $chat_id,
                'message_id' => $message_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "remove"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage', [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }

    if ($step_2 == 41 and $text != "/start"){
        if ($data == "done"){
            bot('deleteMessage',[
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
            if ($step_1 == 1){
                $pdf_phone = "Telefon raqamingiz: ";
                $pdf_name = "F.I.SH: ";
                $pdf_section_caption = "Soha: ";
                $pdf_village = "Manzilingiz: ";
                $pdf_username = "Foydalanuvchi profili: ";
                $pdf_title = "Hujjat topshiruvchi ma'lumotlari";
                $pdf_user_info = "Hujjat topshiruvchi haqida";
                $pdf_user_famliy = "Hujjat topshiruvchi oilasi";
                $pdf_user_old_work = "Hujjat topshiruvchining avvalgi ish joyi";
                $pdf_user_study = "Ma'lumoti";
                $pdf_user_new_work = "Ish joy talablari";
                $pdf_answer_title = "Sizning ma'lumotlaringiz ⤵️";
                $pdf_button = $confirm;
                $pdf_section = "Bo'lim: ";
                $pdf_fio = "F.I.O: ";
                $pdf_birthday = "Tug'ilgan kuni: ";
                $pdf_address = "Manzili: ";
                $pdf_number = "Telefon raqami: ";
                $pdf_family = "Oilaviy ahvoli: ";
                $pdf_study = "Ta'lim darajasi: ";
                $pdf_study_place = "O'qigan o'quv yurti: ";
                $pdf_study_time = "O'qigan yillari: ";
                $pdf_study_direction = "O'qigan yo'nalishi: ";
                $pdf_work = "Bundan avval boshqa bir korxona yoki tashkilotda ishlaganmi: ";
                $pdf_work_name = "Ishlagan korxona yoki tashkiloti nomi: ";
                $pdf_work_time = "Ishlagan yillari: ";
                $pdf_work_direction = "Faoliyat yuritgan lavozimi: ";
                $pdf_work_cause = "Ishdan ketgan sababi: ";
                $pdf_work_any = "Yana boshqa tashkilotda ishlaganligi haqida: ";
                $pdf_father = "Otam";
                $pdf_mather = "Onam";
                $pdf_any = "Boshqa";
                $pdf_family_about = "Oila a'zolari: ";
                $pdf_family_name = "Oila a'zolarining F.I.O: ";
                $pdf_family_birthday = "Oila a'zolarining tug'ilgan sanalari: ";
                $pdf_family_work = "Oila a'zolarining ishlash joylari: ";
                $pdf_military = "Harbiy xizmatga borganmisiz: ";
                $pdf_convicted = "Sudlanganmisiz: ";
                $pdf_certificate = "Avtomobil haydash guvohnomangiz: ";
                $pdf_car = "Shaxsiy avtomabilingiz rusimi: ";
                $pdf_our_work_about = "Bizning korxona haqida qayerdan ma'lumot oldingiz: ";
                $pdf_our_work_time = "Korxonada ishdan keyin qolib ishlashga rozimisiz: ";
                $pdf_meeting = "Korxona majlislariga qatnashishga rozimisiz: ";
                $pdf_collective = "Kollektiv bilan ishlay olasizmi, Kollektiv ish deganda nimani tushunasiz: ";
                $pdf_parents = "Ota-onangizni korxonamizga chaqirishimizga rozimisiz: ";
                $pdf_health = "Sog'lig'ingizda muammo yo'qmi, Agar bo'lsa uni kiriting: ";
                $pdf_positive = "O'zingiz haqingizda, Ijobiy taraflaringiz: ";
                $pdf_negative = "O'zingiz haqingizda, Salbiy taraflaringiz: ";
            }
            else {
                $pdf_phone = "Ваш номер телефон: ";
                $pdf_name = "Ф.И.О: ";
                $pdf_section_caption = "Раздел: ";
                $pdf_village = "Ваш адрес: ";
                $pdf_username = "Ваш профиль: ";
                $pdf_title = "Информация о заявителе";
                $pdf_user_info = "О заявителе";
                $pdf_user_famliy = "Семья заявителя";
                $pdf_user_old_work = "Предыдущее место работы заявителя";
                $pdf_user_study = "Информация";
                $pdf_user_new_work = "Требования место работы";
                $pdf_answer_title = "Ваша информация ⤵️";
                $pdf_button = $confirm_ru;
                $pdf_section = "Раздел: ";
                $pdf_fio = "Ф.И.О: ";
                $pdf_birthday = "День рождения: ";
                $pdf_address = "Адресс: ";
                $pdf_number = "Номер телефон: ";
                $pdf_family = "Семейное положение: ";
                $pdf_study = "Уровень образования: ";
                $pdf_study_place = "Учебное заведение: ";
                $pdf_study_time = "Годы учебы: ";
                $pdf_study_direction = "Направление, которое вы изучаете: ";
                $pdf_work = "Когда-нибудь работали в другой компании или организации: ";
                $pdf_work_name = "Название компании или организации, в которой вы работали: ";
                $pdf_work_time = "Лет службы в компании или организации: ";
                $pdf_work_direction = "Текущее должность: ";
                $pdf_work_cause = "Причина, по которой вы ушли: ";
                $pdf_work_any = "Подробнее о работе в другой организации: ";
                $pdf_father = "Папа";
                $pdf_mather = "Мама";
                $pdf_any = "Другой";
                $pdf_family_about = "Члены семьи: ";
                $pdf_family_name = "Члены семьи Ф.И.О: ";
                $pdf_family_birthday = "Даты рождения членов семьи: ";
                $pdf_family_work = "Рабочие места членов семьи: ";
                $pdf_military = "Вы служили в армии: ";
                $pdf_convicted = "Были ли вы судимы раньше: ";
                $pdf_certificate = "Водительские права: ";
                $pdf_car = "Модель вашего личного автомобиля: ";
                $pdf_our_work_about = "Откуда вы взяли информацию о нашем компании: ";
                $pdf_our_work_time = "Согласны ли вы на сверхурочную работу: ";
                $pdf_meeting = "Согласны ли вы на участие в деловых встречах: ";
                $pdf_collective = "Можете ли вы работать в команде? Что вы подразумеваете под командной работой?: ";
                $pdf_parents = "Вы согласны пригласить своих родителей в нашу компанию: ";
                $pdf_health = "Если у вас есть проблема со здоровьем, укажите ее, если она у вас есть: ";
                $pdf_positive = "О себе, Ваши положительные стороны: ";
                $pdf_negative = "О себе, Ваши недостатки: ";
            }

            $selectUserImage = "SELECT * FROM users WHERE chat_id = ".$chat_id;
            $resultImage = $conn->query($selectUserImage);
            $row = $resultImage->fetch_assoc();
            $imageFileId = bot('getFile', [
                'file_id' => $row['file_id']
            ]);
            if(isset($imageFileId)) {
                $path = 'https://api.telegram.org/file/bot'.TOKEN.'/'.$imageFileId->result->file_path;
            }

            $_file_name = strtotime("Y-m-d H:i:s").rand(1000, 9999);
            $doc_name = "gippobaby_".rand(1000, 9999).'_'.$_file_name;

            $current = date('Y-m-d H:i:s');

            $dompdf = new Dompdf();

            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $html = '

            <!DOCTYPE html>
            <html>
            <head>
                <title></title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

                <style>
                    *{
                        font-family: DejaVu Sans, sans-serif!important;
                    }
                    table {
                            border-collapse: collapse;
                    }

                    div.page_break + div.page_break{
                        page-break-before: always;
                    }
                    td {
                        text-align: center !important;
                    }
                </style>
            </head>
            <body>

            <div style="margin-top: 20px; width: 200px;">
                <img style="width: 100%;" src="'.$base64.'" alt="">
            </div>
            
            <div>
                   <h1 style="text-align: center !important; font-weight: normal !important">'.$pdf_title.'</h1> 
            </div>
            ';

            $selectUserAnswer = "SELECT * FROM answers WHERE chat_id = ".$chat_id;
            $resultUserAnswer = $conn->query($selectUserAnswer);

            if ($resultUserAnswer->num_rows > 0) {
                $i = 1;
                $a = 1;
                $arr_famliy_answer = [];
                $textCaption = '';
                $pdf_caption = '';
                while ($row = $resultUserAnswer->fetch_assoc()) {
                    $questionId = $row['question_id'];
                    $userAnswer = base64_decode($row['answer']);
                    if ($questionId == 1){
                        $questionTitle = $pdf_section;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                        $pdf_caption .= "#️⃣ *".$pdf_section_caption."*".$userAnswer.PHP_EOL;
                    }
                    else if ($questionId == 2){
                        $questionTitle = $pdf_fio;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                        $textCaption .= "👤 *".$questionTitle."*".$userAnswer.PHP_EOL;
                    }
                    else if ($questionId == 3){
                        $questionTitle = $pdf_birthday;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 4){
                        $questionTitle = $pdf_address;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                        $pdf_caption .= "📍 *".$pdf_village."*".$userAnswer.PHP_EOL;
                    }
                    else if ($questionId == 5){
                        $questionTitle = $pdf_number;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 6){
                        $questionTitle = $pdf_family;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    // study
                    else if ($questionId == 7){
                        $arr_study_title = [];
                        $arr_study_title[] = "№";
                        $arr_study_answer = [];
                        $arr_study_answer[] = "1";
                        $questionTitle = $pdf_study;
                        $arr_study_title[] = $questionTitle;
                        $arr_study_answer[] = $userAnswer;
                    }
                    else if ($questionId == 8){
                        $questionTitle = $pdf_study_place;
                        $arr_study_title[] = $questionTitle;
                        $arr_study_answer[] = $userAnswer;
                    }
                    else if ($questionId == 9){
                        $questionTitle = $pdf_study_time;
                        $arr_study_title[] = $questionTitle;
                        $arr_study_answer[] = $userAnswer;
                    }
                    else if ($questionId == 10){
                        $questionTitle = $pdf_study_direction;
                        $arr_study_title[] = $questionTitle;
                        $arr_study_answer[] = $userAnswer;

                        $html = $html.'
                            <div class="question_one_by_one">
                                <p style="font-weight: normal; font-size: 25px; margin-bottom: 10px !important;"><i>'.$pdf_user_study.'</i></p>
                            </div>
                            <table border="1" style="width: 100%; margin-bottom: 10px !important;">
                                <tr>';
                            foreach ($arr_study_title as $value){
                                $html = $html."<th><b>".$value."</b></th>";
                            }
                            $html = $html.'
                                </tr>';
                            $html = $html.'
                            
                                <tr>';
                            foreach ($arr_study_answer as $value){
                                $html = $html."<td>".$value."</td>";
                            }
                        $html = $html.'
                            </tr></table>';
                    }
                    // study
                    else if ($questionId == 11){
                        $questionTitle = $pdf_work;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    // work
                    else if ($questionId == 12){
                        $arr_work_title = [];
                        $arr_work_title[] = "№";
                        $arr_work_answer = [];
                        $arr_work_answer[] = "1";
                        $questionTitle = $pdf_work_name;
                        $arr_work_title[] = $questionTitle;
                        $arr_work_answer[] = $userAnswer;
                    }
                    else if ($questionId == 13){
                        $questionTitle = $pdf_work_time;
                        $arr_work_title[] = $questionTitle;
                        $arr_work_answer[] = $userAnswer;
                    }
                    else if ($questionId == 14){
                        $questionTitle = $pdf_work_direction;
                        $arr_work_title[] = $questionTitle;
                        $arr_work_answer[] = $userAnswer;
                    }
                    else if ($questionId == 15){
                        $questionTitle = $pdf_work_cause;
                        $arr_work_title[] = $questionTitle;
                        $arr_work_answer[] = $userAnswer;

                        $html = $html.'
                            <div class="question_one_by_one ">
                                <p style="font-weight: normal; font-size: 25px; margin-bottom: 10px !important;"><i>'.$pdf_user_old_work.'</i></p>
                            </div>
                            <table border="1" style="width: 100%; margin-bottom: 10px!important">
                                <tr>';
                        foreach ($arr_work_title as $value){
                            $html = $html."<th><b>".$value."</b></th>";
                        }
                        $html = $html.'
                                </tr>';
                        $html = $html.'
                            
                                <tr>';
                        foreach ($arr_work_answer as $value){
                            $html = $html."<td>".$value."</td>";
                        }
                        $html = $html.'
                            </tr></table>';
                    }
                    // work
                    else if ($questionId == 16){
                        $questionTitle = $pdf_work_any;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    // famliy
                    else if ($questionId == 17){
                        $arr_famliy_answer[] = $i;
                        $arr_famliy_answer[] = $pdf_father;
                        $arr_famliy_answer[] = $userAnswer;
                        $i++;
                    }
                    else if ($questionId == 18){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 19){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 20){
                        $arr_famliy_answer[] = $i;
                        $arr_famliy_answer[] = $pdf_mather;
                        $arr_famliy_answer[] = $userAnswer;
                        $i++;
                    }
                    else if ($questionId == 21){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 22){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 23){
                        $arr_famliy_answer[] = $i;
                        $arr_famliy_answer[] = $pdf_any;
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 24){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    else if ($questionId == 25){
                        $arr_famliy_answer[] = $userAnswer;
                    }
                    // famliy
                    else if ($questionId == 26){
                        $html = $html.'
                            <div class="question_one_by_one ">
                                <p style="font-weight: normal; font-size: 25px; margin-bottom: 10px !important;"><i>'.$pdf_user_famliy.'</i></p>
                            </div>
                            <table border="1" style="width: 100%; margin-bottom: 10px!important">
                                <tr>
                                    <th><b>№</b></th>
                                    <th><b>'.$pdf_family_about.'</b></th>
                                    <th><b>'.$pdf_family_name.'</b></th>
                                    <th><b>'.$pdf_family_birthday.'</b></th>
                                    <th><b>'.$pdf_family_work.'</b></th>
                                </tr>';
                        foreach ($arr_famliy_answer as $value){
                            if ($a == 1){
                                $html = $html."<tr>";
                            }
                            $html = $html."<td>".$value."</td>";
                            if ($a == 5){
                                $a = 0;
                                $html = $html."</tr>";
                            }
                            $a++;
                        }
                        $html = $html.'
                            </table>';

                        $questionTitle = $pdf_military;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 27){
                        $questionTitle = $pdf_convicted;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 28){
                        $questionTitle = $pdf_certificate;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 29){
                        $questionTitle = $pdf_car;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 30){
                        $questionTitle = $pdf_our_work_about;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    // new work
                    else if ($questionId == 31){
                        $arr_new_work_title = [];
                        $arr_new_work_title[] = "№";
                        $arr_new_work_answer = [];
                        $arr_new_work_answer[] = "1";
                        $questionTitle = $pdf_our_work_time;
                        $arr_new_work_title[] = $questionTitle;
                        $arr_new_work_answer[] = $userAnswer;
                    }
                    else if ($questionId == 32){
                        $questionTitle = $pdf_meeting;
                        $arr_new_work_title[] = $questionTitle;
                        $arr_new_work_answer[] = $userAnswer;
                    }
                    else if ($questionId == 33){
                        $questionTitle = $pdf_collective;
                        $arr_new_work_title[] = $questionTitle;
                        $arr_new_work_answer[] = $userAnswer;

                        $html = $html.'
                            <div class="question_one_by_one">
                                <p style="font-weight: normal; font-size: 25px; margin-bottom: 10px !important;"><i>'.$pdf_user_new_work.'</i></p>
                            </div>
                            <table border="1" style="width: 100%; margin-bottom: 10px !important">
                                <tr>';
                        foreach ($arr_new_work_title as $value){
                            $html = $html."<th><b>".$value."</b></th>";
                        }
                        $html = $html.'
                                </tr>';
                        $html = $html.'
                            
                                <tr>';
                        foreach ($arr_new_work_answer as $value){
                            $html = $html."<td>".$value."</td>";
                        }
                        $html = $html.'
                            </tr></table>';
                    }
                    // new work
                    else if ($questionId == 34){
                        $questionTitle = $pdf_parents;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    else if ($questionId == 35){
                        $questionTitle = $pdf_health;
                        $html = $html."<p style='font-size: 20px; margin: 0 !important'>".$questionTitle."<span style='color: #616161'>".$userAnswer."</span></p>";
                    }
                    // health
                    else if ($questionId == 36){
                        $arr_health_title = [];
                        $arr_health_title[] = "№";
                        $arr_health_answer = [];
                        $arr_health_answer[] = "1";
                        $questionTitle = $pdf_positive;
                        $arr_health_title[] = $questionTitle;
                        $arr_health_answer[] = $userAnswer;
                    }
                    else if ($questionId == 37){
                        $questionTitle = $pdf_negative;
                        $arr_health_title[] = $questionTitle;
                        $arr_health_answer[] = $userAnswer;

                        $html = $html.'
                            <div class="question_one_by_one ">
                                <p style="font-weight: normal; font-size: 25px;"><i>'.$pdf_user_info.'</i></p>
                            </div>
                            <table border="1" style="width: 100%; margin-bottom: 20px!important">
                                <tr>';
                        foreach ($arr_health_title as $value){
                            $html = $html."<th><b>".$value."</b></th>";
                        }
                        $html = $html.'
                                </tr>';
                        $html = $html.'
                            
                                <tr>';
                        foreach ($arr_health_answer as $value){
                            $html = $html."<td>".$value."</td>";
                        }
                        $html = $html.'
                            </tr></table>';
                    }
                    // health
                }
            }

            $html = $html.'
            <div class="page_break"></div>

                <script src="bootstrap-4.6.0-dist/bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
            </body>
            </html>
        ';

            $dompdf->loadHtml($html);

            $dompdf->setPaper('ra3', 'portrait');

            $dompdf->render();

            file_put_contents("uploads/".$doc_name.".pdf", $dompdf->output());

            $selectInfo = "SELECT * FROM users WHERE chat_id = ".$chat_id;
            $resultInfo = $conn->query($selectInfo);

            if ($resultInfo->num_rows > 0){
                $row = $resultInfo->fetch_assoc();
                $username = $row['username'];
                $phone_number = base64_decode($row['phone_number']);

                if (isset($username) and !empty($username)){
                    $textCaption.= "📞 *".$pdf_phone."* ".$phone_number.PHP_EOL."🅿️ *".$pdf_username."* @".$username.PHP_EOL.$pdf_caption;
                } else {
                    $textCaption.= "📞 *".$pdf_phone."* ".$phone_number.PHP_EOL.$pdf_caption;

                }
            }

            $res = bot('sendDocument', [
                'chat_id' => $chat_id,
                'document' => "https://gippobaby.uz/gippobaby_hr_bot/uploads/".$doc_name.".pdf",
                'parse_mode' => 'markdown',
                'caption' => $textCaption
            ]);

            $file_id = $res->result->document->file_id;
            $pdf_file_id = "INSERT INTO pdf (chat_id,file_id) VALUES (".$chat_id.",'".$file_id."')";
            $resultPdfFileId = $conn->query($pdf_file_id);

            bot('sendDocument', [
                'chat_id' => -557509529,
                'document' => "https://gippobaby.uz/gippobaby_hr_bot/uploads/".$doc_name.".pdf",
                'parse_mode' => 'markdown',
                'caption' => $textCaption
            ]);

            unlink('uploads/' . $doc_name . '.pdf');

            $stepUpdate = "UPDATE step SET step_2 = 1 WHERE chat_id = ".$chat_id;
            $resultStep = $conn->query($stepUpdate);

            $deleteAnswer = "DELETE FROM answers WHERE chat_id = ".$chat_id;
            $resultDelete = $conn->query($deleteAnswer);

            if ($step_1 == 1){
                $text_lang = "_Sizning anketangiz hodimlarimizga muvaffaqiyatli jo'natildi.
24 soat ichida ko‘rib chiqib sizga aloqaga chiqamiz.
Eʼtiboringiz uchun rahmat !_".PHP_EOL.PHP_EOL."*Asosiy bo'limga xush keldingiz 😊*";
                $button_lang = $filing;
            } else {
                $text_lang = "_Ваша заявка была успешно отправлена нашим сотрудникам.
Мы рассмотрим и свяжемся с вами в течение 24 часов.
Спасибо за внимание!_". PHP_EOL.PHP_EOL."*Добро пожаловать в основной раздел 😊*";
                $button_lang = $filing_ru;
            }

            bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => $text_lang,
                'parse_mode' => 'markdown',
                'reply_markup' => $button_lang
            ]);
        }
        else if ($data == "delete"){
            cancelAllAnswer($step_1, $conn, $chat_id, $message_id);
        }
        else {
            bot('deleteMessage', [
                'chat_id' => $chat_id,
                'message_id' => $message_id
            ]);
        }
    }