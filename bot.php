<?php 
    ini_set('display_errors', true);
    $servername = "localhost";
    $username = "gippobab_shop_user";
    $dbname = "gippobab_shop";
    $password = "z7AFUp*8RQFA";
    
    $conn = mysqli_connect($servername,$username,$password,$dbname);

    if(!$conn){
        die("Connection failed:".mysqli_connect_error());
    } else {
        echo "SuccessFull";
    }  

    const TOKEN = '1975190483:AAHZBeqUbPWr3UXvjiSC9rSenjRBF69jFDM';
    const BASE_URL = 'https://api.telegram.org/bot'.TOKEN;

    function bot($method, $data = []){
        $url = BASE_URL.'/'.$method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($ch);
        
        if(curl_error($ch))
        {
         var_dump(curl_error($ch));   
        }
        else{
            return json_decode($res);
        }
    }

    class Payment
    {
        public $amount;
        public $order_code;
        public $client_id;
        public $provider;
    }

    function botFile($method, $data = []) {
        $url = BASE_URL.'/'.$method;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $res = curl_exec($ch);
        curl_close($ch);
        
        if(curl_error($ch)) {
            var_dump(curl_error($ch));
        } else {
            return json_decode($res);
        }
    }


    function typing($ch){
        return bot('sendChatAction',[
            'chat_id' => $ch,
            'action' => 'typing'
            ]);
    }

    function deleteMessage($chat_id,$message_id) {
        bot('deleteMessage',[
            'chat_id' => $chat_id,
            'message_id' => $message_id
        ]);
    }

    function sendMessage($chat_id,$text) {
        $result = bot('sendMessage',[
            'chat_id' => $chat_id,
            'text' => $text,
            'parse_mode' => "html"
        ]);
        return $result;
    }
    function sendLocation($chat_id,$lat,$lang) {
        $result = bot('sendLocation',[
            'chat_id' => $chat_id,
            'latitude' => $lat,
            'longitude' => $lang,
        ]);
        return $result;
    }
    function sendLocationBtn($chat_id,$lat,$lang,$btn) {
        $result = bot('sendLocation',[
            'chat_id' => $chat_id,
            'latitude' => $lat,
            'longitude' => $lang,
            'reply_markup' => $btn
        ]);
        return $result;
    }

    function sendMessageBtn($chat_id,$text,$btn) {
        $result = bot('sendMessage',[
            'chat_id' => $chat_id,
            'text' => $text,
            'reply_markup' => $btn,
            'parse_mode' => "html"
        ]);
        return $result;
    }

    function sendPhoto($chat_id,$photo,$caption) {
        $result = bot('sendPhoto',[
            'chat_id' => $chat_id,
            'photo' => $photo,
            'caption' => $caption,
            'parse_mode' => 'html'
        ]);
        return $result;

    }

    function sendPhotoBtn($chat_id,$photo,$caption,$btn) {
        $result = bot('sendPhoto',[
            'chat_id' => $chat_id,
            'photo' => $photo,
            'caption' => $caption,
            'reply_markup' => $btn,
            'parse_mode' => 'html'
        ]);
        return $result;
    }

    function updateStep($step,$chat_id,$conn) {
        $sql = "UPDATE step SET step_2 = ".$step." WHERE chat_id = ".$chat_id;
        $conn->query($sql);
    }

    function updateLogMessage($chat_id,$log_message_id,$conn) {
       $sql_update = "update log_messages set message_id = ".$log_message_id." where chat_id =  ".$chat_id;
       $result = $conn->query($sql_update); 
       return $result;
    }

    function insertLogMessage($chat_id,$log_message_id,$conn) {
       $sql = "INSERT INTO log_messages (chat_id,message_id) VALUES (".$chat_id.",".$log_message_id.")";
       // return $sql;
       $result = $conn->query($sql); 
    }

    function deleteLogMessage($chat_id,$conn) {
       $sql = "DELETE FROM log_messages where chat_id = ".$chat_id;
       $result = $conn->query($sql); 
       return $result;
    }

    function editMessageCaption($chat_id,$message_id,$caption,$btn) {
        bot('editMessageCaption',[
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'caption' => $caption,
            'reply_markup' => $btn,
            'parse_mode' => 'html'
        ]);
    }

    function editMessageText($chat_id,$message_id,$txt,$btn_details) {
        bot("editMessageText",[
            'chat_id' => $chat_id,
            'message_id' => $message_id,
            'text' => $txt,
            'reply_markup' => $btn_details,
            'parse_mode' => 'html'
        ]);
    }

    // MENU IS BUILD

    function getCatalog($conn,$step_1) {

        $sql = "SELECT * FROM category where status = 1 limit 10";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $row_btn = [];
            $btn = [];
            $i = 1;
            while ($row = $result->fetch_assoc()) {
                $clb_data = "category_".$row["id"];
                if ($step_1 == 1) {
                    $title = $row["title_uz"];
                    $home = "Bosh menu 🏡";
                } else if ($step_1 == 2) {
                    $title = $row["title_ru"];
                    $home = "Главное меню 🏡";
                }
                $btn[] = ["callback_data" => $clb_data,"text" => $title];
                if ($i % 2 == 0) {
                    $row_btn[] = $btn;
                    $btn = [];
                }

                $i++;
            }

            if (count($btn) == 1) {
                $row_btn[] = $btn;
            }
            $row_btn[] = [["callback_data" => "menu","text" => $home]];

            $btn_menu = json_encode([
                "inline_keyboard" => $row_btn,
            ]);
        }
        return $btn_menu;
    }

    $update = file_get_contents('php://input');
    
    $update = json_decode($update);
    $message = " | ";
    $text = " | ";
    $chat_id = " | ";
    $name = " | ";
    $username = " | ";
    $surname = "";
    $data = " | ";
    $chid = " | ";
    $mid = " | ";
    if (isset($update->message)) {
        $message = $update->message;
        $message_id = $message->message_id;
        $text = $message->text;
        $chat_id = $message->chat->id;
        $name = $message->chat->first_name;
        $username = $message->chat->username;
        if(isset($message->chat->last_name)){
            $surname = $message->chat->last_name;
        }
    } else {
        $data = $update->callback_query->data;
        $chat_id = $update->callback_query->message->chat->id;
        $message_id = $update->callback_query->message->message_id;
    }

    if (isset($update->pre_checkout_query)) {
        $checkout = $update->pre_checkout_query;
        $payload_info  = json_decode($checkout->invoice_payload);
        $amount  = $payload_info->amount;
        $order_code  = $payload_info->order_code;
        $client_id  = $payload_info->client_id;
        $provider  = $payload_info->provider;
        $provider_type = 0;
        if ($provider == 'click') {
            $provider_type = 1;
        } else if ($provider == 'payme') {
            $provider_type = 2;
        }
        $sql = "
            SELECT 
                *
            FROM order_code as oc 
            INNER JOIN arrive_orders as ao ON oc.order_code = ao.order_code_id
            WHERE oc.order_code = ".$order_code." and client_id = ".$client_id." and sell_price = ".$amount;
        $result = $conn->query($sql);
    
        if ($result->num_rows > 0) {
            $sql_update = "UPDATE order_code SET pay = ".$provider_type.", billing_status = 3 where order_code = ".$order_code;
            $result_update = $conn->query($sql_update);
            if ($result_update) {
                // sendMessage(1033542488,'success');
                $checkout_id = $checkout->id; 

                $get = bot('answerPreCheckoutQuery',[
                    'pre_checkout_query_id' => $checkout_id,
                    'ok' => 'true'
                ]);
            } else {
                sendMessage(1033542488,$sql_update);
            }
        } else {
            sendMessage(1033542488,$sql);
        }
    }

    if (isset($update->message->successful_payment)) {
        $successFullPayment = $update->message->successful_payment;
        $successInvoice = json_decode($successFullPayment->invoice_payload);
        $amount  = $successInvoice->amount;
        $order_code  = $successInvoice->order_code;
        $client_id  = $successInvoice->client_id;
        $provider  = $successInvoice->provider;
        $provider_type = 0;
        if ($provider == 'click') {
            $provider_type = 1;
        } else if ($provider == 'payme') {
            $provider_type = 2;
        }
        $sql = "
            SELECT 
                *
            FROM order_code as oc 
            INNER JOIN arrive_orders as ao ON oc.order_code = ao.order_code_id
            WHERE oc.billing_status = 3 and oc.order_code = ".$order_code." and oc.client_id = ".$client_id." and ao.sell_price = ".$amount;
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $sql_update = "UPDATE order_code SET billing_status = 4 where order_code = ".$order_code;
            $result_update = $conn->query($sql_update);
            if ($result_update) {
                $sql = "SELECT st.step_1,st.chat_id FROM clients AS cl INNER JOIN step AS st on cl.chat_id = st.chat_id  where cl.id = ".$client_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0 ) {
                    $row = $result->fetch_assoc();
                    $step_1 = $row["step_1"];
                    $chat_id = $row["chat_id"];
                    if ($step_1 == 1) {
                        $successful_payment = "Sizning to'lovingiz muvaffaqiyatli amalga oshirldi.Buyurtmangizni tez orada ko`rib chiqladi!";
                        $menuText = "🏡 Bosh menu";
                    } else if ($step_1 == 2) {
                        $menuText = "🏡 Главное меню";
                        $successful_payment = "Sizning to'lovingiz muvaffaqiyatli amalga oshirldi.Buyurtmangizni tez orada ko`rib chiqladi!";
                    }

                    $back_to_menu = json_encode([
                        'inline_keyboard' =>[
                            [
                                [
                                    'callback_data' => "menu",
                                    'text'=> $menuText
                                ],

                                
                            ],
                        ]
                    ]);
                    sendMessageBtn($chat_id,$successful_payment,$menu_btn);
                }
                $checkout_id = $checkout->id; 
            } else {
                sendMessage(1033542488,$sql_update);
            }
        } else {
            sendMessage(1033542488,$sql);
        }
    }

    // bot('sendMessage',[
    //     'chat_id' => 1033542488,
    //     'text' => json_encode($update)
    // ]);
    $empty = "https://static.thenounproject.com/png/538404-200.png";

    // BTN LANGUAGE
    $lang = json_encode([
        'inline_keyboard' =>[
            [
                [
                    'callback_data' => "uz",
                    'text'=>"O`zbek 🇺🇿"
                ],
                [
                    'callback_data' => "ru",
                    'text'=>"Русский 🇷🇺"
                ]
                
            ],
        ]
    ]);

    // START 

    if ($text == "/start") {
        $sql_drop = "DELETE FROM orders where status = 0 and chat_id = ".$chat_id;
        $conn->query($sql_drop);
        
        $sql = "SELECT * FROM clients where chat_id = ".$chat_id;
        $result = $conn->query($sql);

        $sql_update = "UPDATE orders SET status = 3 where status = 2 and chat_id = ".$chat_id;
        $conn->query($sql_update);

        if (!$result->num_rows > 0) {
            $name = base64_encode($name);
            $username = base64_encode($username);

            $sqlOne = "INSERT INTO clients (chat_id, tg_name, tg_username) VALUES (".$chat_id.",'".$name."','".$username."')";
            $conn->query($sqlOne);

            $sqlTwo = "INSERT INTO step (chat_id, step_1, step_2) VALUES (".$chat_id.",0,0)";
            $conn->query($sqlTwo);

            $sqlThree = "INSERT INTO log_messages (chat_id,message_id) VALUES(".$chat_id.",0)";
            $conn->query($sqlThree);

            $sqlFour = "INSERT INTO last_id (chat_id,last_id) VALUES (".$chat_id.",0)";;
            $conn->query($sqlFour);

            bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "🇺🇿 Ўзингизга қулай бўлган тилни танланг \n🇷🇺 Выберите язык, на котором вам комфортно общаться",
                'reply_markup' => $lang
            ]);
        } else {

            $sql = "UPDATE step SET step_1 = 0 , step_2 = 0 where chat_id = ".$chat_id;
            $conn->query($sql);

            bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => "🇺🇿 Ўзингизга қулай бўлган тилни танланг \n🇷🇺 Выберите язык, на котором вам комфортно общаться",
                'reply_markup' => $lang
            ]);
        }
    }
    // die();
    $step_1 = 0;
    $step_2 = 0;
    $sql = "SELECT * FROM step WHERE chat_id = ".$chat_id;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $step_1 = $row["step_1"];
        $step_2 = $row["step_2"];
    }

    // CHOOSE LANGUAGE

    if ($step_1 == 0 AND $step_2 == 0 and $text != "/start") {
        
        if (isset($update->callback_query)) {
            
            deleteMessage($chat_id,$message_id);
            if ($data == "uz") {
                $sql = "UPDATE step SET step_1 = 1 WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result == TRUE) {
                    $clientPohneNumberText = "📞 Telefon raqam Qoldirish";
                    $sendTelText = "Iltimos telefon raqamingizni qoldiring!";
                }
                $ourMenu = "Bizning menu!";
            } else if ($data == "ru") {
                $sql = "UPDATE step SET step_1 = 2 WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result == TRUE) {
                    $clientPohneNumberText = "📞 Оставьте номера телефонов";
                    $sendTelText = "Пожалуйста оставьте свой номер телефона!";
                }
                $ourMenu = "Наш меню!";
            }
            $sql = "SELECT * FROM clients where  status = 1 and chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                if ($data == "uz") {
                    $product_catalog = "Mahsulotlar Katalogi 🧳";
                    $karzinka = "Savatcha 🛒";
                    $about_us = "Biz Haqimizda ℹ️";
                    $my_orders = "Mening buyurtmalarim 🛍";
                    $about_my = "Mening ma'lumotlarim 👤";
                    $feedback = "Qayta Aloqa 📲";
                    $change_language = "🇺🇿 🔄 🇷🇺 Tilni o`zgartitish";
                    $caption_txt = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio doloribus quaerat ab omnis dolor, deserunt, ducimus ipsam eum porro explicabo fugiat quis a numquam culpa quisquam, debitis in dolores nihil!';
                } else if ($data == "ru") {
                    $product_catalog = "Каталог товаров 🧳";
                    $karzinka = "Корзина 🛒";
                    $about_us = "О нас ℹ️";
                    $my_orders = "Мои заказы 🛍";
                    $about_my = "Моя информация 👤";
                    $feedback = "Обратная Связь 📲";
                    $change_language = "🇺🇿 🔄 🇷🇺 Изменить язык";
                    $caption_txt = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio doloribus quaerat ab omnis dolor, deserunt, ducimus ipsam eum porro explicabo fugiat quis a numquam culpa quisquam, debitis in dolores nihil!';
                }
               
                $menu_btn = json_encode([
                    'keyboard' =>[
                        [
                            [
                                "text" => $product_catalog
                            ],
                            
                            
                        ],
                        [
                            [
                                "text" => $about_my,
                            ],
                            [
                                "text" => $my_orders
                            ]
                            
                        ],
                        [
                            [
                                "text" => $change_language,
                            ],
                            [
                                "text" => $about_us
                            ]
                            
                        ],
                        [
                            [
                                "text" => $karzinka
                            ]
                        ],
                    ],
                    "resize_keyboard" => true
                ]);


                $get_result = bot('sendPhoto',[
                    'chat_id' => $chat_id,
                    'photo' => 'https://myexact.uz/web/images/img/logo.png',
                    'caption' => $caption_txt
                ]);

                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
                // $btn_menu = getMenu($conn,$stepMenu);
                $get_result = sendMessageBtn($chat_id,$ourMenu,$menu_btn);

                updateStep(2,$chat_id,$conn);
                
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            } else {
                $replyMarkup = array(
                    'keyboard' => array(
                        array(
                            array( 
                                  'text'=>"$clientPohneNumberText",
                                  'request_contact'=>true
                                )
                            ),
                        ),
                    'one_time_keyboard' => true,
                    'resize_keyboard' => true
                );
                $givePhoneNumberBtn = json_encode($replyMarkup);

                $get_result = bot('sendMessage',[
                    'chat_id' => $chat_id,
                    'text' => $sendTelText,
                    'reply_markup' => $givePhoneNumberBtn
                ]);

                $question_message_id = $get_result->result->message_id;
                $sql_update = "update log_messages set message_id = ".$question_message_id." where chat_id =  ".$chat_id;
                $conn->query($sql_update);
            }
        }
    }

    if ($step_1 == 1) {
        $menuText = "🏡 Bosh menu";
        $back_text = "⬅️ Ortga";
        $product_catalog = "Mahsulotlar Katalogi 🧳";
        $karzinka = "Savatcha 🛒";
        $about_us = "Biz Haqimizda ℹ️";
        $my_orders = "Mening buyurtmalarim 🛍";
        $about_my = "Mening ma'lumotlarim 👤";
        $feedback = "Qayta Aloqa 📲";
        $cancel_text = "❌ Bekor qilish";
        $summText = "so'm";
        $take_order = "✅ Buyurtmani tasdiqlash";
        $back = "⬅️ Ortga";
        $karzina = "✅ Buyurtma berish";
        $go_to_carzina = "🛒 Savatchaga o`tish";
        $text_order = "Savatchadagi Buyurtmalaringiz!";
        $all_summ_text = "💰 Ummumiy hisob: ";
        $clear_basket = "🗑 Savatchani tozalash";
        $buy_orders = "Sotib Olish 💴";
        $your_orders_deleted = "Sizning savatchangiz tozalandi!";
        $take_him = "O`zim olib ketaman 🏃🏻‍♂️";
        $delivery = "🚚 Yetkazib berish"; 
        $choose_order_type = "<b>Buyurtmani yetkazib berish turi: </b>";
        $choose_pay_type = "To'lov turini tanlang! 💳";
        $give_addres_text = "Buyurtmani yetkazib berish manzilini kiriting!\nMasalan: <i>(Chorsu Qoratosh ko`cha 12a uy)</i> ";
        $my_name = "Mening Ismim 👤";
        $my_number = "Mening Raqamim 📞";
        $my_informations = "Mening Ma'lumotlarim ℹ️";
        $next_text = "Keyingisi ➡️ ";
        $category_product = 'Mahsulotlar Kategoriyasi! 📦';
        $lookPictures = "To`liq Rasmlar 🖼";
        $error_number = "Telefon raqam noto`gri kiritldi!\nIltimos telefon raqamingizni to`gri kiritng!\nMasalan: (+998997772211)";
        $empty_word = "Hozircha ma'lumotlar yo`q! 😔";
        $payme = "Payme";
        $click = "Click";
        $change_language = "🇺🇿 🔄 🇷🇺 Tilni o`zgartitish";        
        $your_name = "👤 Sizning Ismingiz";
        $your_phone_number = "📞 Sizning Telefon raqamingiz";
        $your_gender = "📌 Sizning Jinsingiz";
        $your_old = "1️⃣  Siznig yoshingiz";
        $my_gender = "📌 Mening Jinsim";
        $my_old = "1️⃣ Mening Yoshim";
        $man = "🧔🏻 Erkak";
        $woman = "👩🏻‍🦰 Ayol";
        $choose_region = "Viloyatlardan birini tanlang!";
        $send_your_location = "Yetkazib berish manzilini lakatsiya orqali jo`nating!";
        $checked = "Tasdiqlash ✅";
        $cash = "Naqd";
        $get_order_text = "Buyurtmangiz muvaffaqiyatli qabul qilndi.Tez orada siz bilan bog`lanishadi!";
        $nextText = " Keyingisi 🔜";
        $prevText = "🔙 Oldingi";
    } else if ($step_1 == 2) {
        $next_text = "Далее ➡️";
        $menuText = "🏡 Главное меню";
        $back_text = "⬅️ Назад";
        $product_catalog = "Каталог товаров 🧳";
        $karzinka = "Корзина 🛒";
        $about_us = "О нас ℹ️";
        $my_orders = "Мои заказы 🛍";
        $about_my = "Моя информация 👤";
        $feedback = "Обратная Связь 📲";
        $cancel_text = "❌ Отмена";
        $summText = "сум";
        $take_order = "✅ Подтвердите заказ";
        $back = "⬅️ Назад";
        $karzina = "✅ Заказать";
        $go_to_carzina = "🛒 В корзину";
        $text_order = "Ваша корзина заказов!";
        $all_summ_text = "💰 Общий счет :";
        $clear_basket = "🗑 Очистит корзину";
        $buy_orders = "Купить 💴";
        $your_orders_deleted = "Ваша корзина очищена!";
        $take_him = "Я сам возьму 🏃🏻‍♂️";
        $delivery = "🚚  Доставка";
        $choose_order_type = "<b>Тип доставка заказа: </b>";
        $choose_pay_type = "Выберите способ оплаты! 💳";
        $give_addres_text = "Введите адрес доставки вашего заказа!\nНапример: <i>(улица Чорсу Караташ, 12а)</i>";
        $my_name = " Моё имя 👤";
        $my_number = "Мой номер 📞";
        $my_informations = "Мои данные ℹ️";
        $category_product = 'Категория продукта! 📦';
        $lookPictures = "Все изображения 🖼";
        $error_number = "Номер телефона введен неправильно!\nПожалуйста, введите свой номер телефона правильно!\nНапример: (+998997772211)";
        $empty_word = "Данных пока нет 😔";
        $payme = "Payme";
        $click = "Click";
        $change_language = "🇺🇿 🔄 🇷🇺 Изменить язык";
        $your_name = "👤 Ваше имя";
        $your_phone_number = "📞 Ваше номер телефона";
        $your_gender = "📌 Ваш пол";
        $your_old = "1️⃣  Ваш возраст";
        $my_gender = "📌 Мой пол";
        $my_old = "1️⃣ Мой возраст";
        $man = "🧔🏻 Мужчина";
        $woman = "👩🏻‍🦰 Женщина";
        $choose_region = "Выбери один из регионов!";
        $send_your_location = "Отправьте адрес доставки до лактации!";
        $checked = "Tasdiqlash ✅";
        $cash = "Наличные";
        $get_order_text = "Ваш заказ был успешно принят. Мы свяжемся с вами в ближайшее время!";
        $nextText = "Следущий 🔜";
        $prevText = "🔙 Предыдущий";
    }

    $back_to_menu = json_encode([
        'inline_keyboard' =>[
            [
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ],

                
            ],
        ]
    ]);

    $back_btn = json_encode([
        'inline_keyboard' =>[
            [
                [
                    'callback_data' => "back",
                    'text'=> $back_text
                ]
                
            ],
        ]
    ]);

    $menu_btn = json_encode([
        'keyboard' =>[
            [
                [
                    "text" => $product_catalog
                ],
                
                
            ],
            [
                [
                    "text" => $about_my,
                ],
                [
                    "text" => $my_orders
                ]
                
            ],
            [
                [
                    "text" => $change_language,
                ],
                [
                    "text" => $about_us
                ]
                
            ],
            [
                [
                    "text" => $karzinka
                ]
            ],
        ],
        "resize_keyboard" => true
    ]);

    $buy_type = json_encode([
        'inline_keyboard' => [
            [
                [
                    "callback_data" => "delivery","text" => $delivery
                ],
            ],
            [
                [
                    'callback_data' => "back",
                    'text'=> $back_text
                ],
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ]
                
            ],
        ]
    ]);

    $back_keyboard = json_encode([
        'keyboard' => [
            [$back_text]
        ],
        'resize_keyboard' => true
    ]);

    $menu_keyboard = json_encode([
        'keyboard' => [
            [$menuText]
        ],
        'resize_keyboard' => true
    ]);

    $menu_back_keyboard = json_encode([
        'keyboard' => [
            [
                ["text" => $back_text],
                ["text" => $menuText]
            ],
        ],
        'resize_keyboard' => true
    ]);

    $about_me_btn = json_encode([
        'inline_keyboard' =>[
            [
                [
                    "callback_data" => "my_name","text" => $my_name
                ],
                [
                    "callback_data" => "my_number","text" => $my_number
                ],
                
            ],
            [
                [
                    "callback_data" => "my_gender","text" => $my_gender
                ],
                [
                    "callback_data" => "my_old","text" => $my_old
                ],
                
            ],
            [
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ]
            ]
            
        ]
    ]);

    $gender_btn = json_encode([
        'inline_keyboard' =>[
            [
                [
                    "callback_data" => "man","text" => $man
                ],
                [
                    "callback_data" => "woman","text" => $woman
                ],
                
            ],
            [
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ]
            ]
            
        ]
    ]);

    $btn_check = json_encode([
        'inline_keyboard' =>[
            [
                [
                    "callback_data" => "checked","text" => $checked
                ],
                [
                    "callback_data" => "cancel","text" => $cancel_text
                ],
                
            ],
            [
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ]
            ]
            
        ]
    ]);

    $btn_payment_providers = json_encode([
        'inline_keyboard' =>[
            [
                [
                    "callback_data" => "click","text" => $click
                ],
                [
                    "callback_data" => "payme","text" => $payme
                ],
                [
                    "callback_data" => "cash","text" => $cash
                ],
                
            ],
            [
                [
                    'callback_data' => "menu",
                    'text'=> $menuText
                ]
            ]
            
        ]
    ]);

    // GET PHONE NUMBER FROM USERS  

    if (($step_1 == 1 OR $step_1 == 2) AND $step_2 == 0 ) {
        if (isset($message->contact)) {
            $contact = $message->contact;
            $phone_number = $contact->phone_number;
            $phone_number = str_replace("+", "", $phone_number);
            $phone_number = base64_encode($phone_number);

            deleteMessage($chat_id,$message_id);

            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            } 
            deleteLogMessage($chat_id,$conn);

            $sql = "UPDATE clients SET phone_number = '".$phone_number."' WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result == TRUE) {
                if ($step_1 == 1)  {
                    $text_send = "Iltimos <b>Ism Famliyangiz</b>ni qoldiring!";
                } else if ($step_1 == 2) {
                    $text_send = "Пожалуйста, оставьте свои <b>имя</b> и <b>фамилию</b>!";
                }
                $get_result = sendMessage($chat_id,$text_send);

                updateStep(1,$chat_id,$conn);
                
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            }

        }
    }

    // GET FULL NAME FROM USERS

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 1 and $text != "/start") {
        if (isset($text)) {
            deleteMessage($chat_id,$message_id);

            $full_name = base64_encode($text);
            $sql = " UPDATE clients SET status = 1, full_name = '".$full_name."' WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            deleteLogMessage($chat_id,$conn);
            if ($result == TRUE) {
                if ($step_1 == 1) {
                    $text_send = "Bizning menu!";
                } else if ($step_1 == 2) {
                    $text_send = "Наш меню!";
                }
                $get_result = sendMessageBtn($chat_id,$text_send,$menu_btn);

                updateStep(2,$chat_id,$conn);
                
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            }
        }
    }

    // menu

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 2 and $text != "/start") {
        if (isset($update->message)) {
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            deleteLogMessage($chat_id,$conn);
            if ($text == $product_catalog) {
                deleteMessage($chat_id,$message_id);
                if ($step_1 == 1) {
                    $our_catalog = "Bizning Katalogdagi Tovarlar 🔥";
                } else if ($step_1 == 2) {
                    $our_catalog = "Товары в нашем каталоге 🔥";
                }
                $catalog_btn = getCatalog($conn,$step_1);
                sendMessageBtn($chat_id,$our_catalog,$catalog_btn);
                updateStep(3,$chat_id,$conn);
            } else if ($text == $about_us) {
                deleteMessage($chat_id,$message_id);
                $sql = "SELECT * FROM about_us";
                $result = $conn->query($sql);
                $num_rows = $result->num_rows;
                if ($result->num_rows > 0) {
                    $caption = '';
                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                            $description = $row["description_uz"];
                        } else if ($step_1 == 2) {
                            $title = $row["title_ru"];
                            $description = $row["description_ru"];
                        }
                        $caption = "<b>".$title."</b>\n\n<code>".$description."</code>";
                        switch ($row["type"]) {
                            case 'photo':
                                $img = $row["file_id"];
                                if ($num_rows != $i){ 
                                    $get_result = sendPhoto($chat_id,$img,$caption);
                                    $delete_message_id = $get_result->result->message_id;
                                    insertLogMessage($chat_id,$delete_message_id,$conn);
                                } else if ($num_rows == $i) {
                                    sendPhotoBtn($chat_id,$img,$caption,$back_to_menu);
                                }
                                break;
                        }
                        $i++;
                    }
                }
            } else if ($text == $about_my) {
                $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $full_name = '';
                    if (isset($row["full_name"]) && !empty($row["full_name"])) {
                        $full_name = base64_decode($row["full_name"]);
                    } 
                    $old = "🚫";
                    if (isset($row["old"]) && !empty($row["old"])) {

                        $old = $row["old"];
                    }
                    $gender = "🚫";
                    if (isset($row["gender"]) && !empty($row["gender"])) {
                        if ($row["gender"] == 1) {
                            $gender = $man;
                        }  else if ($row["gender"] == 2) {
                            $gender = $woman;
                        }
                    }
                    $phone_number = base64_decode($row["phone_number"]);
                    $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

                }
                deleteMessage($chat_id,$message_id);
                sendMessageBtn($chat_id,$my_informations,$about_me_btn);
                updateStep(20,$chat_id,$conn);
            } else if ($text == $karzinka) {
                deleteMessage($chat_id,$message_id);
                $sql = "
                    SELECT 
                        o.id as order_id,
                        o.count as order_count,
                        p.id as product_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        p.price as product_price
                    FROM orders as o    
                    INNER JOIN products as p on o.product_id = p.id 
                    WHERE o.chat_id = ".$chat_id." ORDER BY o.id ASC
                ";
                $result = $conn->query($sql);
                if ($result->num_rows > 0 ){
                    $row_btn = [];
                    $btns = [];
                    $i = 0;
                    $txt = '';
                    $txt = $txt.$text_order;
                    $txt = $txt."\n\n<b>----------------------------------------</b>\n\n";
                    $umumiy_hisob = 0;
                    while ($row = $result->fetch_assoc()){
                        if ($step_1 == 1) {
                            $product_name = $row["product_name_uz"];
                        } else  if ($step_1 == 2) {
                            $product_name = $row["product_name_ru"];
                        }
                        $order_id = $row["order_id"];
                        $product_id = $row["product_id"];
                        $product_price = $row["product_price"];
                        $order_count = $row["order_count"];
                        $clb_order = $order_id."_order"; 


                        $txt = $txt."<b>".$product_name."</b>\n\n";
                        $all_summ = $order_count * $product_price;

                        $umumiy_hisob = $umumiy_hisob + $all_summ;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;
                        
                        $product_price = number_format("$product_price",0," "," ");
                        $product_price = $product_price." ".$summText;

                        $txt = $txt."<b>".$product_price." ✖️ ".$order_count." = ".$all_summ."</b>\n\n";

                        if ($i == 0) {
                            $btns[] =  ["callback_data" => $clb_order , "text" => "👉 ".$product_name." 👈"];
                            $row_btn[] = $btns;
                            $btns = [];
                            $btns[] = ["callback_data" => "minus_".$order_id, "text" => "-1"];
                            $btns[] = ["callback_data" => "plus_".$order_id, "text" => "+1"];
                            $row_btn[] = $btns;
                            $btns = [];
                        } else {
                            $btns[] =  ["callback_data" => $clb_order , "text" => $product_name];
                        }

                        if ($i % 2 == 0){
                            $row_btn[] = $btns;
                            $btns = [];
                        }
                        $i++;
                    }
                    if (count($btns) == 1) {
                        $row_btn[] = $btns;
                    }
                    $row_btn[] = [
                        ["callback_data" => "clear_basket" , "text" => $clear_basket],
                        ["callback_data" => "buy_orders","text" => $buy_orders]
                    ];
                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];
                    $btn_details = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                    $umumiy_hisob = number_format("$umumiy_hisob",0," "," ");
                    $umumiy_hisob = $umumiy_hisob." ".$summText;
                    $txt = $txt."<b>".$all_summ_text.$umumiy_hisob."</b>\n\n";
                    $txt = $txt."<b>----------------------------------------</b>";
                    sendMessageBtn($chat_id,$txt,$btn_details);
                } else {
                    deleteMessage($chat_id,$message_id);
                    if ($step_1 == 1) {
                        $txt_emtpy = "😔 Sizning savatchangiz hozircha bo`sh!";
                    } else if ($step_1 == 2) {
                        $txt_emtpy = "😔 Ваша корзина сейчас пуста!";
                    }
                    sendPhotoBtn($chat_id,$empty,$txt_emtpy,$back_to_menu);                     
                }
                updateStep(8,$chat_id,$conn);
            } else if ($text == $my_orders) {
                deleteMessage($chat_id,$message->message_id);
                $sql = "
                    SELECT 
                        oc.id,
                        oc.order_code,
                        p.title_uz, 
                        p.title_ru,
                        ao.sell_price,
                        ao.count,
                        p.id as product_id,
                        p.price  ,
                        oc.created_date,
                        oc.pay

                    FROM clients as c
                    INNER JOIN order_code AS oc on c.id = oc.client_id
                    INNER JOIN arrive_orders as ao on oc.order_code = ao.order_code_id
                    INNER JOIN order_details as od on ao.order_id = od.order_id
                    inner join details as d on od.details_id = d.id
                    inner join multi_details as md on od.multi_details_id = md.id
                    inner join products as p on ao.product_id = p.id
                    WHERE oc.billing_status > 3 and c.chat_id = ".$chat_id;
                $result = $conn->query($sql);
                
                if ($result->num_rows > 0) {

                    foreach ($result as $key => $value) {
                       $arr[$value["id"]]["order_code"] = $value["order_code"];
                       $arr[$value["id"]]["created_date"] = $value["created_date"];
                       $arr[$value["id"]]["pay"] = $value["pay"];
                       $arr[$value["id"]]["products"][$value["product_id"]]["title_uz"] = $value["title_uz"];
                       $arr[$value["id"]]["products"][$value["product_id"]]["title_ru"] = $value["title_ru"];
                       $arr[$value["id"]]["products"][$value["product_id"]]["sell_price"] = $value["sell_price"];
                       $arr[$value["id"]]["products"][$value["product_id"]]["count"] = $value["count"];
                       $arr[$value["id"]]["products"][$value["product_id"]]["price"] = $value["price"];
                    }
                    $txt = "---------------------------------------------------------- \n\n";
                    foreach ($arr as $key => $value) {
                        $dt = date("Y-m-d H:i",$value["created_date"]);
                        if ($step_1 == 1) {
                            $txt = $txt."<b> Buyurtma raqami: #".$value["order_code"]."</b> \n\n";
                            $date_text = "🕧 Buyurtma vaqti:".$dt."\n💰 To`lov turi:";
                            $all_summ_text = "💴 Ummumiy to`lov:";
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b> Номер заказа: #".$value["order_code"]."</b> \n\n";
                            $date_text = "🕧 Время заказа:".$dt."\n💰 Тип платежа:";
                            $all_summ_text = "💴 Ummumiy to`lov:";
                        }
                        $all_summ = 0;
                        foreach ($value["products"] as $key2 => $value2) {
                            if ($step_1 == 1) {
                                $txt = $txt."<b>".$value2["title_uz"]."</b>";
                            } else if ($step_1 == 2) {
                                $txt = $txt."<b>".$value2["title_ru"]."</b>";
                            }
                            $price = $value2["price"]." ".$summText;
                            $sell_price = $value2["sell_price"]." ".$summText;
                            $txt = $txt."\n".$value2["count"] . " ✖️ ".$price." = ".$sell_price."\n";
                            $all_summ = $all_summ + $value2["sell_price"];
                        }
                        if ($value["pay"] == 1){
                            $txt = $txt.PHP_EOL.$date_text." Payme";
                        } else if ($value["pay"] == 2) {
                            $txt = $txt.PHP_EOL.$date_text." Click";    
                        }

                        $txt = $txt.PHP_EOL.$all_summ_text." ".$all_summ." ".$summText;
                        $txt = $txt."\n\n".'----------------------------------------------------------'."\n\n";
                    }
                    sendMessageBtn($chat_id,$txt,$back_to_menu);
                } else {
                    if ($step_1 == 1) {
                        $txt = "Sizning faol buyurtmangiz yo`q";
                    } else if ($step_1 == 2) {
                        $txt = "У вас нет активного заказа";
                    }
                    // deleteMessage($chat_id,$message_id);

                    sendMessageBtn($chat_id,$txt,$back_to_menu);
                }
            } else if ($text == $feedback) {
                $sql = "SELECT * FROM contact";
                $result = $conn->query($sql);
                if($result->num_rows > 0) {
                    deleteMessage($chat_id,$message_id);
                    $row = $result->fetch_assoc();
                    $phone_number = $row["phone_number"];
                    $txt = '';
                    if ($step_1 == 1) {
                        $our_phone_number = '<b>📞  Bizning <i>Telefon Raqam</i></b>: ';
                        $our_email = "<b>📧 Bizning <i>Elektron Pochta</i></b>: ";
                        $description = $row["description_uz"];
                    } else if ($step_1 == 2) {
                        $our_phone_number = '<b>📞  Наш <i>номер телефона</i></b>: ';
                        $description = $row["description_ru"];
                        $our_email = "<b>📧 Наша <i>электронная почта</i></b>: ";
                    }
                    $txt = $txt."<code>".$description."</code>\n\n";
                    $txt = $txt.$our_phone_number.PHP_EOL."☎️".$phone_number;
                    if (isset($row["phone_number1"]) and !empty($row["phone_number1"])) {
                        $phone_number1 = $row["phone_number1"];
                        $txt = $txt."\n☎️".$phone_number1;
                    }
                    if (isset($row["phone_number2"]) and !empty($row["phone_number2"])) {
                        $phone_number2 = $row["phone_number2"];
                        $txt = $txt."\n☎️".$phone_number2;
                    }
                    if (isset($row["phone_number3"]) and !empty($row["phone_number3"])) {
                        $phone_number3 = $row["phone_number3"];
                        $txt = $txt."\n☎️".$phone_number3;
                    }
                    $email = $row["email"];
                    $txt = $txt."\n\n ".$our_email.$email;
                    sendMessageBtn($chat_id,$txt,$back_to_menu);
                }
            }
        } 
    }

    // category

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 3 and $text != "/start") {
        if (isset($update->callback_query)) {
            $explode_data = explode("_",$data);
            $category = $explode_data[0];
            $category_id = $explode_data[1];
            if ($category == "category") {
                $sql = "SELECT * FROM sub_category where status = 1 and category_id = ".$category_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row_btn = [];
                    $btn = [];
                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        $clb_data = "sub-category_".$row["id"];
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_1 == 2) {
                            $title = $row["title_ru"];
                        }
                        $btn[] = ["callback_data" => $clb_data,"text" => $title];
                        if ($i%2 == 0) {
                            $row_btn[] = $btn;
                            $btn = [];
                        }

                        $i++;
                    }

                    if (count($btn) == 1) {
                        $row_btn[] = $btn;
                    }

                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                    $btn_menu = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                   sendMessageBtn($chat_id,$category_product,$btn_menu);
                } else {
                    sendMessageBtn($chat_id,$empty_word,$back_btn);
                    updateStep(5,$chat_id,$conn);
                }
                deleteMessage($chat_id,$message_id);
                updateStep(4,$chat_id,$conn);
            }
            deleteLogMessage($chat_id,$conn);
        }
    } 

    // sub category

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 4 and $text != "/start") {
        $explode_data = explode("_",$data);
        $sub_category = $explode_data[0];
        deleteMessage($chat_id,$message_id);
        if ($sub_category == "sub-category") {
            $id = $explode_data[1];
            $sql = "select * from sub_category where sub_category_id = ".$id;
            $result_sub_category = $conn->query($sql);
            if ($result_sub_category->num_rows > 0) {
                $row_btn = [];
                $btn = [];
                $i = 1;
                while ($row = $result_sub_category->fetch_assoc()) {
                    $clb_data = "sub-category_".$row["id"];
                    if ($step_1 == 1) {
                        $title = $row["title_uz"];
                    } else if ($step_1 == 2) {
                        $title = $row["title_ru"];
                    }
                    $btn[] = ["callback_data" => $clb_data,"text" => $title];
                    if ($i%2 == 0) {
                        $row_btn[] = $btn;
                        $btn = [];
                    }

                    $i++;
                }

                if (count($btn) == 1) {
                    $row_btn[] = $btn;
                }

                $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                $btn_menu = json_encode([
                    "inline_keyboard" => $row_btn,
                ]);
               sendMessageBtn($chat_id,$category_product,$btn_menu);
            } else {
                $sql = '
                SELECT * FROM (
                    SELECT 
                        p.id as product_id,
                        d.id as details_id,
                        md.id as multi_details_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        d.title_uz as datail_name_uz,
                        d.title_ru as datail_name_ru,
                        p.description_uz as description_uz,
                        p.description_ru as description_ru,
                        md.title_uz as multi_details_name_uz,
                        md.title_ru as multi_details_name_ru,
                        p.price as price,
                        p.img as img
                    FROM products as p
                    LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                    LEFT JOIN details AS d ON pd.details_id = d.id
                    LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where p.sub_category_id ='.$id.'
                ) AS t limit 10';  
                // sendMessage($chat_id,$sql);
                // die();
                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    foreach ($result as $key => $value) {
                        $arr[$value['product_id']]['img'] = $value['img'];
                        $arr[$value['product_id']]['price'] = $value['price'];
                        $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                        $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                        $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                        }

                        $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                            $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                             $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                        }
                    }
                }
                $txt = '';
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        if ($step_1 == 1) {

                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        if ($i == 0) {
                            $prev_callback = $product_id."_prev";

                        }
                        if ($i == 9) {
                            $next_callback = $product_id."_next";
                            $row_btn_count[] = [['callback_data' => $next_callback,'text' => $nextText]];
                        }
                        $row_btn_count[] =  [
                            ['callback_data' => $id."_back",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 

                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                } else {
                    sendMessageBtn($chat_id,$empty_word,$back_btn);
                }
            }

            // sendMessage($chat_id,json_encode($arr));
        } else if ($data == "back") {
            deleteMessage($chat_id,$message_id);
            if ($step_1 == 1) {
                $our_catalog = "Bizning Katalogdagi Tovarlar 🔥";
            } else if ($step_1 == 2) {
                $our_catalog = "Товары в нашем каталоге 🔥";
            }
            $catalog_btn = getCatalog($conn,$step_1);
            sendMessageBtn($chat_id,$our_catalog,$catalog_btn);
            updateStep(3,$chat_id,$conn);
        }
    }

    // buy product

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 5 and $text != "/start") {
        if (isset($update->callback_query)) {
            $explode_data = explode("_", $data);
            $key_word = $explode_data[1]; 
            $count_product = $explode_data[0]; 
            $id_product = $explode_data[2]; 
            if ($key_word == "back") {
                deleteMessage($chat_id,$message_id);

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                deleteLogMessage($chat_id,$conn);

                $sql = "select * from sub_category as sc inner join sub_category as sct on sc.category_id = sct.category_id where sc.id = ".$count_product;
                // sendMessage($chat_id,$sql);
                // die();
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row_btn = [];
                    $btn = [];
                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        $clb_data = "sub-category_".$row["id"];
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_1 == 2) {
                            $title = $row["title_ru"];
                        }
                        $btn[] = ["callback_data" => $clb_data,"text" => $title];
                        if ($i%2 == 0) {
                            $row_btn[] = $btn;
                            $btn = [];
                        }

                        $i++;
                    }

                    if (count($btn) == 1) {
                        $row_btn[] = $btn;
                    }

                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                    $btn_menu = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                }
                sendMessageBtn($chat_id,$category_product,$btn_menu);
                updateStep(4,$chat_id,$conn);
            } else if ($data == "_karzina_".$id_product) {
                $sql = "SELECT * FROM log_messages WHERE message_id != ".$message_id." and chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                $sql = "
                    select 
                        d.id as details_id,
                        d.title_uz as details_name_uz,
                        d.title_ru as details_name_ru,
                        md.id as multi_detail_id,
                        md.title_uz as multi_detail_name_uz,
                        md.title_ru as multi_detail_name_ru,
                        p.id as product_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        p.price as price
                    from product_details_ as pd
                    INNER JOIN products as p ON pd.product_id = p.id
                    INNER JOIN details as d on pd.details_id = d.id 
                    INNER JOIN multi_details as md on pd.multi_details_id = md.id
                    WHERE pd.product_id = ".$id_product." order by pd.id asc
                ";
                $result = $conn->query($sql);

                $arr = [];
                $has_array = [];
                $i = 0;
                if ($result->num_rows > 0) {
                    if (isset($result) and !empty($result) and $result->num_rows > 0) {
                        foreach ($result as $key => $value) {
                    
                            $arr[$value["details_id"]]["id_details"] = $i;
                            $arr[$value["details_id"]]["price"] = $value["price"];
                            $arr[$value["details_id"]]["product_id"] = $value["product_id"];
                            $arr[$value["details_id"]]["product_name_uz"] = $value["product_name_uz"];
                            $arr[$value["details_id"]]["product_name_ru"] = $value["product_name_ru"];
                            $arr[$value["details_id"]]["detail_name_uz"] = $value["details_name_uz"];
                            $arr[$value["details_id"]]["detail_name_ru"] = $value["details_name_ru"];
                            $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_uz"] = $value["multi_detail_name_uz"];
                            $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_ru"] = $value["multi_detail_name_ru"];
                            $count_multi_detail_id = count($arr[$value["details_id"]]["multi_detail_id"]);


                            if ($count_multi_detail_id == 1){
                                $i++;
                            } 
                        }
                    }
                    if (isset($arr) and !empty($arr)) {

                        foreach ($arr as $key => $value) {
                            $detail_id = $key;
                            // sendMessage($chat_id,$value["id_details"]);
                            if ($value["id_details"] == 1) {
                                $id_details = $value["id_details"];
                                $price = $value["price"];
                                $id_product = $value["product_id"];
                                if ($step_1 == 1) {
                                    $product_name = $value["product_name_uz"];
                                    $detail_name = $value["detail_name_uz"];
                                } else if ($step_1 == 2){
                                    $product_name = $value["product_name_ru"];
                                    $detail_name = $value["detail_name_ru"];
                                }
                                $row_btn = [];
                                $btn = [];
                                $i = 1;
                                foreach ($value["multi_detail_id"] as $keyTwo => $valueTwo) {
                                    $clb_data = $detail_id."_".$keyTwo."_detail_".$id_product."_".$id_details;
                                    if ($step_1 == 1) {
                                        $title = $valueTwo["multi_detail_name_uz"];
                                        $cancel_text = "❌ Bekor qilish";
                                        $selecting = "Kutlimoqda... ";
                                        $text_select = "<i>Quyidagi <b>".$detail_name."</b>lardan birini tanlang!</i>";
                                    } else if ($step_1 == 2) {
                                        $title = $valueTwo["multi_detail_name_ru"];
                                        $cancel_text = "❌ Отмена";
                                        $selecting = "Ожидающий ... ";
                                        $text_select = "<i>Выберите один из следующих <b>".$detail_name."</b>!</i>";
                                    }
                                    $btn[] = ["callback_data" => $clb_data,"text" => $title];
                                    if ($i%4 == 0) {
                                        $row_btn[] = $btn;
                                        $btn = [];
                                    }
                                    $i++;

                                }
                                if (count($btn) < 4) {
                                    $row_btn[] = $btn;
                                }
                                $clb_cancel = "cancel_".$id_product;
                                $row_btn[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text]];
                                $row_btn[] = [["callback_data" => $id_product."_back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                                $btn_details = json_encode([
                                    "inline_keyboard" => $row_btn,
                                ]);
                                $price = number_format("$price",0," "," ");
                                $price = $price." ".$summText;

                                $caption_text = "<b>".$product_name."</b>\n"."<b><i>".$price."</i></b>\n\n"."<b>".$detail_name."</b>: <i>".$selecting."🔎</i>\n\n".$text_select;
                                // sendMessage($chat_id,$caption_text.$btn_details);
                                // die();
                                editMessageCaption($chat_id,$message_id,$caption_text,$btn_details);
                                updateStep(6,$chat_id,$conn);
                                die();
                            }
                        }
                    }
                } else {
                    $sql = "SELECT * FROM orders where status = 0 and product_id = ".$id_product." and chat_id = ".$chat_id;
                    $result_sql = $conn->query($sql);
                    if (!$result_sql->num_rows > 0) {
                        $insert = "INSERT INTO orders (chat_id,product_id,create_date,status) VALUES (".$chat_id.",".$id_product.",'".$current_date."',0)";
                        $result = $conn->query($insert);
                        $last_id = $conn->insert_id;
                    } else {
                        $row = $result_sql->fetch_assoc();
                        $last_id = $row["id"];
                    }

                    $sql_two =  "select * from products where id = ".$id_product;
                    $result_two = $conn->query($sql_two);
                    if ($result_two->num_rows > 0) {
                        $row_two = $result_two->fetch_assoc();
                        $img = $row_two["img"];
                        $price = $row_two["price"];
                        if ($step_1 == 1) {
                            $product_name = $row_two["title_uz"];
                            $product_name = $row_two["description_uz"];

                        } else if ($step_1 == 2) {
                            $product_name = $row_two["title_ru"];
                            $product_name = $row_two["description_ru"];
                        }

                        $row_btn_count = [];
                        $btn_column =[];
                        for ($i=1; $i < 10; $i++) {
                            $clb_data_count_btn = $i."_buysecond_".$id_product;
                            $text_btn = "+".$i;
                            $btn_column[] = ["callback_data" => $clb_data_count_btn,"text" => $text_btn];
                            if ($i % 3 == 0) {
                                $row_btn_count[] = $btn_column;
                                $btn_column = [];
                            }
                        }
                        $clb_cancel = "cancel_".$id_product;
                        $clb_confirm = "confirm_".$id_product;
                        $clb_back = $id_product."_back";

                        $row_btn_count[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text],["callback_data" => 'karzina',"text" =>$go_to_carzina]];
                        $row_btn_count[] = [["callback_data" => $clb_back,"text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                        $btn_count = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]);

                        updateStep(7,$chat_id,$conn);


                        $caption_text = "<b>".$product_name."</b>\n\n<code>".$description."</code>";
                        editMessageCaption($chat_id,$message_id,$caption_text,$btn_count);
                    } 
                }
            } else if($explode_data[1] == "pictures") {
                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }

                $sql = "SELECT * FROM product_img WHERE product_id = ".$explode_data[0];
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $media = [];
                    while ($row = $result->fetch_assoc()) {
                        $photo[] = ["type" => "photo","media" => $row["img"]];
                    }
                }
                $get_result = bot("sendMediaGroup",[
                    'chat_id' => $chat_id,
                    'media' => json_encode($photo)
                ]);
                foreach ($get_result->result as $key => $value) {
                    $delete_message_id =  $get_result->result[$key]->message_id;
                    insertLogMessage($chat_id,$delete_message_id,$conn);
                }

                $sql = '
                    SELECT 
                        p.id as product_id,
                        d.id as details_id,
                        md.id as multi_details_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        d.title_uz as datail_name_uz,
                        d.title_ru as datail_name_ru,
                        p.description_uz as description_uz,
                        p.description_ru as description_ru,
                        md.title_uz as multi_details_name_uz,
                        md.title_ru as multi_details_name_ru,
                        p.price as price,
                        p.img as img,
                        p.sub_category_id as sub_id
                    FROM products as p
                    LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                    LEFT JOIN details AS d ON pd.details_id = d.id
                    LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where p.id =
                '.$explode_data[0];  
                // sendMessage($chat_id,$sql);
                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    foreach ($result as $key => $value) {
                        $arr[$value['product_id']]['img'] = $value['img'];
                        $arr[$value['product_id']]['price'] = $value['price'];
                        $arr[$value['product_id']]['sub_id'] = $value['sub_id'];
                        $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                        $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                        $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                        }

                        $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                            $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                             $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                        }
                    }
                }
                $txt = '';
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        if ($step_1 == 1) {
                            $sub_id = $value["sub_id"];
                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        $row_btn_count[] =  [
                            ['callback_data' => $product_id."_back-product",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 


                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                }
            } else if ($key_word == "back-product") {
                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                deleteLogMessage($chat_id,$conn);
                $sql = '
                    SELECT 
                        p.id as product_id,
                        d.id as details_id,
                        md.id as multi_details_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        d.title_uz as datail_name_uz,
                        d.title_ru as datail_name_ru,
                        p.description_uz as description_uz,
                        p.description_ru as description_ru,
                        md.title_uz as multi_details_name_uz,
                        md.title_ru as multi_details_name_ru,
                        p.price as price,
                        p.img as img,
                        p.sub_category_id as sub_id
                    FROM products as p
                    INNER JOIN products AS pr ON pr.sub_category_id = p.sub_category_id
                    LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                    LEFT JOIN details AS d ON pd.details_id = d.id
                    LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where pr.id = 
                '.$explode_data[0];  
                // sendMessage($chat_id,$sql);
                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    foreach ($result as $key => $value) {
                        $arr[$value['product_id']]['img'] = $value['img'];
                        $arr[$value['product_id']]['sub_id'] = $value['sub_id'];
                        $arr[$value['product_id']]['price'] = $value['price'];
                        $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                        $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                        $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                        }

                        $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                            $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                             $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                        }
                    }
                }
                $txt = '';
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        $sub_id = $value["sub_id"];
                        if ($step_1 == 1) {
                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        $row_btn_count[] =  [
                            ['callback_data' => $sub_id."_back",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 


                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                }
            } else if ($key_word == "next") {

                deleteMessage($chat_id,$message_id);

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                $sql = "
                    SELECT 
                      * 
                    FROM 
                      (
                        SELECT 
                          p.id as product_id, 
                          d.id as details_id, 
                          md.id as multi_details_id, 
                          p.title_uz as product_name_uz, 
                          p.title_ru as product_name_ru, 
                          d.title_uz as datail_name_uz, 
                          d.title_ru as datail_name_ru, 
                          p.description_uz as description_uz, 
                          p.description_ru as description_ru, 
                          md.title_uz as multi_details_name_uz, 
                          md.title_ru as multi_details_name_ru, 
                          p.price as price, 
                          p.img as img, 
                          p.sub_category_id 
                        FROM 
                          products as p 
                          INNER JOIN products AS prd ON p.sub_category_id = prd.sub_category_id  and prd.id = ".$count_product."
                          LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                          LEFT JOIN details AS d ON pd.details_id = d.id 
                          LEFT JOIN multi_details as md ON pd.multi_details_id = md.id 
                        where 
                          p.id > ".$count_product."
                      ) AS t
                    limit 
                      11
                ";

                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    $b = 1;
                    foreach ($result as $key => $value) {
                        if ($b < 11) {
                            $arr[$value['product_id']]['img'] = $value['img'];
                            $arr[$value['product_id']]['price'] = $value['price'];
                            $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                            $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                            $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                            }

                            $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                                $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                                 $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                            }
                        }
                        $b++;
                    }
                }
                $txt = '';
                $count_arr = count($arr);
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        if ($step_1 == 1) {

                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        if ($i == 0) {
                            $prev_callback = $product_id."_prev";

                        }
                        if ($i == $count_arr - 1) {
                            $next_callback = $product_id."_next";
                            if ($result->num_rows > 10) {
                                $row_btn_count[] = [['callback_data' => $prev_callback,'text' => $prevText],['callback_data' => $next_callback,'text' => $nextText]];
                            }  else {
                                $row_btn_count[] = [['callback_data' => $prev_callback,'text' => $prevText]];

                            }
                        }
                        $row_btn_count[] =  [
                            ['callback_data' => $id."_back",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 


                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                } else {
                    sendMessageBtn($chat_id,$empty_word,$back_btn);
                }
            } else if ($key_word == "prev") {

                deleteMessage($chat_id,$message_id);

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                $sql = "
                    SELECT 
                      * 
                    FROM 
                      (
                        SELECT 
                          p.id as product_id, 
                          d.id as details_id, 
                          md.id as multi_details_id, 
                          p.title_uz as product_name_uz, 
                          p.title_ru as product_name_ru, 
                          d.title_uz as datail_name_uz, 
                          d.title_ru as datail_name_ru, 
                          p.description_uz as description_uz, 
                          p.description_ru as description_ru, 
                          md.title_uz as multi_details_name_uz, 
                          md.title_ru as multi_details_name_ru, 
                          p.price as price, 
                          p.img as img, 
                          p.sub_category_id 
                        FROM 
                          products as p 
                          INNER JOIN products AS prd ON p.sub_category_id = prd.sub_category_id  and prd.id = ".$count_product."
                          LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                          LEFT JOIN details AS d ON pd.details_id = d.id 
                          LEFT JOIN multi_details as md ON pd.multi_details_id = md.id 
                        where 
                          p.id < ".$count_product."
                      ) AS t
                    limit 
                      11
                ";
                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    $b = 1;
                    foreach ($result as $key => $value) {
                        if ($b < 11) {
                            $arr[$value['product_id']]['img'] = $value['img'];
                            $arr[$value['product_id']]['price'] = $value['price'];
                            $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                            $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                            $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                            }

                            $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                                $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                                 $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                            }
                        }
                        $b++;
                    }
                }
                $txt = '';
                $count_arr = count($arr);
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        if ($step_1 == 1) {

                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        if ($i == 0) {
                            $prev_callback = $product_id."_prev";

                        }
                        if ($i == $count_arr - 1) {
                            $next_callback = $product_id."_next";
                            if ($result->num_rows > 10) {
                                $row_btn_count[] = [['callback_data' => $prev_callback,'text' => $prevText],['callback_data' => $next_callback,'text' => $nextText]];
                            }  else {
                                $row_btn_count[] = [['callback_data' => $next_callback,'text' => $nextText]];

                            }
                        }
                        $row_btn_count[] =  [
                            ['callback_data' => $id."_back",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 


                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                } else {
                    sendMessageBtn($chat_id,$empty_word,$back_btn);
                }
            } 
        }
    }

    // choose details

    // sendMessage($chat_id,$data);

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 6 and $text != "/start") {
        if (isset($update->callback_query)) {
            $explode_data = explode("_", $data);
            $detail_id = $explode_data[0];
            $multi_detail_id = $explode_data[1];
            $key_word = $explode_data[2];
            $id_product = $explode_data[3];
            $id_detail = $explode_data[4];
            $current_date = date("Y-m-d H:i:s");
            if ($key_word == "detail") {

                $sql = "SELECT * FROM orders where status = 0 and product_id = ".$id_product." and chat_id = ".$chat_id;
                $result_sql = $conn->query($sql);
                if (!$result_sql->num_rows > 0) {
                    $insert = "INSERT INTO orders (chat_id,product_id,create_date,status) VALUES (".$chat_id.",".$id_product.",'".$current_date."',0)";
                    $result = $conn->query($insert);
                    $last_id = $conn->insert_id;
                } else {
                    $row = $result_sql->fetch_assoc();
                    $last_id = $row["id"];
                }

                if ($result == TRUE OR $result_sql->num_rows > 0) {
                    $insert_order_details = "INSERT INTO order_details (order_id, details_id, multi_details_id) VALUES (".$last_id.",".$detail_id.",".$multi_detail_id.")";
                    $result_order_result = $conn->query($insert_order_details);
                    if ($result_order_result == TRUE) {

                        $sql = "
                            SELECT 
                                d.id as details_id,
                                d.title_uz as details_name_uz,
                                d.title_ru as details_name_ru,
                                md.id as multi_detail_id,
                                md.title_uz as multi_detail_name_uz,
                                md.title_ru as multi_detail_name_ru,
                                p.id as product_id,
                                p.title_uz as product_name_uz,
                                p.title_ru as product_name_ru,
                                p.price as price,
                                od.details_id as order_details_id,
                                od.multi_details_id as order_multi_details_id
                            FROM product_details_ as pd 
                            INNER JOIN products as p ON pd.product_id = p.id 
                            INNER JOIN details as d on pd.details_id = d.id 
                            INNER JOIN orders as o on p.id = o.product_id and o.status = 0 and o.chat_id = ".$chat_id."
                            LEFT JOIN order_details as od on d.id = od.details_id and o.id = od.order_id
                            INNER JOIN multi_details as md on pd.multi_details_id = md.id 
                            WHERE pd.product_id = ".$id_product." order by pd.id asc
                        ";

                        $result = $conn->query($sql);
                        $arr = [];
                        $i = 0;
                        if (isset($result) and !empty($result) and $result->num_rows > 0) {
                            foreach ($result as $key => $value) {
                               $arr[$value["details_id"]]["id_details"] = $i;
                               $arr[$value["details_id"]]["price"] = $value["price"];
                               $arr[$value["details_id"]]["product_id"] = $value["product_id"];
                               $arr[$value["details_id"]]["product_name_uz"] = $value["product_name_uz"];
                               $arr[$value["details_id"]]["product_name_ru"] = $value["product_name_ru"];
                               $arr[$value["details_id"]]["detail_name_uz"] = $value["details_name_uz"];
                               $arr[$value["details_id"]]["detail_name_ru"] = $value["details_name_ru"];
                               $arr[$value["details_id"]]["order_details_id"] = $value["order_details_id"];
                               $arr[$value["details_id"]]["order_multi_details_id"] = $value["order_multi_details_id"];
                               $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_uz"] = $value["multi_detail_name_uz"];
                               $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_ru"] = $value["multi_detail_name_ru"];
                                $count_multi_detail_id = count($arr[$value["details_id"]]["multi_detail_id"]);


                                if ($count_multi_detail_id == 1){
                                    $i++;
                                }                            }
                        }
                        $details_txt = '';
                        $i = 0;
                        if (isset($arr) and !empty($arr)) {
                            foreach ($arr as $key => $value) {
                                $detail_id = $key;
                                if ($value["id_details"] > $id_detail and $value["id_details"] < ($id_detail + 2)) {
                                    // sendMessage($chat_id,'success 2');
                                    $id_details = $value["id_details"];
                                    $price = $value["price"];
                                    $id_product = $value["product_id"];
                                    if ($step_1 == 1) {
                                        $product_name = $value["product_name_uz"];
                                        $detail_name = $value["detail_name_uz"];
                                    } else if ($step_1 == 2){
                                        $product_name = $value["product_name_ru"];
                                        $detail_name = $value["detail_name_ru"];
                                    }
                                    $row_btn = [];
                                    $btn = [];
                                    $i = 1;
                                    foreach ($value["multi_detail_id"] as $keyTwo => $valueTwo) {
                                        $clb_data = $detail_id."_".$keyTwo."_detail_".$id_product."_".$id_details;
                                        if ($step_1 == 1) {
                                            $title = $valueTwo["multi_detail_name_uz"];
                                            $cancel_text = "❌ Bekor qilish";
                                            $selecting = "Kutlimoqda... ";
                                            $text_select = "<i>Quyidagi <b>".$detail_name."</b>lardan birini tanlang!</i>";
                                        } else if ($step_1 == 2) {
                                            $title = $valueTwo["multi_detail_name_ru"];
                                            $cancel_text = "❌ Отмена";
                                            $selecting = "Ожидающий ... ";
                                            $text_select = "<i>Выберите один из следующих <b>".$detail_name."</b>!</i>";
                                        }
                                        $btn[] = ["callback_data" => $clb_data,"text" => $title];
                                        if ($i%4 == 0) {
                                            $row_btn[] = $btn;
                                            $btn = [];
                                        }
                                        $i++;
                                    }
                                    if (count($btn) < 4) {
                                        $row_btn[] = $btn;
                                    }
                                    $clb_cancel = "cancel_".$id_product;
                                    $row_btn[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text]];
                                    $row_btn[] = [["callback_data" => $id_product."_back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                                    $btn_details = json_encode([
                                        "inline_keyboard" => $row_btn,
                                    ]);
                                    $price = number_format("$price",0," "," ");
                                    $price = $price." ".$summText;
                                    $caption_text = "<b>".$product_name."</b>\n\n"."<b><i>".$price."</i></b>\n\n".$details_txt."<b>".$detail_name."</b>: <i>".$selecting."🔎</i>\n\n".$text_select;
                                    editMessageCaption($chat_id,$message_id,$caption_text,$btn_details);
                                    $s = 1;
                                    die();
                                } else if ($value["id_details"] < ($id_detail + 1)) {
                                    if ($s != 1) {
                                        if ($step_1 == 1) {
                                            $detail_name_uz = $value["detail_name_uz"];
                                            $order_multi_details_id = $value["order_multi_details_id"];
                                            $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_uz"];
                                        } else if ($step_1 == 2) {
                                            $detail_name_uz = $value["detail_name_ru"];
                                            $order_multi_details_id = $value["order_multi_details_id"];
                                            $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_ru"];
                                        }
                                        $details_txt = $details_txt."<b>".$detail_name_uz."</b>: <b><i>".$multi_detail_name."</i></b>\n\n";
                                    }
                                } 
                                $i++;
                            }
                            if ($i == count($arr)) {
                                foreach ($arr as $key => $value) {
                                    $id_details = $value["id_details"];
                                    $price = $value["price"];
                                    $id_product = $value["product_id"];
                                    if ($step_1 == 1) {
                                        $product_name = $value["product_name_uz"];
                                        $detail_name = $value["detail_name_uz"];
                                        $detail_name_uz = $value["detail_name_uz"];
                                        $order_multi_details_id = $value["order_multi_details_id"];
                                        $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_uz"];
                                    } else if ($step_1 == 2){
                                        $product_name = $value["product_name_ru"];
                                        $detail_name = $value["detail_name_ru"];
                                        $detail_name_uz = $value["detail_name_ru"];
                                        $order_multi_details_id = $value["order_multi_details_id"];
                                        $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_ru"];
                                    }
                                    if ($value["id_details"] >= count($arr) ) {
                                        $details_txt = $details_txt."<b>".$detail_name_uz."</b>: <b><i>".$multi_detail_name."</i></b>\n\n";
                                    }

                                    $row_btn = [];
                                    $btn = [];
                                    $i = 1;
                                    
                                    


                                    $price = number_format("$price",0," "," ");
                                    $price = $price." ".$summText;
                                    $caption_text = "<b>".$product_name."</b>\n\n"."<b><i>".$price."</i></b>\n\n".$details_txt;
                                    if ($value["id_details"] == 1) { 
                                        $row_btn_count = [];
                                        $btn_column =[];
                                        for ($i=1; $i < 10; $i++) {
                                            $clb_data_count_btn = $i."_buy_".$id_product;
                                            $text_btn = "+".$i;
                                            $btn_column[] = ["callback_data" => $clb_data_count_btn,"text" => $text_btn];
                                            if ($i % 3 == 0) {
                                                $row_btn_count[] = $btn_column;
                                                $btn_column = [];
                                            }
                                        }
                                        $clb_cancel = "cancel_".$id_product;
                                        $clb_back = $id_product."_back";

                                        $row_btn_count[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text]];
                                        $row_btn_count[] = [["callback_data" => $clb_back,"text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                                        $btn_count = json_encode([
                                            'inline_keyboard' => $row_btn_count
                                        ]);
                                        editMessageCaption($chat_id,$message_id,$caption_text,$btn_count);
                                        updateStep(7,$chat_id,$conn);
                                        die();
                                    }
                                }
                            }
                        }



                    // $caption_text =  "<b>".$title_product."</b>\n\n<b>".$size_product.$size_title."</b>";
                    // editMessageCaption($chat_id,$message_id,$caption_text,$btn_color);
                    // updateStep(7,$chat_id,$conn);
                    }
                }
            } else if ($explode_data[1] == "back") {

                deleteMessage($chat_id,$message_id);

                $sql = '
                    SELECT 
                        p.id as product_id,
                        d.id as details_id,
                        md.id as multi_details_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        d.title_uz as datail_name_uz,
                        d.title_ru as datail_name_ru,
                        p.description_uz as description_uz,
                        p.description_ru as description_ru,
                        md.title_uz as multi_details_name_uz,
                        md.title_ru as multi_details_name_ru,
                        p.price as price,
                        p.img as img,
                        p.sub_category_id as sub_id
                    FROM products as p
                    INNER JOIN products AS pr ON pr.sub_category_id = p.sub_category_id
                    LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                    LEFT JOIN details AS d ON pd.details_id = d.id
                    LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where pr.id = 
                '.$explode_data[0];  
                // sendMessage($chat_id,$sql);
                $result = $conn->query($sql);
                $arr = [];
                if (isset($result) && !empty($result)) {
                    foreach ($result as $key => $value) {
                        $arr[$value['product_id']]['img'] = $value['img'];
                        $arr[$value['product_id']]['sub_id'] = $value['sub_id'];
                        $arr[$value['product_id']]['price'] = $value['price'];
                        $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                        $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                        $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                            $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                        }

                        $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                        if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                            $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                             $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                        }
                    }
                }
                $txt = '';
                if (isset($arr) and !empty($arr)) {
                    $i = 0;
                    foreach ($arr as $key => $value) {
                        $product_id = $key;
                        $sub_id = $value["sub_id"];
                        if ($step_1 == 1) {
                            $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_uz"];
                            foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        } else if ($step_1 == 2) {
                            $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                            $price = $value["price"];
                            $price = number_format("$price",0," "," ");
                            $price = $price." ".$summText;
                            $txt = $txt."<b><i>".$price."</i></b>\n\n";
                            $description = $value["description_ru"];
                            foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                    $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                }
                                $txt = substr($txt,0,-2);
                                $txt = $txt."\n\n";
                            }
                        }

                        $clb_karzina = "_karzina_".$product_id;

                        $row_btn_count = [];
                        
                        $row_btn_count[] =  [
                            ['callback_data' => $sub_id."_back",'text' => $back],
                            ['callback_data' => $clb_karzina,'text' => $karzina]
                        ];
                        $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                        $keyboard_products = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]); 


                        $img = $value["img"];
                        $price = $value["price"];
                        $txt = $txt."<code>".$description."</code>";
                        $get_result = sendPhotoBtn($chat_id,$img,$txt,$keyboard_products);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                        $txt = '';
                        $i++;
                    }
                    updateStep(5,$chat_id,$conn);
                }
            } else if ($explode_data[0] == "cancel") {
                $delete_order = "DELETE FROM orders WHERE status = 0 and count = 0 and chat_id = ".$chat_id;
                $result_order = $conn->query($delete_order);
                if ($result_order == TRUE) {
                    $sql = '
                        SELECT 
                            p.id as product_id,
                            d.id as details_id,
                            md.id as multi_details_id,
                            p.title_uz as product_name_uz,
                            p.title_ru as product_name_ru,
                            d.title_uz as datail_name_uz,
                            d.title_ru as datail_name_ru,
                            p.description_uz as description_uz,
                            p.description_ru as description_ru,
                            md.title_uz as multi_details_name_uz,
                            md.title_ru as multi_details_name_ru,
                            p.price as price,
                            p.img as img,
                            p.sub_category_id as sub_id
                        FROM products as p
                        LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                        LEFT JOIN details AS d ON pd.details_id = d.id
                        LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where p.id =
                    '.$explode_data[1];  
                    $result = $conn->query($sql);
                    $arr = [];
                    if (isset($result) && !empty($result)) {
                        foreach ($result as $key => $value) {
                            $arr[$value['product_id']]['img'] = $value['img'];
                            $arr[$value['product_id']]['sub_id'] = $value['sub_id'];
                            $arr[$value['product_id']]['price'] = $value['price'];
                            $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                            $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                            $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                            }

                            $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                                $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                                 $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                            }
                        }
                    }
                    $txt = '';
                    if (isset($arr) and !empty($arr)) {
                        $i = 0;
                        foreach ($arr as $key => $value) {
                            $product_id = $key;
                            if ($step_1 == 1) {

                                $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                                $price = $value["price"];
                                $price = number_format("$price",0," "," ");
                                $price = $price." ".$summText;
                                $txt = $txt."<b><i>".$price."</i></b>\n\n";
                                $description = $value["description_uz"];
                                foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                    $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                    foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                        $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                    }
                                    $txt = substr($txt,0,-2);
                                    $txt = $txt."\n\n";
                                }
                            } else if ($step_1 == 2) {
                                $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                                $price = $value["price"];
                                $price = number_format("$price",0," "," ");
                                $price = $price." ".$summText;
                                $txt = $txt."<b><i>".$price."</i></b>\n\n";
                                $description = $value["description_ru"];
                                foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                    $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                    foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                        $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                    }
                                    $txt = substr($txt,0,-2);
                                    $txt = $txt."\n\n";
                                }
                            }

                            $clb_karzina = "_karzina_".$product_id;

                            $row_btn_count = [];
                            
                            $row_btn_count[] =  [
                                ['callback_data' => $value["sub_id"]."_back",'text' => $back],
                                ['callback_data' => $clb_karzina,'text' => $karzina]
                            ];
                            $row_btn_count[] = [['callback_data' => $product_id."_pictures",'text' => $lookPictures],['callback_data' => "menu",'text' => $menuText]];

                            $keyboard_products = json_encode([
                                'inline_keyboard' => $row_btn_count
                            ]); 


                            $img = $value["img"];
                            $price = $value["price"];
                            $txt = $txt."<code>".$description."</code>";
                            // sendMessage($chat_id,$txt);
                            $get_result = editMessageCaption($chat_id,$message_id,$txt,$keyboard_products);
                            $txt = '';
                            $i++;
                        }
                        updateStep(5,$chat_id,$conn);
                    }
                }
                updateStep(5,$chat_id,$conn);
            }
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 7 and $text != "/start") {
        if (isset($update->callback_query)) {
            $explode_data = explode("_", $data);
            $count = $explode_data[0];
            $id_product = $explode_data[2];
            $key_word = $explode_data[1];
            // sendMessage($chat_id,$data);

            if ($key_word == "buy") {

                $sql = "UPDATE orders as o SET o.count = o.count + ".$count." , status = 2 WHERE status = 0 or status = 2 and chat_id = ".$chat_id." and product_id = ".$id_product;
                $result = $conn->query($sql);
                
                if ($result == true) {
                    $row_btn_count = [];
                    $btn_column =[];
                    for ($i=1; $i < 10; $i++) {
                        $clb_data_count_btn = $i."_buy_".$id_product;
                        $text_btn = "+".$i;
                        $btn_column[] = ["callback_data" => $clb_data_count_btn,"text" => $text_btn];
                        if ($i % 3 == 0) {
                            $row_btn_count[] = $btn_column;
                            $btn_column = [];
                        }
                    }
                    $clb_cancel = "cancel_".$id_product;
                    $clb_take_order = "take-order_".$id_product;

                    $row_btn_count[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text]];
                    $row_btn_count[] = [["callback_data" => $clb_take_order,"text" =>$take_order]];
                    $row_btn_count[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                    $btn_count = json_encode([
                        'inline_keyboard' => $row_btn_count
                    ]);

                    $sql = "
                        SELECT 
                            d.id as details_id,
                            d.title_uz as details_name_uz,
                            d.title_ru as details_name_ru,
                            md.id as multi_detail_id,
                            md.title_uz as multi_detail_name_uz,
                            md.title_ru as multi_detail_name_ru,
                            p.id as product_id,
                            p.title_uz as product_name_uz,
                            p.title_ru as product_name_ru,
                            p.price as price,
                            od.details_id as order_details_id,
                            od.multi_details_id as order_multi_details_id,
                            o.count as count
                        FROM product_details_ as pd 
                        INNER JOIN products as p ON pd.product_id = p.id 
                        INNER JOIN details as d on pd.details_id = d.id 
                        INNER JOIN orders as o on p.id = o.product_id and o.status = 0 or o.status = 2 and o.chat_id = ".$chat_id."
                        LEFT JOIN order_details as od on d.id = od.details_id and o.id = od.order_id
                        INNER JOIN multi_details as md on pd.multi_details_id = md.id 
                        WHERE pd.product_id = ".$id_product." order by pd.id asc
                    ";
                    $result = $conn->query($sql);
                    $arr = [];
                    $i = 0;
                    if (isset($result) and !empty($result) and $result->num_rows > 0) {
                        foreach ($result as $key => $value) {
                           $arr[$value["details_id"]]["id_details"] = $i;
                           $arr[$value["details_id"]]["price"] = $value["price"];
                           $arr[$value["details_id"]]["count"] = $value["count"];
                           $arr[$value["details_id"]]["product_id"] = $value["product_id"];
                           $arr[$value["details_id"]]["product_name_uz"] = $value["product_name_uz"];
                           $arr[$value["details_id"]]["product_name_ru"] = $value["product_name_ru"];
                           $arr[$value["details_id"]]["detail_name_uz"] = $value["details_name_uz"];
                           $arr[$value["details_id"]]["detail_name_ru"] = $value["details_name_ru"];
                           $arr[$value["details_id"]]["order_details_id"] = $value["order_details_id"];
                           $arr[$value["details_id"]]["order_multi_details_id"] = $value["order_multi_details_id"];
                           $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_uz"] = $value["multi_detail_name_uz"];
                           $arr[$value["details_id"]]["multi_detail_id"][$value["multi_detail_id"]]["multi_detail_name_ru"] = $value["multi_detail_name_ru"];
                            if (count($arr[$value["details_id"]]["multi_detail_id"]) == 1){
                               $i++;
                            }
                        }
                    }
                    $details_txt = '';
                    $i = 0;
                    // sendMessage($chat_id,json_encode($arr));
                    if (isset($arr) and !empty($arr)) {
                        foreach ($arr as $key => $value) {
                            $id_details = $value["id_details"];
                            $price = $value["price"];
                            $count_order = $value["count"];
                            $id_product = $value["product_id"];
                            if ($step_1 == 1) {
                                $product_name = $value["product_name_uz"];
                                $detail_name = $value["detail_name_uz"];
                                $detail_name_uz = $value["detail_name_uz"];
                                $order_multi_details_id = $value["order_multi_details_id"];
                                $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_uz"];
                            } else if ($step_1 == 2){
                                $product_name = $value["product_name_ru"];
                                $detail_name = $value["detail_name_ru"];
                                $detail_name_uz = $value["detail_name_ru"];
                                $order_multi_details_id = $value["order_multi_details_id"];
                                $multi_detail_name = $value["multi_detail_id"][$order_multi_details_id]["multi_detail_name_ru"];
                            }
                            if ($value["id_details"]<= count($arr) ) {
                                $details_txt = $details_txt."<b>".$detail_name_uz."</b>: <b><i>".$multi_detail_name."</i></b>\n\n";
                            }

                            $row_btn = [];
                            $btn = [];
                            $i = 1;
                            
                            if (count($btn) < 4) {
                                $row_btn[] = $btn;
                            }
                        }
                        $row_btn_count = [];
                        $btn_column =[];
                        for ($i=1; $i < 10; $i++) {
                            $clb_data_count_btn = $i."_buy_".$id_product;
                            $text_btn = "+".$i;
                            $btn_column[] = ["callback_data" => $clb_data_count_btn,"text" => $text_btn];
                            if ($i % 3 == 0) {
                                $row_btn_count[] = $btn_column;
                                $btn_column = [];
                            }
                        }
                        $clb_cancel = "cancel_".$id_product;
                        $clb_confirm = "confirm_".$id_product;
                        $clb_back = $id_product."_back";

                        $row_btn_count[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text],["callback_data" => 'karzina',"text" =>$go_to_carzina]];
                        $row_btn_count[] = [["callback_data" => $clb_back,"text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                        $btn_count = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]);

                        // btn end


                        $all_summ = $price * $count_order;  

                        $price = number_format("$price",0," "," ");
                        $price = $price." ".$summText;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;

                        $sum_text = "<b>".$price." * ".$count_order. " = ".$all_summ."</b>";
                        $caption_text = "<b>"   .$product_name."</b>\n\n<b><i>".$sum_text."</i></b>\n\n".$details_txt;
                        editMessageCaption($chat_id,$message_id,$caption_text,$btn_count);
                    }
                    
                }
            } else if ($key_word == "buysecond") {

                $sql = "UPDATE orders as o SET o.count = o.count + ".$count." , status = 2 WHERE status = 0 or status = 2 and chat_id = ".$chat_id." and product_id = ".$id_product;
                $result = $conn->query($sql);
                
                if ($result == true) {
                    $row_btn_count = [];
                        $btn_column =[];
                        for ($i=1; $i < 10; $i++) {
                            $clb_data_count_btn = $i."_buysecond_".$id_product;
                            $text_btn = "+".$i;
                            $btn_column[] = ["callback_data" => $clb_data_count_btn,"text" => $text_btn];
                            if ($i % 3 == 0) {
                                $row_btn_count[] = $btn_column;
                                $btn_column = [];
                            }
                        }
                        $clb_cancel = "cancel_".$id_product;
                        $clb_confirm = "confirm_".$id_product;
                        $clb_back = $id_product."_back";

                        $row_btn_count[] = [["callback_data" => $clb_cancel,"text" =>$cancel_text],["callback_data" => 'karzina',"text" =>$go_to_carzina]];
                        $row_btn_count[] = [["callback_data" => $clb_back,"text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                        $btn_count = json_encode([
                            'inline_keyboard' => $row_btn_count
                        ]);

                    $sql = "
                        SELECT p.title_uz,
                            p.title_ru,
                            p.price,
                            p.description_uz,
                            p.description_ru,
                            o.count 
                        FROM orders AS o 
                        INNER JOIN products as p on o.product_id = p.id 
                        WHERE ( o.status = 0 or o.status = 2 ) and p.id = ".$id_product." and o.chat_id = 
                    ".$chat_id;
                    $result_two = $conn->query($sql);
                    if ($result_two->num_rows > 0) {
                        $row_two = $result_two->fetch_assoc();
                        $price = $row_two["price"];
                        $count_order = $row_two["count"];
                        $all_summ = $price * $count_order;

                        $price = number_format("$price",0," "," ");
                        $price = $price." ".$summText;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;

                        if ($step_1 == 1) {
                            $product_name = $row_two["title_uz"];
                            $description = $row_two["description_uz"];
                        } else if ($step_1 == 2) {
                            $product_name = $row_two["title_ru"];
                            $description = $row_two["description_ru"];
                        }
                        $sum_text = "<b>".$price." * ".$count_order. " = ".$all_summ."</b>";
                        $caption_text = "<b>"   .$product_name."</b>\n\n<b><i>".$sum_text."</i></b>";
                        editMessageCaption($chat_id,$message_id,$caption_text,$btn_count);
                    }
                }

            } else if ($explode_data[1] == "back") {
                $sql_update = "UPDATE orders SET status = 3 where status = 2 and chat_id = ".$chat_id;
                $conn->query($sql_update);


                deleteMessage($chat_id,$message_id);

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }

                deleteLogMessage($chat_id,$conn);

                $sql = "
                    select sct.title_uz, sct.title_ru,sct.id from products AS p 
                    inner join sub_category as sc on p.sub_category_id = sc.id
                    inner join sub_category as sct on sc.category_id = sct.category_id or sc.sub_category_id = sct.sub_category_id
                    where p.id = ".$explode_data[0];
                // sendMessage($chat_id,$sql."\n".$data);                  
                // die();
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row_btn = [];
                    $btn = [];
                    $i = 1;
                    while ($row = $result->fetch_assoc()) {
                        $clb_data = "sub-category_".$row["id"];
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_1 == 2) {
                            $title = $row["title_ru"];
                        }
                        $btn[] = ["callback_data" => $clb_data,"text" => $title];
                        if ($i%2 == 0) {
                            $row_btn[] = $btn;
                            $btn = [];
                        }

                        $i++;
                    }

                    if (count($btn) == 1) {
                        $row_btn[] = $btn;
                    }

                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];

                    $btn_menu = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                }
                sendMessageBtn($chat_id,$product_catalog,$btn_menu);
                updateStep(4,$chat_id,$conn);
            } else if ($data == "karzina") {
               deleteMessage($chat_id,$message_id);

               $sql_update = "UPDATE orders SET status = 3 where status = 2 and chat_id = ".$chat_id;
               $conn->query($sql_update);

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }

                deleteLogMessage($chat_id,$conn);
                $sql = "
                    SELECT 
                        o.id as order_id,
                        o.count as order_count,
                        p.id as product_id,
                        p.title_uz as product_name_uz,
                        p.title_ru as product_name_ru,
                        p.price as product_price
                    FROM orders as o    
                    INNER JOIN products as p on o.product_id = p.id 
                    WHERE o.chat_id = ".$chat_id." ORDER BY o.id ASC
                ";
                $result = $conn->query($sql);
                if ($result->num_rows > 0 ){
                    $row_btn = [];
                    $btns = [];
                    $i = 0;
                    $txt = '';
                    $txt = $txt.$text_order;
                    $txt = $txt."\n\n<b>----------------------------------------</b>\n\n";
                    $umumiy_hisob = 0;
                    while ($row = $result->fetch_assoc()){
                        if ($step_1 == 1) {
                            $product_name = $row["product_name_uz"];
                        } else  if ($step_1 == 2) {
                            $product_name = $row["product_name_ru"];
                        }
                        $order_id = $row["order_id"];
                        $product_id = $row["product_id"];
                        $product_price = $row["product_price"];
                        $order_count = $row["order_count"];
                        $clb_order = $order_id."_order"; 


                        $txt = $txt."<b>".$product_name."</b>\n\n";
                        $all_summ = $order_count * $product_price;

                        $umumiy_hisob = $umumiy_hisob + $all_summ;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;
                        
                        $product_price = number_format("$product_price",0," "," ");
                        $product_price = $product_price." ".$summText;

                        $txt = $txt."<b>".$product_price." ✖️ ".$order_count." = ".$all_summ."</b>\n\n";

                        if ($i == 0) {
                            $btns[] =  ["callback_data" => $clb_order , "text" => "👉 ".$product_name." 👈"];
                            $row_btn[] = $btns;
                            $btns = [];
                            $btns[] = ["callback_data" => "minus_".$order_id, "text" => "-1"];
                            $btns[] = ["callback_data" => "plus_".$order_id, "text" => "+1"];
                            $row_btn[] = $btns;
                            $btns = [];
                        } else {
                            $btns[] =  ["callback_data" => $clb_order , "text" => $product_name];
                        }

                        if ($i % 2 == 0){
                            $row_btn[] = $btns;
                            $btns = [];
                        }
                        $i++;
                    }
                    if (count($btns) == 1) {
                        $row_btn[] = $btns;
                    }
                    $row_btn[] = [
                        ["callback_data" => "clear_basket" , "text" => $clear_basket],
                        ["callback_data" => "buy_orders","text" => $buy_orders]
                    ];
                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];
                    $btn_details = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                    $umumiy_hisob = number_format("$umumiy_hisob",0," "," ");
                    $umumiy_hisob = $umumiy_hisob." ".$summText;
                    $txt = $txt."<b>".$all_summ_text.$umumiy_hisob."</b>\n\n";
                    $txt = $txt."<b>----------------------------------------</b>";
                    sendMessageBtn($chat_id,$txt,$btn_details);
                }
                updateStep(8,$chat_id,$conn);
            } else if ($explode_data[0] == "cancel") {
                $delete_order = "DELETE FROM orders WHERE status = 0 or status = 2 and chat_id = ".$chat_id;
                $result_order = $conn->query($delete_order);
                if ($result_order == TRUE) {
                    $sql = '
                        SELECT 
                            p.id as product_id,
                            d.id as details_id,
                            md.id as multi_details_id,
                            p.title_uz as product_name_uz,
                            p.title_ru as product_name_ru,
                            d.title_uz as datail_name_uz,
                            d.title_ru as datail_name_ru,
                            p.description_uz as description_uz,
                            p.description_ru as description_ru,
                            md.title_uz as multi_details_name_uz,
                            md.title_ru as multi_details_name_ru,
                            p.price as price,
                            p.img as img
                        FROM products as p
                        LEFT JOIN product_details_ as pd on p.id = pd.product_id 
                        LEFT JOIN details AS d ON pd.details_id = d.id
                        LEFT JOIN multi_details as md ON pd.multi_details_id = md.id where p.id =
                    '.$explode_data[1];  
                    $result = $conn->query($sql);
                    $arr = [];
                    if (isset($result) && !empty($result)) {
                        foreach ($result as $key => $value) {
                            $arr[$value['product_id']]['img'] = $value['img'];
                            $arr[$value['product_id']]['price'] = $value['price'];
                            $arr[$value['product_id']]['title_uz'] = $value['product_name_uz'];
                            $arr[$value['product_id']]['description_uz'] = $value['description_uz'];
                            $arr[$value['product_id']]['description_ru'] = $value['description_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_uz'])) {
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['datail_name_uz'] = $value['datail_name_uz'];
                                $arr[$value['product_id']]['detail_uz'][$value['details_id']]['multi_detail_uz'][$value['multi_details_id']] = $value['multi_details_name_uz'];
                            }

                            $arr[$value['product_id']]['title_ru'] = $value['product_name_ru'];
                            if (isset($value['datail_name_uz']) and !empty($value['datail_name_ru'])) {
                                $arr[$value['product_id']]['detail_ru'][$value['details_id']]['datail_name_ru'] = $value['datail_name_ru'];
                                 $arr[$value['product_id']]['detail_ru'][$value['details_id']]['multi_detail_ru'][$value['multi_details_id']] = $value['multi_details_name_ru'];
                            }
                        }
                    }
                    $txt = '';
                    if (isset($arr) and !empty($arr)) {
                        $i = 0;
                        foreach ($arr as $key => $value) {
                            $product_id = $key;
                            if ($step_1 == 1) {

                                $txt = $txt."<b>".$value["title_uz"]."</b>\n\n";
                                $price = $value["price"];
                                $price = number_format("$price",0," "," ");
                                $price = $price." ".$summText;
                                $txt = $txt."<b><i>".$price."</i></b>\n\n";
                                $description = $value["description_uz"];
                                foreach ($value["detail_uz"] as $keyTwo => $valueTwo) {
                                    $txt = $txt."<b>".$valueTwo["datail_name_uz"]."</b>: "; // need base64_decode 
                                    foreach ($valueTwo["multi_detail_uz"] as $keyThree => $valueThree) {
                                        $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                    }
                                    $txt = substr($txt,0,-2);
                                    $txt = $txt."\n\n";
                                }
                            } else if ($step_1 == 2) {
                                $txt = $txt."<b>".$value["title_ru"]."</b>\n\n";
                                $price = $value["price"];
                                $price = number_format("$price",0," "," ");
                                $price = $price." ".$summText;
                                $txt = $txt."<b><i>".$price."</i></b>\n\n";
                                $description = $value["description_ru"];
                                foreach ($value["detail_ru"] as $keyTwo => $valueTwo) {
                                    $txt = $txt."<b>".$valueTwo["datail_name_ru"]."</b>: "; // need base64_decode 
                                    foreach ($valueTwo["multi_detail_ru"] as $keyThree => $valueThree) {
                                        $txt = $txt."<b><i>".$valueThree."</i></b>, ";
                                    }
                                    $txt = substr($txt,0,-2);
                                    $txt = $txt."\n\n";
                                }
                            }

                            $clb_karzina = "_karzina_".$product_id;

                            $row_btn_count = [];
                            
                            $row_btn_count[] =  [
                                ['callback_data' => $id."_back",'text' => $back],
                                ['callback_data' => $clb_karzina,'text' => $karzina]
                            ];
                            $row_btn_count[] = [['callback_data' => "menu",'text' => $menuText]];

                            $keyboard_products = json_encode([
                                'inline_keyboard' => $row_btn_count
                            ]); 


                            $img = $value["img"];
                            $price = $value["price"];
                            $txt = $txt."<code>".$description."</code>";
                            // sendMessage($chat_id,$txt);
                            $get_result = editMessageCaption($chat_id,$message_id,$txt,$keyboard_products);
                            $txt = '';
                            $i++;
                        }
                        updateStep(5,$chat_id,$conn);
                    }
                }
                updateStep(5,$chat_id,$conn);
            }
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 8 and $text != "/start") {
        if (isset($update->callback_query)) {
            $explode_data = explode("_",$data);
            $key_word = $explode_data[0];
            // sendMessage($chat_id,$data);
            if ($key_word == "plus") {
                $order_id = $explode_data[1];
                $sql_update = "UPDATE orders as o SET o.count = o.count + 1 WHERE o.id = ".$order_id;
                $result_update = $conn->query($sql_update);
                if ($result_update == TRUE) {

                    $sql = "
                        SELECT
                            od.id as order_id,
                            od.count as order_count,
                            p1.id as product_id,
                            p1.title_uz as product_name_uz,
                            p1.title_ru as product_name_ru,
                            p1.price as product_price
                        FROM orders as od  
                        INNER JOIN products as p1 on od.product_id = p1.id
                        WHERE od.id = ".$order_id." and od.chat_id = ".$chat_id."
                        UNION 
                        SELECT 
                            ord_t.id as order_id,
                            ord_t.count as order_count,
                            p2.id as product_id,
                            p2.title_uz as product_name_uz,
                            p2.title_ru as product_name_ru,
                            p2.price as product_price
                        FROM orders as ord_t  
                        INNER JOIN products as p2 on ord_t.product_id = p2.id
                        WHERE ord_t.id !=  
                    ".$order_id." and ord_t.chat_id = ".$chat_id;
                    // sendMessage(1033542488,$sql);
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0 ){
                        $row_btn = [];
                        $btns = [];
                        $i = 0;
                        $txt = '';
                        $txt = $txt.$text_order;
                        $txt = $txt."\n\n<b>----------------------------------------</b>\n\n";
                        $umumiy_hisob = 0;
                        while ($row = $result->fetch_assoc()){
                            if ($step_1 == 1) {
                                $product_name = $row["product_name_uz"];
                            } else  if ($step_1 == 2) {
                                $product_name = $row["product_name_ru"];
                            }
                            $order_id = $row["order_id"];
                            $product_id = $row["product_id"];
                            $product_price = $row["product_price"];
                            $order_count = $row["order_count"];
                            $clb_order = $order_id."_order"; 


                            $txt = $txt."<b>".$product_name."</b>\n\n";
                            $all_summ = $order_count * $product_price;

                            $umumiy_hisob = $umumiy_hisob + $all_summ;

                            $all_summ = number_format("$all_summ",0," "," ");
                            $all_summ = $all_summ." ".$summText;
                            
                            $product_price = number_format("$product_price",0," "," ");
                            $product_price = $product_price." ".$summText;

                            $txt = $txt."<b>".$product_price." ✖️ ".$order_count." = ".$all_summ."</b>\n\n";

                            if ($i == 0) {
                                $btns[] =  ["callback_data" => $clb_order , "text" => "👉 ".$product_name." 👈"];
                                $row_btn[] = $btns;
                                $btns = [];
                                $btns[] = ["callback_data" => "minus_".$order_id, "text" => "-1"];
                                $btns[] = ["callback_data" => "plus_".$order_id, "text" => "+1"];
                                $row_btn[] = $btns;
                                $btns = [];
                            } else {
                                $btns[] =  ["callback_data" => $clb_order , "text" => $product_name];
                            }

                            if ($i % 2 == 0){
                                $row_btn[] = $btns;
                                $btns = [];
                            }
                            $i++;
                        }
                        if (count($btns) == 1) {
                            $row_btn[] = $btns;
                        }
                        $row_btn[] = [
                            ["callback_data" => "clear_basket" , "text" => $clear_basket],
                            ["callback_data" => "buy_orders","text" => $buy_orders]
                        ];
                        $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];
                        $btn_details = json_encode([
                            "inline_keyboard" => $row_btn,
                        ]);
                        $umumiy_hisob = number_format("$umumiy_hisob",0," "," ");
                        $umumiy_hisob = $umumiy_hisob." ".$summText;
                        $txt = $txt."<b>".$all_summ_text.$umumiy_hisob."</b>\n\n";
                        $txt = $txt."<b>----------------------------------------</b>";
                        editMessageText($chat_id,$message_id,$txt,$btn_details);
                    }
                } else {
                    sendMessage($chat_id,"succes");
                }
            } else if ($key_word == "minus") {
                $order_id = $explode_data[1];
                $sql = "SELECT * FROM orders as ord WHERE ord.id = ".$order_id;
                $result_check = $conn->query($sql);
                $delete_status = false;
                $update_status = false;
                if ($result_check->num_rows > 0) {
                    $row = $result_check->fetch_assoc();
                    $count = $row["count"];
                    if ($count == 1) {
                        $delete = "DELETE FROM orders where id = ".$order_id;
                        $result_delete = $conn->query($delete);
                        if ($result_delete == TRUE ) {
                            $delete_status = true;
                        }
                    } else if ($count > 1) {
                        $sql_update = "UPDATE orders as ord SET ord.count = ord.count - 1 WHERE ord.id = ".$order_id;
                        $result_update = $conn->query($sql_update);
                        if ($result_update == TRUE) {
                            $update_status = true;
                        }
                    }
                }
                if ($delete_status == false and $update_status == true) {
                    $sql = "
                        SELECT
                            od.id as order_id,
                            od.count as order_count,
                            p1.id as product_id,
                            p1.title_uz as product_name_uz,
                            p1.title_ru as product_name_ru,
                            p1.price as product_price
                        FROM orders as od  
                        INNER JOIN products as p1 on od.product_id = p1.id
                        WHERE od.id = ".$order_id." and od.chat_id = ".$chat_id."
                        UNION 
                        SELECT 
                            ord_t.id as order_id,
                            ord_t.count as order_count,
                            p2.id as product_id,
                            p2.title_uz as product_name_uz,
                            p2.title_ru as product_name_ru,
                            p2.price as product_price
                        FROM orders as ord_t  
                        INNER JOIN products as p2 on ord_t.product_id = p2.id
                        WHERE ord_t.id !=  
                    ".$order_id . " and ord_t.chat_id = ".$chat_id;
                    $result = $conn->query($sql);
                } else if ($delete_status == true and $update_status == false ){
                    $sql = "
                        SELECT 
                            o.id as order_id,
                            o.count as order_count,
                            p.id as product_id,
                            p.title_uz as product_name_uz,
                            p.title_ru as product_name_ru,
                            p.price as product_price
                        FROM orders as o    
                        INNER JOIN products as p on o.product_id = p.id 
                        WHERE o.chat_id = ".$chat_id." ORDER BY o.id ASC
                    ";
                    $result = $conn->query($sql);
                } else {
                    sendMessage($chat_id,"nothing is true");
                }
                if ($result->num_rows > 0 ){
                    $row_btn = [];
                    $btns = [];
                    $i = 0;
                    $txt = '';
                    $txt = $txt.$text_order;
                    $txt = $txt."\n\n<b>----------------------------------------</b>\n\n";
                    $umumiy_hisob = 0;
                    while ($row = $result->fetch_assoc()){
                        if ($step_1 == 1) {
                            $product_name = $row["product_name_uz"];
                        } else  if ($step_1 == 2) {
                            $product_name = $row["product_name_ru"];
                        }
                        $order_id = $row["order_id"];
                        $product_id = $row["product_id"];
                        $product_price = $row["product_price"];
                        $order_count = $row["order_count"];
                        $clb_order = $order_id."_order"; 


                        $txt = $txt."<b>".$product_name."</b>\n\n";
                        $all_summ = $order_count * $product_price;

                        $umumiy_hisob = $umumiy_hisob + $all_summ;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;
                        
                        $product_price = number_format("$product_price",0," "," ");
                        $product_price = $product_price." ".$summText;

                        $txt = $txt."<b>".$product_price." ✖️ ".$order_count." = ".$all_summ."</b>\n\n";

                        if ($i == 0) {
                            $btns[] =  ["callback_data" => $clb_order , "text" => "👉 ".$product_name." 👈"];
                            $row_btn[] = $btns;
                            $btns = [];
                            $btns[] = ["callback_data" => "minus_".$order_id, "text" => "-1"];
                            $btns[] = ["callback_data" => "plus_".$order_id, "text" => "+1"];
                            $row_btn[] = $btns;
                            $btns = [];
                        } else {
                            $btns[] =  ["callback_data" => $clb_order , "text" => $product_name];
                        }

                        if ($i % 2 == 0){
                            $row_btn[] = $btns;
                            $btns = [];
                        }
                        $i++;
                    }
                    if (count($btns) == 1) {
                        $row_btn[] = $btns;
                    }
                    $row_btn[] = [
                        ["callback_data" => "clear_basket" , "text" => $clear_basket],
                        ["callback_data" => "buy_orders","text" => $buy_orders]
                    ];
                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];
                    $btn_details = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                    $umumiy_hisob = number_format("$umumiy_hisob",0," "," ");
                    $umumiy_hisob = $umumiy_hisob." ".$summText;
                    $txt = $txt."<b>".$all_summ_text.$umumiy_hisob."</b>\n\n";
                    $txt = $txt."<b>----------------------------------------</b>";
                    editMessageText($chat_id,$message_id,$txt,$btn_details);
                } else {
                    deleteMessage($chat_id,$message_id);
                    if ($step_1 == 1) {
                        $txt_emtpy = "😔 Sizning savatchangiz hozircha bo`sh!";
                    } else if ($step_1 == 2) {
                        $txt_emtpy = "😔 Ваша корзина сейчас пуста!";
                    }
                    sendPhotoBtn($chat_id,$empty,$txt_emtpy,$back_to_menu); 
                }
            } else if ($explode_data[1] == "order") {
                $order_id = $explode_data[0];
                $sql = "
                    SELECT
                        od.id as order_id,
                        od.count as order_count,
                        p1.id as product_id,
                        p1.title_uz as product_name_uz,
                        p1.title_ru as product_name_ru,
                        p1.price as product_price
                    FROM orders as od  
                    INNER JOIN products as p1 on od.product_id = p1.id
                    WHERE od.id = ".$order_id." and od.chat_id = ".$chat_id."
                    UNION 
                    SELECT 
                        ord_t.id as order_id,
                        ord_t.count as order_count,
                        p2.id as product_id,
                        p2.title_uz as product_name_uz,
                        p2.title_ru as product_name_ru,
                        p2.price as product_price
                    FROM orders as ord_t  
                    INNER JOIN products as p2 on ord_t.product_id = p2.id
                    WHERE ord_t.id !=  
                ".$order_id." and ord_t.chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0 ){
                    $row_btn = [];
                    $btns = [];
                    $i = 0;
                    $txt = '';
                    $txt = $txt.$text_order;
                    $txt = $txt."\n\n<b>----------------------------------------</b>\n\n";
                    $umumiy_hisob = 0;
                    while ($row = $result->fetch_assoc()){
                        if ($step_1 == 1) {
                            $product_name = $row["product_name_uz"];
                        } else  if ($step_1 == 2) {
                            $product_name = $row["product_name_ru"];
                        }
                        $order_id = $row["order_id"];
                        $product_id = $row["product_id"];
                        $product_price = $row["product_price"];
                        $order_count = $row["order_count"];
                        $clb_order = $order_id."_order"; 


                        $txt = $txt."<b>".$product_name."</b>\n\n";
                        $all_summ = $order_count * $product_price;

                        $umumiy_hisob = $umumiy_hisob + $all_summ;

                        $all_summ = number_format("$all_summ",0," "," ");
                        $all_summ = $all_summ." ".$summText;
                        
                        $product_price = number_format("$product_price",0," "," ");
                        $product_price = $product_price." ".$summText;

                        $txt = $txt."<b>".$product_price." ✖️ ".$order_count." = ".$all_summ."</b>\n\n";

                        if ($i == 0) {
                            $btns[] =  ["callback_data" => $clb_order , "text" => "👉 ".$product_name." 👈"];
                            $row_btn[] = $btns;
                            $btns = [];
                            $btns[] = ["callback_data" => "minus_".$order_id, "text" => "-1"];
                            $btns[] = ["callback_data" => "plus_".$order_id, "text" => "+1"];
                            $row_btn[] = $btns;
                            $btns = [];
                        } else {
                            $btns[] =  ["callback_data" => $clb_order , "text" => $product_name];
                        }

                        if ($i % 2 == 0){
                            $row_btn[] = $btns;
                            $btns = [];
                        }
                        $i++;
                    }
                    if (count($btns) == 1) {
                        $row_btn[] = $btns;
                    }
                    $row_btn[] = [
                        ["callback_data" => "clear_basket" , "text" => $clear_basket],
                        ["callback_data" => "buy_orders","text" => $buy_orders]
                    ];
                    $row_btn[] = [["callback_data" => "back","text" => $back_text],["callback_data" => "menu","text" => $menuText]];
                    $btn_details = json_encode([
                        "inline_keyboard" => $row_btn,
                    ]);
                    $umumiy_hisob = number_format("$umumiy_hisob",0," "," ");
                    $umumiy_hisob = $umumiy_hisob." ".$summText;
                    $txt = $txt."<b>".$all_summ_text.$umumiy_hisob."</b>\n\n";
                    $txt = $txt."<b>----------------------------------------</b>";
                    editMessageText($chat_id,$message_id,$txt,$btn_details);
                }
            } else if ($data == "clear_basket") {
                $delete_order = "DELETE FROM orders WHERE chat_id = ".$chat_id;
                $result_drop = $conn->query($delete_order);
                if ($result_drop == true) {
                    deleteMessage($chat_id,$message_id);
                    sendMessageBtn($chat_id,$your_orders_deleted,$menu_btn);
                    updateStep(2,$chat_id,$conn);

                }
            } else if ($data == "buy_orders") {
                deleteMessage($chat_id,$message_id);
                $sql = "SELECT * FROM region";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $arr = [];
                    $row_arr = [];
                    $b = 1;
                    while ($row = $result->fetch_assoc()) {
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_2 == 2) {
                            $title = $row["title_ru"];
                        }
                        $callback_data = "region_".$row["id"];
                        $arr[] = ["callback_data" => $callback_data,'text' => $title];
                        if ($b % 3 == 0) {
                            $row_arr[] = $arr;
                            $arr = [];
                        }
                        $b++;
                    }
                    $row_arr[] = [
                        [
                            'callback_data' => "back",
                            'text'=> $back_text
                        ],
                        [
                            'callback_data' => "menu",
                            'text'=> $menuText
                        ]
                    ];
                    $btn_region = json_encode(["inline_keyboard" => $row_arr]);
                    sendMessageBtn($chat_id,$choose_region,$btn_region);
                    updateStep(10,$chat_id,$conn);
                }
            } else if ($data == "back") {
                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                deleteLogMessage($chat_id,$conn);

                updateStep(2,$chat_id,$conn);
                deleteMessage($chat_id,$message_id);

                $get_result = bot('sendMessage',[
                    'chat_id' => $chat_id,
                    'text' => $menuText,
                    'reply_markup' => $menu_btn
                ]);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);

            }
        }
    }

    // take product delivery or take him
  
    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 10 and $text != "/start") {
        if (isset($update->callback_query)) {
            $data_ex = explode("_", $data);
            if ($data_ex[0] == "region") {
                $id = $data_ex[1];
                $sql = "UPDATE last_id SET region_id = ".$id." WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result) {
                    deleteMessage($chat_id,$message_id);
                    $get_result = sendMessageBtn($chat_id,$send_your_location,$back_btn);
                    $delete_message_id = $get_result->result->message_id;
                    insertLogMessage($chat_id,$delete_message_id,$conn);
                    updateStep(11,$chat_id,$conn);
                }
            } else if ($data == "back") {
                deleteMessage($chat_id,$message_id);
                sendMessageBtn($chat_id,$choose_order_type,$buy_type);
                updateStep(9,$chat_id,$conn);
            }
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 11 and $text != "/start") {
        if (isset($message->location)) {
            deleteMessage($chat_id,$message_id);
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            $location = $message->location;
            $long = $location->longitude;
            $lat = $location->latitude;
            $sql = "UPDATE last_id SET lang = '".$long."', lat = '".$lat."' WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result) {
                $sql = "
                    SELECT 
                        ord.count,
                        pr.price,
                        pr.title_uz,
                        pr.title_ru
                    FROM orders as ord
                    INNER JOIN products AS pr  ON ord.product_id = pr.id
                    WHERE ord.status = 3 AND ord.chat_id = ".$chat_id;
                $result = $conn->query($sql);
                $all_sum = 0;
                if ($result->num_rows > 0) {
                    $txt = "➖➖➖➖➖➖➖➖➖➖\n\n";
                    while ($row = $result->fetch_assoc()) {
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_1 == 2) {
                            $title = $row["title_ru"];
                        }
                        $txt = $txt."<b>".$title."</b>";
                        $sum = $row["price"] * $row["count"];
                        $all_summ = $all_summ + $sum;
                        $price = number_format($row["price"],0," "," ")." ".$summText;
                        $sum = number_format($sum,0," "," ")." ".$summText;
                        $txt = $txt.PHP_EOL."<b>".$price. " ✖️ ".$row["count"]." = ".$sum."</b>".PHP_EOL.PHP_EOL;

                    }
                    $all_summ = number_format($all_summ,0," "," ")." ".$summText;
                    $txt = $txt."<b>".$all_summ_text." ".$all_summ."</b>";
                
                    $sql_region = "
                        SELECT 
                            ls.lang,
                            ls.lat,
                            rg.title_uz,
                            rg.title_ru
                        FROM last_id as ls 
                        INNER JOIN  region as rg on ls.region_id = rg.id  
                        WHERE chat_id = ".$chat_id;
                    $result_region = $conn->query($sql_region);
                    if($result_region->num_rows > 0) {
                        $row = $result_region->fetch_assoc();
                        if ($step_1 == 1) {
                            $title = $row["title_uz"];
                        } else if ($step_1 == 2){
                            $title = $row["title_ru"];
                        }
                        $your_region_text = "Siz tanlagan viloyat";
                        $free_delivery = "O'zbekiston bo`yicha yetkazib berish bepul ❗️❗️❗️";
                        $location_addres_text = "Lacatsiya manzili : ⤵️";
                        $txt = $txt.PHP_EOL.PHP_EOL."<b>".$your_region_text.": ".$title."</b>";
                        $txt = $txt.PHP_EOL.PHP_EOL."<b>".$free_delivery."</b>";
                        $txt = $txt.PHP_EOL.PHP_EOL."<b>".$location_addres_text."</b>";
                        $lang = $row["lang"];
                        $lat = $row["lat"];
                    }

                    $txt = $txt."\n\n➖➖➖➖➖➖➖➖➖➖";
                    $get_result = sendMessage($chat_id,$txt);
                    $delete_message_id = $get_result->result->message_id;
                    insertLogMessage($chat_id,$delete_message_id,$conn);
                    sendLocationBtn($chat_id,$lat,$lang,$btn_check);
                    updateStep(12,$chat_id,$conn);
                }
            }            
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 12 and $text != "/start") {
        if (isset($update->callback_query)) {
            if ($data == "checked") {
                deleteMessage($chat_id,$message_id);
                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                $sql_region = "
                    SELECT 
                        ls.lang,
                        ls.lat,
                        ls.region_id,
                        rg.title_uz,
                        rg.title_ru,
                        cl.id
                    FROM last_id as ls 
                    INNER JOIN  region as rg on ls.region_id = rg.id  
                    INNER JOIN clients AS cl on ls.chat_id = cl.chat_id
                    WHERE ls.chat_id = ".$chat_id;
                $result_region = $conn->query($sql_region);
                if ($result_region->num_rows > 0) {
                    $row = $result_region->fetch_assoc();
                    $lat = $row["lat"];
                    $lang = $row["lang"];
                    $client_id = $row["id"];
                    $region_id = $row["region_id"];
                }
                $sql_order_code = "SELECT order_code FROM order_code ORDER BY order_code DESC limit 1";
                $result_order_code = $conn->query($sql_order_code);
                if ($result_order_code->num_rows > 0) {
                    $row = $result_order_code->fetch_assoc();
                    $order_code = $row["order_code"];
                    $order_code = $order_code + 1;
                } else {
                    $order_code = 100000;                    
                }
                $current_date = date("Y-m-d H:i:s");
                $sql_insert_order_code = "
                    INSERT INTO order_code (order_code,created_date,stage,client_id,lat,lang,region_id) 
                    VALUES 
                    (".$order_code.",'".$current_date."',1,".$client_id.",'".$lat."','".$lang."',".$region_id.")
                ";
                $result_insert = $conn->query($sql_insert_order_code);
                if ($result_insert) {
                    $sql = "
                        SELECT 
                            ord.count,
                            pr.price,
                            pr.title_uz,
                            pr.title_ru,
                            ord.product_id,
                            ord.id
                        FROM orders as ord
                        INNER JOIN products AS pr  ON ord.product_id = pr.id
                        WHERE ord.status = 3 AND ord.chat_id = ".$chat_id;
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0 ) {
                        while ($row = $result->fetch_assoc()) {
                            $count = $row["count"];
                            $product_id = $row["product_id"];
                            $price = $row["price"];
                            $order_id = $row["id"];
                            $sell_price = $count * $price;
                            $sql_arrive_order = "
                                INSERT INTO arrive_orders (order_code_id,product_id,sell_price,count,created_date,order_id)
                                VALUES 
                                (".$order_code.",".$product_id.",".$sell_price.",".$count.",'".$current_date."',".$order_id.")";
                            $conn->query($sql_arrive_order);
                        }
                        sendMessageBtn($chat_id,$choose_pay_type,$btn_payment_providers);
                        $sql = "DELETE FROM orders WHERE chat_id = ".$chat_id;
                        $conn->query($sql);
                        updateStep(100,$chat_id,$conn);
                    }
                } else {
                    sendMessage($chat_id,"fail");
                }
            } else if ($data == "cancel") {
                // deleteMessage($chat_id,$message_id);
            }
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 20 and $text != "/start") {
        if (isset($update->callback_query)) {
            // sendMessage($chat_id,$data);
            // die();
            $sql = " SELECT * FROM clients WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();
            if ($data == "my_name") {
                deleteMessage($chat_id,$message_id);
                if ($step_1 == 1){
                    $txt = "Sizning Ismingiz: <b>".base64_decode($row["full_name"])."</b>\n";
                    $txt = $txt."Ismingizni o'zgartirishingiz mumkin! Buning uchun yangi Ismingizni bizga yuboring!";

                } else if ($step_1 == 2) {
                    $txt = "Ваше имя: <b>".base64_decode($row["full_name"])."</b>\n";
                    $txt = $txt."Вы можете изменить свое имя! Пришлите нам для этого свое новое Имя!";
                }

                $get_result = sendMessageBtn($chat_id,$txt,$menu_back_keyboard);
                updateStep(21,$chat_id,$conn);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            } else if ($data == "my_number") {
                deleteMessage($chat_id,$message_id);
                if ($step_1 == 1){
                    $txt = "Sizning Telefon Raqamingiz: <b>"    .base64_decode($row["phone_number"])."</b>\n";
                    $txt = $txt."Telefon Raqamingizni o'zgartirishingiz mumkin! Buning uchun yangi Telefon raqamingizni bizga yuboring!";

                } else if ($step_1 == 2) {
                    $txt = "Ваш номер телефона: <b>".base64_decode($row["phone_number"])."</b>\n";
                    $txt = $txt."Вы можете изменить свое номер телефона! Пришлите нам для этого свое новое номер телефона!";
                }
                $get_result = sendMessageBtn($chat_id,$txt,$menu_back_keyboard);
                updateStep(22,$chat_id,$conn);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            } else if ($data == "my_gender") {
                deleteMessage($chat_id,$message_id);
                $gender = "🚫";
                if (isset($row["gender"]) and !empty($row["gender"])) {
                    if ($row["gender"] == 1) {
                        $gender = $man;
                    } else if ($row["gender"] == 2) {
                        $gender = $woman;
                    }
                }
                if ($step_1 == 1){
                    $txt = "Sizning Jinsingiz: <b>".$gender."</b>\n";
                    $txt = $txt."Jinsingizni o'zgartirishingiz mumkin!";

                } else if ($step_1 == 2) {
                    $txt = "Ваш пол: <b>".$gender."</b>\n";
                    $txt = $txt."Вы можете изменить свое пол!";
                }
                $get_result = sendMessageBtn($chat_id,$txt,$gender_btn);
                updateStep(23,$chat_id,$conn);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            } else if ($data == "my_old") {
                deleteMessage($chat_id,$message_id);
                $old = "🚫";
                if (isset($row["old"]) and !empty($row["old"])) {
                    $old = $row["old"];
                }
                if ($step_1 == 1){
                    $txt = "Sizning Yoshingiz: <b>".$old."</b>\n";
                    $txt = $txt."Yoshingizni o'zgartirishingiz mumkin! Buning uchun yangi Yoshingizni bizga yuboring!";

                } else if ($step_1 == 2) {
                    $txt = "Ваше возраст: <b>".$old."</b>\n";
                    $txt = $txt."Вы можете изменить свое возраст! Пришлите нам для этого свое новое возраст!";
                }

                $get_result = sendMessageBtn($chat_id,$txt,$menu_back_keyboard);
                updateStep(24,$chat_id,$conn);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            }
        }       
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 21 and $text != "/start" and $text != $menuText) {
        if (isset($text) and $text != $back_text) {
            $txt = base64_encode($text);
            $update_sql = "UPDATE clients SET full_name = '".$txt."' WHERE chat_id = ".$chat_id;
            $result_update = $conn->query($update_sql);
            if ($result_update == true) {

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                deleteLogMessage($chat_id,$conn);

                deleteMessage($chat_id,$message_id);

                if ($step_1 == 1) {
                    $your_name_is_updated = "Sizning Ismingiz: <b>$text</b>ga o'zgartirildi!";
                } else if ($step_1 == 2) {
                    $your_name_is_updated = "Ваше имя было изменено на <b>".$text."</b>";
                }
                sendMessageBtn($chat_id,$your_name_is_updated,$about_me_btn);
                updateStep(20,$chat_id,$conn);
            }
        } else if ($text == $back_text) {
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $full_name = '';
                if (isset($row["full_name"]) && !empty($row["full_name"])) {
                    $full_name = base64_decode($row["full_name"]);
                } 
                $old = "🚫";
                if (isset($row["old"]) && !empty($row["old"])) {

                    $old = $row["old"];
                }
                $gender = "🚫";
                if (isset($row["gender"]) && !empty($row["gender"])) {
                    if ($row["gender"] == 1) {
                        $gender = $man;
                    }  else if ($row["gender"] == 2) {
                        $gender = $woman;
                    }
                }
                $phone_number = base64_decode($row["phone_number"]);
                $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

            }
            deleteMessage($chat_id,$message_id);
            sendMessageBtn($chat_id,$my_informations,$about_me_btn);
            updateStep(20,$chat_id,$conn);
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 22 and $text != "/start" and $text != $menuText) {
        if (isset($text) and $text != $back_text) {
            if (preg_match("/^[0-9+]*$/",$text)) {
                $countNumber = strlen($text);
                if($countNumber == 13) {
                    if (mb_strpos($text, "+998") !== false) {
                        $txt = base64_encode($text);
                        $update_sql = "UPDATE clients SET phone_number = '".$txt."' WHERE chat_id = ".$chat_id;
                        $result_update = $conn->query($update_sql);
                        if ($result_update == true) {
                            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                                    deleteMessage($chat_id,$row["message_id"]);
                                }
                            }
                            deleteLogMessage($chat_id,$conn);

                            deleteMessage($chat_id,$message_id);

                            if ($step_1 == 1) {
                                $your_name_is_updated = "Sizning Telefon Raqamingiz: <b>$text</b>ga o'zgartirildi!";
                            } else if ($step_1 == 2) {
                                $your_name_is_updated = "Ваш номер телефона было изменено на <b>".$text."</b>";
                            }
                            sendMessageBtn($chat_id,$your_name_is_updated,$about_me_btn);
                            updateStep(20,$chat_id,$conn);
                        }
                    } else {
                        deleteMessage($chat_id,$message_id);
                        $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                deleteMessage($chat_id,$row["message_id"]);
                            }
                        }
                        $get_result = sendMessageBtn($chat_id,$error_number,$back_keyboard);
                        $delete_message_id = $get_result->result->message_id;
                        insertLogMessage($chat_id,$delete_message_id,$conn);
                    }
                } else {
                    deleteMessage($chat_id,$message_id);
                    $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            deleteMessage($chat_id,$row["message_id"]);
                        }
                    }
                    $get_result = sendMessageBtn($chat_id,$error_number,$back_keyboard);
                    $delete_message_id = $get_result->result->message_id;
                    insertLogMessage($chat_id,$delete_message_id,$conn);
                }
            } else {
                deleteMessage($chat_id,$message_id);
                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                deleteLogMessage($chat_id,$conn);
                $get_result = sendMessageBtn($chat_id,$error_number,$back_keyboard);
                $delete_message_id = $get_result->result->message_id;
                insertLogMessage($chat_id,$delete_message_id,$conn);
            }
        } else if ($text == $back_text) {
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }

            $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $full_name = '';
                if (isset($row["full_name"]) && !empty($row["full_name"])) {
                    $full_name = base64_decode($row["full_name"]);
                } 
                $old = "🚫";
                if (isset($row["old"]) && !empty($row["old"])) {

                    $old = $row["old"];
                }
                $gender = "🚫";
                if (isset($row["gender"]) && !empty($row["gender"])) {
                    if ($row["gender"] == 1) {
                        $gender = $man;
                    }  else if ($row["gender"] == 2) {
                        $gender = $woman;
                    }
                }
                $phone_number = base64_decode($row["phone_number"]);
                $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

            }
            deleteMessage($chat_id,$message_id);
            sendMessageBtn($chat_id,$my_informations,$about_me_btn);
            updateStep(20,$chat_id,$conn);
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 23 and $text != "/start" and $text != $menuText) {
        if (isset($update->callback_query) and !empty($update->callback_query)) {
            if ($data == "man") {
                $gender_id = 1;
            } else if ($data == "woman") {
                $gender_id = 2;
            }
            $sql = "UPDATE clients SET gender = ".$gender_id." WHERE chat_id =".$chat_id;
            $result = $conn->query($sql);
            if ($result) {
                $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $full_name = '';
                    if (isset($row["full_name"]) && !empty($row["full_name"])) {
                        $full_name = base64_decode($row["full_name"]);
                    } 
                    $old = "🚫";
                    if (isset($row["old"]) && !empty($row["old"])) {

                        $old = $row["old"];
                    }
                    $gender = "🚫";
                    if (isset($row["gender"]) && !empty($row["gender"])) {
                        if ($row["gender"] == 1) {
                            $gender = $man;
                        }  else if ($row["gender"] == 2) {
                            $gender = $woman;
                        }
                    }
                    $phone_number = base64_decode($row["phone_number"]);
                    $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

                }
                deleteMessage($chat_id,$message_id);
                sendMessageBtn($chat_id,$my_informations,$about_me_btn);
                updateStep(20,$chat_id,$conn);
            } 

        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 24 and $text != "/start" and $text != $menuText) {
        if (isset($text) and $text != $back_text) {
            $txt = intval($text);
            $update_sql = "UPDATE clients SET old = '".$txt."' WHERE chat_id = ".$chat_id;
            $result_update = $conn->query($update_sql);
            if ($result_update == true) {

                $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        deleteMessage($chat_id,$row["message_id"]);
                    }
                }
                
            }
            $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $full_name = '';
                if (isset($row["full_name"]) && !empty($row["full_name"])) {
                    $full_name = base64_decode($row["full_name"]);
                } 
                $old = "🚫";
                if (isset($row["old"]) && !empty($row["old"])) {

                    $old = $row["old"];
                }
                $gender = "🚫";
                if (isset($row["gender"]) && !empty($row["gender"])) {
                    if ($row["gender"] == 1) {
                        $gender = $man;
                    }  else if ($row["gender"] == 2) {
                        $gender = $woman;
                    }
                }
                $phone_number = base64_decode($row["phone_number"]);
                $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

            }
            deleteMessage($chat_id,$message_id);
            sendMessageBtn($chat_id,$my_informations,$about_me_btn);
            updateStep(20,$chat_id,$conn);
        } else if ($text == $back_text) {
            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            
            $sql = "SELECT * FROM clients WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $full_name = '';
                if (isset($row["full_name"]) && !empty($row["full_name"])) {
                    $full_name = base64_decode($row["full_name"]);
                } 
                $old = "🚫";
                if (isset($row["old"]) && !empty($row["old"])) {

                    $old = $row["old"];
                }
                $gender = "🚫";
                if (isset($row["gender"]) && !empty($row["gender"])) {
                    if ($row["gender"] == 1) {
                        $gender = $man;
                    }  else if ($row["gender"] == 2) {
                        $gender = $woman;
                    }
                }
                $phone_number = base64_decode($row["phone_number"]);
                $my_informations = $my_informations.PHP_EOL.PHP_EOL.$your_name.": ".$full_name.PHP_EOL.$your_phone_number.": +".$phone_number.PHP_EOL.$your_gender.": ".$gender.PHP_EOL.$your_old.": ".$old;

            }
            deleteMessage($chat_id,$message_id);
            sendMessageBtn($chat_id,$my_informations,$about_me_btn);
            updateStep(20,$chat_id,$conn);
        }
    }

    if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 100 and $text != "/start" and $text != $menuText) {
        if (isset($update->callback_query)) {
            $sql = "
                SELECT sum(ao.sell_price) as sell_price, order_code, client_id FROM (
                    SELECT oc.order_code,oc.client_id FROM clients as c
                    INNER JOIN order_code as oc ON c.id = oc.client_id 
                    WHERE c.chat_id = ".$chat_id." ORDER BY oc.id DESC LIMIT 1
                ) as t
                INNER JOIN arrive_orders as ao ON t.order_code = ao.order_code_id
            ";
            // sendMessage($chat_id,$sql);
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $sum_bs = $row["sell_price"];
                $order_code = $row["order_code"];
                $client_id = $row["client_id"];
            }
            $sum = $sum_bs * 100;
            
            if ($data == "click") {
                deleteMessage($chat_id,$message_id);
                $payment = new Payment;
                $payment->order_code = $order_code;
                $payment->amount = $sum_bs;
                $payment->client_id = $client_id;
                $payment->provider = 'click';
                $labeled = array('label' => 'example','amount' => $sum);
                $data_ret = bot('sendInvoice',[
                    'chat_id' => $chat_id,
                    'title' => $order_code.' Order code',
                    'description' => 'Click',
                    'payload' => json_encode($payment),
                    'provider_token' => '333605228:LIVE:13698_11E033C6F3C6F991A00BA973845518F0B1E589F7',
                    'start_parameter' => 'start',
                    'currency' => 'UZS',
                    'prices' => json_encode((array($labeled))),
                ]);
            } else if ($data == "payme") {
                deleteMessage($chat_id,$message_id);
                $payment = new Payment;
                $payment->order_code = $order_code;
                $payment->amount = $sum_bs;
                $payment->client_id = $client_id;
                $payment->provider = 'click';
                $labeled = array('label' => 'example','amount' => $sum);
                $data_ret = bot('sendInvoice',[
                    'chat_id' => $chat_id,
                    'title' => $order_code.' Order code',
                    'description' => 'Payme',
                    'payload' => json_encode($payment),
                    'provider_token' => '387026696:LIVE:612779f89b3d88791c47377a',
                    'start_parameter' => 'start',
                    'currency' => 'UZS',
                    'prices' => json_encode((array($labeled))),
                ]);
            } else if ($data == "cash") {
                deleteMessage($chat_id,$message_id);
                $sql = "UPDATE order_code  SET pay = 3 WHERE order_code = ".$order_code." AND client_id = ".$client_id;
                $result = $conn->query($sql);
                if ($result) {
                    sendMessageBtn($chat_id,$get_order_text,$back_to_menu);
                }
            }
        }
    }

    // if (($step_1 == 1 OR $step_1 == 2) and $step_2 == 101 and $text != "/start" and $text != $menuText) {
    //     if (isset($update->message)) {
    //         $sql = "select * from last_id where chat_id = ".$chat_id;
    //         $result = $conn->query($sql);
    //         if ($result->num_rows > 0 ){
    //             $row = $result->fetch_assoc();
    //             $last_id = $row["last_id"];
    //         }
    //         if (isset($message->photo)) {
    //             $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
    //             $result_two = $conn->query($sql);
    //             if ($result_two->num_rows > 0) {
    //                while ($row_two = $result->fetch_assoc()) {
    //                    deleteMessage($chat_id,$row_two["message_id"]);
    //               }
    //             }
    //             deleteMessage($chat_id,$message_id);
    //             deleteLogMessage($chat_id,$conn);
    //             if(isset($message->photo[2])) {
    //                 $file_id = $message->photo[2]->file_id;
    //             } else if (isset($message->photo[1])) {
    //                 $file_id = $message->photo[1]->file_id;
    //             } else { 
    //                 $file_id = $message->photo[0]->file_id;
    //             }
    //             $imageFileId = bot('getFile', [
    //                 'file_id' => $file_id
    //             ]);
    //             if(isset($imageFileId)) {
    //                 $file = 'https://api.telegram.org/file/bot'.TOKEN.'/'.$imageFileId->result->file_path;
    //             }
                
    //             $sql_update = "update order_code set type = 1, file_id = '".$file."' WHERE order_code = ".$last_id;
    //             $result_update = $conn->query($sql_update);
    //             if ($result_update == true) {
    //                 if ($step_1 == 1) {
    //                     $txt = "Sizning to`lovingiz rasmi muvaffaqiyatli tarzda saqlandi! ✅\nTez orada siz bilan bog`lanamiz 📞";
    //                 } else if ($step_1 == 2){ 
    //                     $txt = "Изображение вашего платежа успешно сохранено! ✅\nМы свяжемся с вами в ближайшее время 📞";
    //                 }
    //                 sendMessageBtn($chat_id,$txt,$back_to_menu);
    //             }
    //         } else if (isset($message->text)) {
                
    //             $type = $message->entities[0]->type;
    //             if ($type == "url") {
    //                 $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
    //                 $result_two = $conn->query($sql);
    //                 if ($result_two->num_rows > 0) {
    //                    while ($row_two = $result->fetch_assoc()) {
    //                        deleteMessage($chat_id,$row_two["message_id"]);
    //                   }
    //                 }
    //                 deleteMessage($chat_id,$message_id);
    //                 deleteLogMessage($chat_id,$conn);

    //                 $sql_update = "update order_code set type = 2, file_id = '".$text."' WHERE order_code = ".$last_id;
    //                 $result_update = $conn->query($sql_update);
    //                 if ($result_update == true) {
    //                     if ($step_1 == 1) {
    //                         $txt = "Sizning to`lovingiz url linki muvaffaqiyatli tarzda saqlandi! ✅\nTez orada siz bilan bog`lanamiz 📞";
    //                     } else if ($step_1 == 2) {
    //                         $txt = "Ссылка на ваш платежный URL-адрес успешно сохранена! ✅\nМы свяжемся с вами в ближайшее время 📞";
    //                     }
    //                     sendMessageBtn($chat_id,$txt,$back_to_menu);
    //                 } else {
    //                     sendMessage($chat_id,$sql_update);
    //                 }
    //             } else {
    //                 deleteMessage($chat_id,$message_id);
    //                 die();
    //             }
    //         }
    //         $sql_two = "
    //            SELECT 
    //                 ao.count,
    //                 p.title_uz,
    //                 oc.order_code,
    //                 c.phone_number,
    //                 c.full_name,
    //                 c.tg_username,
    //                 ao.sell_price,
    //                 oc.type
    //             FROM arrive_orders as ao 
    //             INNER JOIN products as p ON ao.product_id = p.id
    //             INNER JOIN order_code as oc on ao.order_code_id = oc.order_code
    //             INNER JOIN clients AS c on oc.client_id = c.id
    //             WHERE order_code_id = ".$last_id;
    //         $result_two = $conn->query($sql_two);
    //         if ($result_two->num_rows > 0 ) {
    //             $txt = "🔔  bbay.uz 🔔 \n📨 Buyurtma berildi \n";
    //             $all_summ = 0;
    //             $all_count = 0;
    //             $i = 1;
    //             $rt_num = $result->num_rows;
    //             while ($row_two = $result_two->fetch_assoc()) {
                    
    //                 if ($i == 1) {
    //                     $txt = $txt."<b>🔢 Buyurtma kodi</b>: #".$row_two["order_code"];   
    //                     $txt = $txt."\n<b>👤 Mijoz ismi</b>: ".base64_decode($row_two["full_name"]);   
    //                     $txt = $txt."\n<b>📱 Telefon raqami</b>: +".base64_decode($row_two["phone_number"]);
    //                     if (isset($row_two["tg_username"]) and !empty($row_two["tg_username"])) {
    //                         $txt = $txt."\n<b>👤 Mijoz username:</b> @".base64_decode($row_two["tg_username"]);   
    //                         $txt = $txt."\n📌 <b>Mahsulotlar</b>:\n";
    //                     }  
    //                 }
    //                 $all_summ = $all_summ + $row_two["sell_price"];
    //                 $all_count = $all_count + $row_two["count"];
    //                 $txt = $txt."♻️ <b>".$row_two["title_uz"]."</b> Soni: ".$row_two["count"]."\n";
    //                 if ($i-2 == $rt_num) {
    //                     $all_summ = number_format($all_summ,0," "," ");
    //                     $all_summ = $all_summ." So`m";
    //                     $txt = $txt."<b>🔄 Mahsulot soni: </b>".$all_count."\n";
    //                     $txt = $txt."<b>💰 Umumiy summa: </b>".$all_summ."\n";
    //                     if ($row_two["type"] == 1 or $row_two["type"] == 2) {
    //                         $txt = $txt." 💳 To'lov turi: karta";
    //                     } else {
    //                         $txt = $txt." 💳 To'lov turi: payme";
    //                     }

    //                 }
    //                 $i++;
    //             }

    //             sendMessage(-557751752,$txt);
    //         }
    //         updateStep(2222,$chat_id,$conn);
    //     } 
    // }

    if (isset($update->callback_query)) {
        if ($data == "menu") {

            updateStep(2,$chat_id,$conn);
            deleteMessage($chat_id,$message_id);

            $sql_update = "UPDATE orders SET status = 3 where status = 2 and chat_id = ".$chat_id;
            $conn->query($sql_update);


            $sql_drop = "DELETE FROM orders where status = 0 and chat_id = ".$chat_id;
            $conn->query($sql_drop);

            $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    deleteMessage($chat_id,$row["message_id"]);
                }
            }
            deleteLogMessage($chat_id,$conn);
            $caption_text = "";
            $get_result = bot('sendPhoto',[
                'chat_id' => $chat_id,
                'photo' => 'https://myexact.uz/web/images/img/logo.png',
                'caption' => $caption_txt
            ]);
            
            $delete_message_id = $get_result->result->message_id;
            insertLogMessage($chat_id,$delete_message_id,$conn);

            $get_result = bot('sendMessage',[
                'chat_id' => $chat_id,
                'text' => $menuText,
                'reply_markup' => $menu_btn
            ]);
            $delete_message_id = $get_result->result->message_id;
            insertLogMessage($chat_id,$delete_message_id,$conn);
        }
    } 

    if (isset($text) and $text == $menuText) {
        $sql = "SELECT * FROM log_messages WHERE chat_id = ".$chat_id;
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                deleteMessage($chat_id,$row["message_id"]);
            }
        }
        deleteLogMessage($chat_id,$conn);

        updateStep(2,$chat_id,$conn);
        deleteMessage($chat_id,$message_id);

        $get_result = bot('sendPhoto',[
            'chat_id' => $chat_id,
            'photo' => 'https://myexact.uz/web/images/img/logo.png',
            'caption' => $caption_txt
        ]);
        
        $delete_message_id = $get_result->result->message_id;
        insertLogMessage($chat_id,$delete_message_id,$conn);


        $get_result = bot('sendMessage',[
            'chat_id' => $chat_id,
            'text' => $menuText,
            'reply_markup' => $menu_btn
        ]);

        $delete_message_id = $get_result->result->message_id;
        insertLogMessage($chat_id,$delete_message_id,$conn);
    }

// 2ta atler table clientsga
// 1ta table last_id