<?php

use yii\db\Migration;

/**
 * Class m210904_063741_cashback_table
 */
class m210904_063741_cashback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cashbacks', [
            'id' => $this->primaryKey(),
            'cashback' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210904_063741_cashback_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210904_063741_cashback_table cannot be reverted.\n";

        return false;
    }
    */
}
