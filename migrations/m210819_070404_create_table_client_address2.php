<?php

use yii\db\Migration;

/**
 * Class m210819_070404_create_table_client_address2
 */
class m210819_070404_create_table_client_address2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('clients_address2', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'region' => $this->string(),
            'address' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210819_070404_create_table_client_address2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210819_070404_create_table_client_address2 cannot be reverted.\n";

        return false;
    }
    */
}
