<?php

use yii\db\Migration;

/**
 * Class m210812_061903_persons_option_tables
 */
class m210812_061903_persons_option_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients_wishlist', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'client_id' => $this->integer()
        ]);
        $this->createTable('clients_address', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'region' => $this->string(),
            'address' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_061903_persons_option_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_061903_persons_option_tables cannot be reverted.\n";

        return false;
    }
    */
}
