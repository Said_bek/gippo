<?php

use yii\db\Migration;

/**
 * Class m210902_124824_cashback_crud
 */
class m210902_124824_cashback_crud extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client_cashbacks', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'cashback_parcent' => $this->integer(),
            'cashback_summ' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210902_124824_cashback_crud cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210902_124824_cashback_crud cannot be reverted.\n";

        return false;
    }
    */
}
