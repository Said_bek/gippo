<?php

use yii\db\Migration;

/**
 * Class m210821_105711_add_column_dollar_to_products
 */
class m210821_105711_add_column_dollar_to_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN dollar DOUBLE PRECISION";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210821_105711_add_column_dollar_to_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210821_105711_add_column_dollar_to_products cannot be reverted.\n";

        return false;
    }
    */
}
