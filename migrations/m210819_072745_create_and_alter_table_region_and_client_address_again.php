<?php

use yii\db\Migration;

/**
 * Class m210819_072745_create_and_alter_table_region_and_client_address_again
 */
class m210819_072745_create_and_alter_table_region_and_client_address_again extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql2 = "ALTER TABLE clients_address2 ALTER COLUMN region_id TYPE integer USING region_id::integer;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210819_072745_create_and_alter_table_region_and_client_address_again cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210819_072745_create_and_alter_table_region_and_client_address_again cannot be reverted.\n";

        return false;
    }
    */
}
