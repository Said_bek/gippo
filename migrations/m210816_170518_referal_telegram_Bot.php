<?php

use yii\db\Migration;

/**
 * Class m210816_170518_referal_telegram_Bot
 */
class m210816_170518_referal_telegram_Bot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE clients ADD COLUMN referal_tgbot varchar";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210816_170518_referal_telegram_Bot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210816_170518_referal_telegram_Bot cannot be reverted.\n";

        return false;
    }
    */
}
