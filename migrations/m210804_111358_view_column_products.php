<?php

use yii\db\Migration;

/**
 * Class m210804_111358_view_column_products
 */
class m210804_111358_view_column_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'view', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210804_111358_view_column_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210804_111358_view_column_products cannot be reverted.\n";

        return false;
    }
    */
}
