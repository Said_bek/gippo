<?php

use yii\db\Migration;

/**
 * Class m210822_143955_add_column_to_products_made_ru_and_made_uz
 */
class m210822_143955_add_column_to_products_made_ru_and_made_uz extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN made_uz character varying";
        $this->execute($sql);

        $sql = "ALTER TABLE products ADD COLUMN made_ru character varying";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210822_143955_add_column_to_products_made_ru_and_made_uz cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210822_143955_add_column_to_products_made_ru_and_made_uz cannot be reverted.\n";

        return false;
    }
    */
}
