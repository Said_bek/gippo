<?php

use yii\db\Migration;

/**
 * Class m210818_093517_alter_table_product
 */
class m210818_093517_alter_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN sold_product integer DEFAULT (0)";
        $this->execute($sql);
        $sql = "ALTER TABLE arrive_orders ADD COLUMN created_date timestamp";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210818_093517_alter_table_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210818_093517_alter_table_product cannot be reverted.\n";

        return false;
    }
    */
}
