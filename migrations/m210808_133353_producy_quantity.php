<?php

use yii\db\Migration;

/**
 * Class m210808_133353_producy_quantity
 */
class m210808_133353_producy_quantity extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN quantity integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210808_133353_producy_quantity cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210808_133353_producy_quantity cannot be reverted.\n";

        return false;
    }
    */
}
