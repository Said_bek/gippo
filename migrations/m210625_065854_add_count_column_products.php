<?php

use yii\db\Migration;

/**
 * Class m210625_065854_add_count_column_products
 */
class m210625_065854_add_count_column_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN count integer";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_065854_add_count_column_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_065854_add_count_column_products cannot be reverted.\n";

        return false;
    }
    */
}
