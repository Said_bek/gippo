<?php

use yii\db\Migration;

/**
 * Class m210822_130441_add_column_to_products_link_title_en_desc_en
 */
class m210822_130441_add_column_to_products_link_title_en_desc_en extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN title_en character varying";
        $this->execute($sql);

        $sql = "ALTER TABLE products ADD COLUMN link character varying";
        $this->execute($sql);

        $sql = "ALTER TABLE products ADD COLUMN description_en text";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210822_130441_add_column_to_products_link_title_en_desc_en cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210822_130441_add_column_to_products_link_title_en_desc_en cannot be reverted.\n";

        return false;
    }
    */
}
