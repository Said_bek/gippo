<?php

use yii\db\Migration;

/**
 * Class m210807_102334_new_table_for_reviews
 */
class m210807_102334_new_table_for_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(),
            'customer_title' => $this->string(),
            'customer_post' => $this->string(),
            'star' => $this->smallInteger(),
            'created_date' => $this->timestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210807_102334_new_table_for_reviews cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210807_102334_new_table_for_reviews cannot be reverted.\n";

        return false;
    }
    */
}
