<?php

use yii\db\Migration;

/**
 * Class m210822_110731_add_column_to_detail_and_multi_details
 */
class m210822_110731_add_column_to_detail_and_multi_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE details ADD COLUMN title_en character varying";
        $this->execute($sql);

        $sql = "ALTER TABLE multi_details ADD COLUMN title_en character varying";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210822_110731_add_column_to_detail_and_multi_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210822_110731_add_column_to_detail_and_multi_details cannot be reverted.\n";

        return false;
    }
    */
}
