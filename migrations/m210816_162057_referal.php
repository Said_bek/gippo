<?php

use yii\db\Migration;

/**
 * Class m210816_162057_referal
 */
class m210816_162057_referal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE clients ADD COLUMN referal varchar";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210816_162057_referal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210816_162057_referal cannot be reverted.\n";

        return false;
    }
    */
}
