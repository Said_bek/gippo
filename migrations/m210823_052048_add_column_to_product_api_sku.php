<?php

use yii\db\Migration;

/**
 * Class m210823_052048_add_column_to_product_api_sku
 */
class m210823_052048_add_column_to_product_api_sku extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE products ADD COLUMN sku_api character varying";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210823_052048_add_column_to_product_api_sku cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210823_052048_add_column_to_product_api_sku cannot be reverted.\n";

        return false;
    }
    */
}
