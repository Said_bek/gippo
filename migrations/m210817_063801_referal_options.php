<?php

use yii\db\Migration;

/**
 * Class m210817_063801_referal_options
 */
class m210817_063801_referal_options extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE clients ADD COLUMN referal_status integer";
        $this->execute($sql);
        $sql = "ALTER TABLE clients ADD COLUMN referal_id integer";
        $this->execute($sql);
        $sql = "ALTER TABLE clients DROP COLUMN referal_step";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210817_063801_referal_options cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210817_063801_referal_options cannot be reverted.\n";

        return false;
    }
    */
}
