<?php

use yii\db\Migration;

/**
 * Class m210824_054646_add_table_to_carousel
 */
class m210824_054646_add_table_to_carousel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE carusel ADD COLUMN product_id integer";
        $this->execute($sql);

        $sql = "ALTER TABLE carusel ADD COLUMN category_id integer";
        $this->execute($sql);

        $sql = "ALTER TABLE carusel ADD COLUMN type smallint";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210824_054646_add_table_to_carousel cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210824_054646_add_table_to_carousel cannot be reverted.\n";

        return false;
    }
    */
}
