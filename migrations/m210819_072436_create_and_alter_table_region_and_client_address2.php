<?php

use yii\db\Migration;

/**
 * Class m210819_072436_create_and_alter_table_region_and_client_address2
 */
class m210819_072436_create_and_alter_table_region_and_client_address2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'title_uz' => $this->string(),
            'title_ru' => $this->string(),
        ]);

        $sql = "ALTER TABLE clients_address2 RENAME COLUMN region to  region_id;";
        $this->execute($sql);


        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210819_072436_create_and_alter_table_region_and_client_address2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210819_072436_create_and_alter_table_region_and_client_address2 cannot be reverted.\n";

        return false;
    }
    */
}
