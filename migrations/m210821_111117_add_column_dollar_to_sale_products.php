<?php

use yii\db\Migration;

/**
 * Class m210821_111117_add_column_dollar_to_sale_products
 */
class m210821_111117_add_column_dollar_to_sale_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = "ALTER TABLE product_sale ADD COLUMN sale_dollar DOUBLE PRECISION";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210821_111117_add_column_dollar_to_sale_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210821_111117_add_column_dollar_to_sale_products cannot be reverted.\n";

        return false;
    }
    */
}
